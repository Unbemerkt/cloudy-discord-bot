module.exports = class {
    message = ""
    link = ""
    theme = "blue"
    title = ""
    showOnce = false;
    id = ""
    closeOnClick = false
    important = false
    constructor(id,title, message, link, theme, showOnce, closeOnClick, important) {
        if (id===undefined){
            this.id = "NOT_"+this.getTimeStamp();
        }else{
            this.id = id;
        }
        this.title = title;
        this.message = message;
        this.link = link;
        this.theme = theme;
        this.showOnce = showOnce;
        this.closeOnClick = closeOnClick;
        this.important = important;
    }
    setId(id){
        if (id===undefined){
            this.id = "NOT_"+this.getTimeStamp();
        }else{
            this.id = id;
        }
    }
    setTitle(title){
        this.title = title;
        return this;
    }
    setMessage(message){
        this.message = message;
        return this;
    }
    setCloseOnClick(closeOnClick){
        this.closeOnClick = closeOnClick;
        return this;
    }

    setLink(url){
        this.link = url;
        return this;
    }
    setImportant(){
        this.important = true;
        return this;
    }

    showOnlyOnce(){
        this.showOnce = true;
        return this;
    }

    setTheme(theme){
     this.theme = theme;
        return this;
    }

    getId(){
        return this.id;
    }
    getTimeStamp() {
        const date = new Date();
        return this.getTime() + "_" + date.getDate() + "_" + date.getMonth() + "_" + date.getFullYear();
    }

    getTime() {
        var str = "";

        var currentTime = new Date()
        var hours = currentTime.getHours()
        var minutes = currentTime.getMinutes()
        var seconds = currentTime.getSeconds()

        if (minutes < 10) {
            minutes = "0" + minutes
        }
        if (seconds < 10) {
            seconds = "0" + seconds
        }
        str += hours + "_" + minutes + "_" + seconds;
        return str;
    }

    build(){
        return {
            id: this.id,
            title: this.title,
            message: this.message,
            link: this.link,
            theme: this.theme,
            showOnce:  this.showOnce,
            closeOnClick: this.closeOnClick,
            important: this.important
        }
    }
}