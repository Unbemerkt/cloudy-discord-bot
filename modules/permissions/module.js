const moduleLoader = require("./../../moduleLoader");
const Notification = require("./Notification");
const NotificationManager = require("./NotificationManager");
const {ModuleUtils} = require("../../ModuleUtils");
const randomstring = require("randomstring");
const md5 = require('md5');
const path = require("path");
const fs = require("fs");
const moduleInfo = {
    name: "permissions",
    prio: 1,
    enable: true,
    requiredModules: [
        "discordCore"
    ],
    requiredNodeModules: [
        "discord.js",
        "randomstring",
        "md5"
    ]
}
const moduleUtils = new ModuleUtils(moduleInfo);
module.exports = {
    moduleInfo,
    moduleUtils,
    permissionsModule: {},
    moduleRegistered: function () {

    },
    moduleLoaded: function () {

    },
    runModule: function () {
        this.NotificationManager = new NotificationManager(this);
        this.permissionsModule = moduleLoader.getModule("permissions");
        const consoleUser = this.permissionsModule.getUser("console")
        if (consoleUser === undefined) {
            moduleUtils.log("Creating default console user...")
            new this.PermissionUser({
                "name": "console",
                "discord": {},
                "groups": [],
                "permissions": [
                    "*"
                ],
                "web": {
                    "canLogin": false,
                    "password": "",
                    "token": "",
                    "timeout": 0
                },
                "groupsPermissions": []
            }, this.permissionsModule).save();
            moduleUtils.log("Default user created!")
        }
        if (moduleLoader.hasCrashed) {
            const crashNotification = new Notification("SYS_CRASH_REPORT");
            crashNotification.setTitle("Crash Detected!")
            crashNotification.setTheme("red");
            crashNotification.setLink("/crashes");
            crashNotification.setImportant();
            if (moduleLoader.crashData.crashes === 1) {
                crashNotification.setMessage("Detected crash! Seems like its the first!")
            } else {
                crashNotification.setMessage("Detected crash! Its the " + moduleLoader.crashData.crashes + ". crash in a row!")
            }
            if (this.NotificationManager.addSystemNotification(crashNotification)) {
                moduleUtils.log("Added Crash Notification!")
            } else {
                moduleUtils.log("Failed to add crash Notification!")
            }
        }
    },
    /**
     * @param group intern group id or name
     * */
    getGroup(group) {
        if (this.permissionsModule === undefined) {
            this.permissionsModule = moduleLoader.getModule("permissions");
        }
        //check for name
        if (fs.existsSync(__dirname + "/data/groups/" + group + ".json")) {
            var tmp = require.resolve(__dirname + "/data/groups/" + group + ".json")
            if (require.cache[tmp] !== undefined) delete require.cache[tmp];
            var tmpGroup = require(__dirname + "/data/groups/" + group + ".json");
            tmpGroup.name = group;
            return new this.PermissionGroup(tmpGroup, this.permissionsModule);
        }
        // moduleUtils.log("Group '"+group+"' not found!")
        return undefined;
    },
    /**
     * @param user discord user id or intern username
     * */
    getUser(user) {
        if (this.permissionsModule === undefined) {
            this.permissionsModule = moduleLoader.getModule("permissions");
        }
        //check for username
        if (fs.existsSync(__dirname + "/data/users/" + user + ".json")) {
            var tmp = require.resolve(__dirname + "/data/users/" + user + ".json")
            if (require.cache[tmp] !== undefined) delete require.cache[tmp];
            var tmpUser = require(__dirname + "/data/users/" + user + ".json")
            tmpUser.name = user;
            return new this.PermissionUser(tmpUser, this.permissionsModule);
        }
        //check for id
        var resultUser;
        this.getAllUsers().every(u => {
            if (u.discord !== undefined && u.discord.userID !== undefined && u.discord.userID === user) {
                resultUser = u;
                return false; //break
            }
            return true;
        });
        // if (resultUser===undefined){
        //     moduleUtils.error("User '"+user+"' not found!")
        // }
        return resultUser;
    },
    createGuestUser(discordMember){
        return new PermissionUser({
            name: discordMember.id,
            discord: {
                userID: discordMember.id,
                roles: discordMember.roles.map(role=> role.id)
            },
            isGuest: true
        },this.permissionsModule);
    },
    getAllUsers() {
        const usersPath = path.resolve(__dirname + "/data/users/");
        const userFiles = fs.readdirSync(usersPath).filter(file => file.endsWith('.json'));
        const users = [];
        for (const file of userFiles) {
            const filePath = path.join(usersPath, file);
            var tmp = require.resolve(__dirname + "/data/users/" + file)
            if (require.cache[tmp] !== undefined) delete require.cache[tmp];
            const tmpUser = require(filePath);
            tmpUser.name = file.replace(".json", "");
            users.push(new this.PermissionUser(tmpUser, this.permissionsModule));
        }
        return users;
    },
    getAllGroups() {
        const groupsPath = path.resolve(__dirname + "/data/groups/");
        const groupFiles = fs.readdirSync(groupsPath).filter(file => file.endsWith('.json'));
        const groups = [];
        for (const file of groupFiles) {
            const filePath = path.join(groupsPath, file);
            const tmpGroup = require(filePath);
            tmpGroup.name = file.replace(".json", "");
            groups.push(new this.PermissionGroup(tmpGroup, this.permissionsModule));
        }
        return groups;
    },
    getGroupsByRoles(guildID, rolesList){
        const groups = this.getAllGroups();
        const result = [];
        groups.forEach(group=>{
            if (group.isInGroup(guildID, rolesList)){
                result.push(group);
            }
        })
        return result;
    },
    loggedinUsers: {},
    /**
     * @param uname string: username
     * @param password string: password
     * @return undefined or PermissionUser object
     * */
    authenticate(uname, password) {
        const user = this.getUser(uname);
        if (user === undefined) {
            return undefined;
        }
        const authUser = user.auth(password);
        if (authUser !== undefined) {
            this.loggedinUsers[authUser.web.token] = authUser;
        }
        return authUser;
    },
    getUserByToken(token) {
        var tokenUser = this.loggedinUsers[token];
        if (tokenUser !== undefined) {
            tokenUser = this.getUser(tokenUser.getName());
            if (tokenUser.getToken() !== undefined) {
                return tokenUser;
            }
        }

        var user;
        this.getAllUsers().every(u => {
            if (u.web.token !== undefined && u.web.token === token) {
                user = u;
                return false; //break
            }
            return true;
        })
        return user;
    },
    /**
     * @param token string: token
     * @return undefined or PermissionUser object
     * */
    authenticateToken(token) {
        const tokenUser = this.loggedinUsers[token];
        var user;
        if (tokenUser === undefined) {
            //no current user by token found!
            this.getAllUsers().every(u => {
                const t = u.getToken();
                if (t !== undefined && t === token) {
                    user = u;
                    return false; //break
                }
                return true;
            })
            if (user === undefined) {
                return undefined;
            }
            token = user.getToken();
            if (token !== undefined) {
                this.loggedinUsers[token] = user;
                return user;
            }
            return undefined;
        }
        user = this.getUser(tokenUser.getName());
        if (user === undefined) {
            delete this.loggedinUsers[token];
            return undefined;
        }
        if (user.getToken() === undefined) {
            delete this.loggedinUsers[token];
            return undefined;
        }
        return user;
    },
    PermissionGroup: require("./PermissionGroup"),
    PermissionUser: require("./PermissionUser"),
    Notification,
    NotificationManager: undefined
};