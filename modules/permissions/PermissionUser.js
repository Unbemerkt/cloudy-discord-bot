const md5 = require("md5");
const randomstring = require("randomstring");
const fs = require("fs");
module.exports = class {
    jsonData = {}
    discord = {}
    name = ""
    groups = []
    permissions = []
    groupsPermissions = []
    permissionManager
    web = {}
    notifications = {}
    isGuest = false;
    constructor(jsonData, permissionManager) {
        this.jsonData = jsonData;
        this.discord = jsonData.discord;
        if (jsonData.discord === undefined) {
            this.discord = {
                userID: "",
                linkToken: ""
            }
        } else {
            this.discord = jsonData.discord;
        }

        this.name = jsonData.name;
        if (jsonData.web === undefined) {
            this.web = {
                canLogin: false,
                password: "",
                token: "",
                timeout: 0
            }

        } else {
            this.web = jsonData.web;
        }
        this.permissions = jsonData.permissions || [];
        this.isGuest = jsonData.isGuest || false;
        this.groups = jsonData.groups || [];
        this.notifications = jsonData.notifications || {};
        this.permissionManager = permissionManager;
        this.loadGroupPermissions();
    }

    addPermission(permission) {
        if (permission === undefined) return false;
        if (this.permissions.includes(permission)) return false;
        this.permissions.push(permission);
        return true;
    }

    getNotifications() {
        return this.notifications;
    }

    getNotification(id) {
        return this.notifications[id];
    }

    removeNotification(id) {
        if (this.isConsoleUser()) return false;
        if (this.notifications[id] === undefined) return;
        delete this.notifications[id];
    }

    addNotification(notificationObject) {
        if (this.isConsoleUser()) return false;
        if (typeof notificationObject.build !== "function") return false;
        this.notifications[notificationObject.getId()] = notificationObject.build();
        return true;
    }

    isGuestUser(){

    }
    isConsoleUser() {
        return this.getName() === "console";
    }

    removePermission(permission) {
        if (permission === undefined) return false;
        if (!this.permissions.includes(permission)) return false;
        if (this.permissions.length === 1) {
            this.permissions = [];
            return true;
        }


        const index = this.permissions.indexOf(permission);
        this.permissions.splice(index, 1);
        return true;
    }

    getPermissions() {
        return this.permissions;
    }

    setDiscordID(id) {
        this.discord.userID = id;
    }

    getJsonData() {
        return this.jsonData;
    }

    getGroups() {
        return this.groups;
    }

    getPassword() {
        return this.web.password;
    }

    isWebEnabled() {
        return !(this.web === undefined || (this.web.canLogin !== undefined && !this.web.canLogin));
    }

    canLink() {
        return this.discord.linkToken !== undefined && this.discord.linkToken.length !== 0;
    }

    isLinkToken(token) {
        return this.discord.linkToken === token;
    }

    link(userID) {
        this.discord.userID = userID;
        this.discord.linkToken = "";
        this.save();
        return true;
    }

    startLink(randomString) {
        if (randomString === undefined || randomString.length === 0) return false;
        this.discord.linkToken = randomString;
        this.save();
        return true;
    }

    getToken() {
        if (Date.now() >= this.web.timeout) {
            this.web.token = "";
            this.web.timeout = 0;
            this.save();
            return undefined;
        }
        return this.web.token;
    }

    auth(password) {
        if (!this.isWebEnabled()) return undefined;
        if (md5(password) === this.getPassword()) {
            this.login();
            return this;
        } else {
            return undefined;
        }
    }

    login() {
        this.web.timeout = Date.now() + 259200000;
        const newToken = randomstring.generate(12);
        if (this.permissionManager.getUserByToken(newToken) !== undefined) {
            return this.login();
        }
        this.web.token = newToken;
        this.save();
        return this.web;
    }

    isTimeout() {
        return false;
    }

    setPassword(password) {
        this.web.password = md5(password);
        return true;
    }

    setGroups(groups) {
        if (groups === undefined) return false;
        this.groups = groups;
        this.loadGroupPermissions();
        return true;
    }

    setPermissions(permissions) {
        if (permissions === undefined) return false;
        this.permissions = permissions;
        return true;
    }

    addGroup(group) {
        if (group === undefined) return false;
        if (typeof group === "string") {
            if (this.groups.includes(group)) return false;
            this.groups.push(group);
            this.loadGroupPermissions()
            return true;
        }
        if (this.groups.includes(group.getName())) return false;
        this.groups.push(group.getName());
        this.loadGroupPermissions()
        return true;
    }

    removeGroup(group) {
        if (group === undefined) return false;
        if (typeof group === "string") {
            if (!this.groups.includes(group)) return false;
            if (this.groups.length === 1) {
                this.groups = [];
                return true;
            }
            const index = this.groups.indexOf(group);
            this.groups = this.groups.splice(index, 1);
            this.loadGroupPermissions()
            return true;
        }
        if (!this.groups.includes(group.getName())) return false;
        if (this.groups.length === 1) {
            this.groups = [];
            return true;
        }
        const index = this.groups.indexOf(group.getName());
        this.groups.splice(index, 1);
        this.loadGroupPermissions()
        return true;
    }

    loadGroupPermissions() {
        this.groupsPermissions = [];
        if (this.permissionManager === undefined) {
            console.error("[" + this.getName() + "] PermissionManager is undefined!");
            return;
        }
        var tmpGroups = [];
        this.groups.forEach(group => {
            const tmpGroup = this.permissionManager.getGroup(group);
            if (tmpGroup !== undefined) {
                tmpGroup.getPermissions().forEach(perm => {
                    if (!this.getGroupsPermissions().includes(perm)) this.groupsPermissions.push(perm)
                });
                tmpGroup.getImportPermissions().forEach(perm => {
                    if (!this.getGroupsPermissions().includes(perm)) this.groupsPermissions.push(perm)
                });
            } else {
                tmpGroups.push(group);
            }
        });
        if (tmpGroups.length > 0) {
            tmpGroups.forEach(group => {
                const index = this.groups.indexOf(group);
                this.groups.splice(index, 1);
            });
            this.save();
        }
    }

    getGroupsPermissions() {
        return this.groupsPermissions;
    }

    isPermissionExcluded(permission) {
        permission = "-" + permission;
        if (this.permissions.includes(permission)) return true;
        if (this.groupsPermissions.includes(permission)) return true;
        return false;
    }

    hasPermission(permission) {
        if (this.isPermissionExcluded(permission)) return false;
        if (this.permissions.includes("*")) return true;
        if (this.groupsPermissions.includes("*")) return true;
        if (this.permissions.includes(permission)) return true;
        if (!this.groupsPermissions.includes(permission)) {
            const tmp = permission.split(".");
            if (tmp.length > 1) {
                try {
                    var count = tmp.length - 1;
                    for (let i = count; i > 0; i--) {
                        var permStr = "";
                        if (i === count) {
                            tmp[i] = "*";
                        } else {
                            tmp.pop();
                            tmp[i] = "*";
                        }
                        for (let j = 0; j < tmp.length; j++) {
                            if (j === 0) {
                                permStr = tmp[j];
                            } else {
                                permStr += "." + tmp[j];
                            }
                        }
                        if (this.permissions.includes(permStr) || this.groupsPermissions.includes(permStr)) return true;
                    }
                } catch (e) {
                    console.error(e);
                }
            } else {
                var permStr = tmp[0] + ".*";
                if (this.permissions.includes(permStr) || this.groupsPermissions.includes(permStr)) return true;
            }
            return false;
        }
        return true;
    }

    hasPermissions(permissions) {
        if (permissions.length === 0) return true;
        if (this.permissions.length === 0 && this.groupsPermissions.length === 0) return false;
        for (let i = 0; i < permissions.length; i++) {
            if (this.hasPermission(permissions[i])) return true;
        }
        return false;
    }

    getName() {
        return this.name.toLowerCase();
    }

    setWebEnabled(bool) {
        if (this.web.password.length === 0 && bool) return false;
        this.web.canLogin = bool;
        return true;
    }

    getId() {
        return this.discord.userID;
    }

    save() {
        if (this.isConsoleUser()) return;
        try {
            //save user to file
            this.jsonData.groups = this.groups;
            this.jsonData.permissions = this.permissions;
            this.jsonData.discord = this.discord;
            this.jsonData.notifications = this.notifications;
            var tmpMap = {};
            for (var i in this.jsonData)
                tmpMap[i] = this.jsonData[i];
            // var tmpMap = new Map(this.jsonData);
            delete tmpMap["name"];
            fs.writeFileSync(__dirname + "/data/users/" + this.getName() + ".json", JSON.stringify(tmpMap, null, 2));
            //update loggedin users
            if (this.permissionManager.loggedinUsers[this.web.token] !== undefined) {
                this.permissionManager.loggedinUsers[this.web.token] = this;
            }
        } catch (e) {
            moduleUtils.error(e);
        }
    }

    updateUser() {
        if (this.permissionManager.loggedinUsers[this.web.token] !== undefined) {
            const newUser = this.permissionManager.getUser(this.getName());
            this.permissionManager.loggedinUsers[this.web.token] = newUser;
            return newUser;
        }
        return this;
    }

    delete() {
        if (this.permissionManager.loggedinUsers[this.web.token] !== undefined) {
            delete this.permissionManager.loggedinUsers[this.web.token];
        }
        fs.unlinkSync(__dirname + "/data/users/" + this.getName() + ".json");
    }
};