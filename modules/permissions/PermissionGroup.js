const fs = require("fs");
module.exports =  class {
    jsonData = {}
    name = ""
    permissions = []
    importFrom = []
    importPermissions = []
    permissionManager
    discord = {};
    constructor(jsonData, permissionManager) {
        this.jsonData = jsonData;
        this.name = jsonData.name;
        this.permissions = jsonData.permissions || [];
        this.importFrom = jsonData.importFrom || [];
        this.discord = jsonData.discord || {
            gID: {roleIDs: ["ID","ID2"]}

        };
        this.permissionManager = permissionManager;
        this.loadImportPermissions();
    }

    addDiscordRoleID(guildID, roleID) {
        if (this.discord[guildID]===undefined){
            this.discord[guildID] = {
                roleIDs: [roleID]
            }
            return;
        }
        if (!this.discord[guildID].roleIDs.includes(roleID))this.discord[guildID].roleIDs.push(roleID);
    }

    removeDiscordRoleID(guildID, roleID) {
        if (this.discord[guildID]===undefined){
            return;
        }
        if (this.discord[guildID].roleIDs.includes(roleID)){
            this.discord[guildID].roleIDs = this.discord[guildID].roleIDs.filter(id=>id!==roleID);
        }
    }

    /**
     * Check if given id/ids is/are connected to this group
     * */
    isInGroup(guildID,IDs){
        if (this.discord[guildID]===undefined)return false;
        if (typeof IDs === "string"){
            return this.discord[guildID].roleIDs.includes(IDs);
        }
        for (let i = 0; i < IDs.length; i++) {
            if (this.discord[guildID].roleIDs!==undefined&&!this.discord[guildID].roleIDs.includes(IDs[i])){
                continue;
            }
            return true;
        }
        return false;
    }

    setDiscordData(discordData) {
        this.discord = discordData
        return true;
    }


    addPermission(permission) {
        if (permission === undefined) return false;
        if (this.permissions.includes(permission)) return false;
        this.permissions.push(permission);
        this.updateUsers();
        return true;
    }

    updateUsers() {
        this.permissionManager.moduleUtils.log("Updating users...");
        this.permissionManager.loggedinUsers.forEach(user => {
            if (user.getGroups().includes(this.getName())) {
                user.loadGroupPermissions();
            }
        });
        this.permissionManager.moduleUtils.log("Users updated!");
    }

    loadImportPermissions() {
        this.importPermissions = [];
        this.importFrom.forEach(gName => {
            const tmpGroup = this.permissionManager.getGroup(gName);
            if (tmpGroup !== undefined) {
                tmpGroup.getPermissions().forEach(perm => {
                    if (!this.importPermissions.includes(perm)) this.importPermissions.push(perm)
                });
            }
        })
    }

    removePermission(permission) {
        if (permission === undefined) return false;
        if (!this.permissions.includes(permission)) return false;
        const index = this.permissions.indexOf(permission);
        this.permissions.splice(index, 1);
        return true;
    }

    setPermissions(permissions) {
        if (permissions === undefined) return false;
        this.permissions = permissions;
        return true;
    }

    getPermissions() {
        return this.permissions;
    }

    getJsonData() {
        return this.jsonData;
    }

    getImports() {
        return this.importFrom;
    }

    setImports(groups) {
        if (groups === undefined) return false;
        this.importFrom = groups;
        this.loadImportPermissions();
        return true;
    }

    addImport(group) {
        if (group === undefined) return false;
        if (typeof group === "string") {
            if (this.importFrom.includes(group)) return false;
            this.importFrom.push(group);
            this.loadImportPermissions();
            return true;
        }
        if (this.importFrom.includes(group.getName())) return false;
        this.importFrom.push(group.getName());
        this.loadImportPermissions();
        return true;
    }

    removeImport(group) {
        if (group === undefined) return false;
        if (typeof group === "string") {
            if (!this.importFrom.includes(group)) return false;
            const index = this.importFrom.indexOf(group);
            this.importFrom.splice(index, 1);
            this.loadImportPermissions();
            return true;
        }
        if (!this.importFrom.includes(group.getName())) return false;
        const index = this.importFrom.indexOf(group.getName());
        this.importFrom.splice(index, 1);
        this.loadImportPermissions();
        return true;
    }

    getImportPermissions() {
        return this.importPermissions;
    }

    getName() {
        return this.name.toLowerCase();
    }


    isPermissionExcluded(permission) {
        permission = "-" + permission;
        return this.permissions.includes(permission);

    }

    hasPermission(permission) {
        if (this.isPermissionExcluded(permission)) return false;
        if (this.permissions.includes("*")) return true;
        if (this.permissions.includes(permission)) return true;
        if (!this.permissions.includes(permission)) {
            const tmp = permission.split(".");
            if (tmp.length > 1) {
                try {
                    var count = tmp.length - 1;
                    for (let i = count; i > 0; i--) {
                        var permStr = "";
                        if (i === count) {
                            tmp[i] = "*";
                        } else {
                            tmp.pop();
                            tmp[i] = "*";
                        }
                        for (let j = 0; j < tmp.length; j++) {
                            if (j === 0) {
                                permStr = tmp[j];
                            } else {
                                permStr += "." + tmp[j];
                            }
                        }
                        if (this.permissions.includes(permStr)) return true;
                    }
                } catch (e) {
                    console.error(e);
                }
            } else {
                var permStr = tmp[0] + ".*";
                if (this.permissions.includes(permStr)) return true;
            }
            return false;
        }
        return true;
    }

    hasPermissions(permissions) {
        if (permissions.length === 0) return true;
        if (this.permissions.length === 0) return false;
        for (let i = 0; i < permissions.length; i++) {
            if (this.hasPermission(permissions[i])) return true;
        }
        return false;
    }


    save() {
        //save group to file
        this.jsonData.importFrom = this.importFrom;
        this.jsonData.permissions = this.permissions;
        this.jsonData.discord = this.discord;
        var tmpMap = {};
        for (var i in this.jsonData)
            tmpMap[i] = this.jsonData[i];
        // var tmpMap = new Map(this.jsonData);
        delete tmpMap["name"];
        fs.writeFileSync(__dirname + "/data/groups/" + this.getName() + ".json", JSON.stringify(tmpMap, null, 2));
    }

    delete() {
        this.permissionManager.moduleUtils.log("Deleting group...");
        fs.unlinkSync(__dirname + "/data/groups/" + this.getName() + ".json");
        this.permissionManager.moduleUtils.log("Group '" + this.getName() + "' deleted!");
        this.permissionManager.moduleUtils.log("Updating users...");
        this.permissionManager.getAllUsers().forEach(user => {
            user.loadGroupPermissions();
        });
        this.permissionManager.moduleUtils.log("Users updated!");
    }
}