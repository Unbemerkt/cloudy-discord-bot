module.exports = class {
    permissionModule

    constructor(permissionModule) {
        this.permissionModule = permissionModule;
    }


    /**
     * Add notification to all users who have the 'system.notification' permission
     * @param notification Notification object
     * */
    addSystemNotification(notification) {
        return this.addNotification(notification, "system.notification");
    }


    /**
     * Add notification to all users who have the 'system.debug' permission
     * @param notification Notification object
     * */
    addDebugNotification(notification) {
        return this.addNotification(notification, "system.debug");
    }

    /**
     * Add notification to all users
     * @param notification Notification object
     * */
    addGlobalNotification(notification) {
        if (notification === undefined) return false;
        if (typeof notification.build !== "function") return false;
        this.permissionModule.getAllUsers().forEach(user => {
            user.addNotification(notification);
            user.save();
        })
        return true;
    }

    /**
     * Add notification to all users who have the given permission
     * @param notification Notification object
     * @param permission String: permission
     * */
    addNotification(notification, permission) {
        if (notification === undefined) return false;
        if (typeof notification.build !== "function") return false;
        if (permission === undefined) return false;
        if (typeof permission !== "string") return false;
        this.permissionModule.getAllUsers().forEach(user => {
            if (user.hasPermission(permission)) {
                user.addNotification(notification);
                user.save();
            }
        })
        return true;
    }

}