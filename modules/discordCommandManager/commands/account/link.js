const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const moduleLoader = require("./../../../../moduleLoader");
const {save} = require("debug");
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder()
        .setName("link")
        .setDescription("Link your discord account with your cloudy account!")
        .addStringOption(option => {
            return option.setName("cloudy-user").setDescription("Username to link")
                .setRequired(true);
        })
        .addStringOption(option => {
            return option.setName("token").setDescription("Your link token").setRequired(true);
        }),
    execute(interaction) {
        if (permissionManager === undefined) {
            permissionManager = moduleLoader.getModule("permissions");
        }
        var embedMessage;
        const token = interaction.options.getString('token',true);
        const userName = interaction.options.getString('cloudy-user', true).toLowerCase().replaceAll(/ /g, "_");
        if (userName===undefined){
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("An error occurred!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        const user = permissionManager.getUser(userName);
        if (user === undefined) {
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("User `" + userName + "` not found!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        if (token===null){
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("Invalid token!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        if (!user.canLink()){
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("This user cannot be linked at the moment!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        if (!user.isLinkToken(token)){
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("Invalid token!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        user.link(interaction.user.id)
        embedMessage = new EmbedBuilder()
            .setTitle("Success")
            .setDescription("You are now connected with the Cloudy-User `"+user.getName()+"`!")
            .setColor("#00FF00");
        interaction.reply({embeds: [embedMessage], ephemeral: true});
        user.addNotification(new permissionManager.Notification("link_"+user.getName(),"You are now linked!","You are now linked with @"+interaction.user.displayName,"/account","green"))
        user.save()
    },
}