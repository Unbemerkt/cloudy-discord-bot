const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const moduleLoader = require("./../../../../moduleLoader");
const discordUserClass = require("../../DiscordUser");
const commandManager = moduleLoader.getModule("Discord CMD Manager");
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder()
        .setName("clear")
        .setDescription("Clears current channel")
        .addNumberOption(option=>{
            return option.setName("amount")
                .setDescription("Amount of messages to delete")
                .setMinValue(1)
                .setMaxValue(100)
        }),
    execute(interaction) {
        if (permissionManager===undefined)permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermission("discord.command.clear")) {
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("You are not allowed to execute this command!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        try {
            var amount = interaction.options.getNumber("amount")
            if (amount === null) amount = 100;
            interaction.channel.bulkDelete(amount, true)
                .then((_message) => {
                    interaction
                        // do you want to include the current message here?
                        // if not it should be ${_message.size - 1}
                        .reply(`Cleared \`${_message.size}\` messages :broom:`)
                        .then((sent) => {
                            setTimeout(() => {
                                sent.delete();
                            }, 2500);
                        });
                })
        }catch (e){
            interaction.followUp("Something went wrong: "+e.toString());
            console.error(e);
        }
    }
}