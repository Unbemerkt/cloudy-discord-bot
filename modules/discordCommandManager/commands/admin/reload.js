const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const moduleLoader = require("./../../../../moduleLoader");
const discordUserClass = require("../../DiscordUser");
const commandManager = moduleLoader.getModule("Discord CMD Manager");
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder()
        .setName("reload")
        .setDescription("Reloads a command")
        .addStringOption(option=>{
            return option.setName("command")
                .setDescription("The command to reload")
                .setAutocomplete(true)
                .setRequired(true);
        }),
    execute(interaction) {
        const commandName = interaction.options.getString('command', true).toLowerCase();
        if (permissionManager===undefined)permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermission("discord.command.reload")) {
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("You are not allowed to execute this command!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }


        const command = interaction.client.commands.get(commandName);
        if (!command) {
            return interaction.reply({content: `There is no command with name \`${commandName}\`!`, ephemeral: true});
        }


        if (command.isExtern()) {
            commandManager.removeExternCommand(command);
            try {
                // interaction.client.commands.delete(command.data.name);
                delete require.cache[require.resolve(command.getAbsolutePath())];
                const cmd = require(command.getAbsolutePath());

                var newCommand = commandManager.registerExternCommand(command.getAbsolutePath());
                interaction.reply({content: `Command \`${newCommand.data.name}\` was reloaded!`, ephemeral: true});
            } catch (error) {
                console.error(error);
                nteraction.reply({content: `There was an error while reloading a command \`${command.data.name}\`:\n\`${error.message}\``, ephemeral: true});
            }
        }else{
            commandManager.removeCommand(command);
            const category = command.getCategory();

            try {
                // interaction.client.commands.delete(command.data.name);
                delete require.cache[require.resolve("./../"+category+"/"+command.data.name+".js")];
                const cmd = require("./../"+category+"/"+command.data.name+".js");

                var newCommand;
                if (cmd.command!==undefined){
                    newCommand = cmd.command;
                }else{
                    newCommand = new commandManager.Command(cmd.data,cmd.execute);
                    if (cmd.autoComplete!==undefined)newCommand.setAutoComplete(cmd.autoComplete);
                }
                newCommand.setCategory(category);


                commandManager.registerCommand(newCommand)
                interaction.reply({content: `Command \`${newCommand.data.name}\` was reloaded!`, ephemeral: true});
            } catch (error) {
                console.error(error);
                interaction.reply({content: `There was an error while reloading a command \`${command.data.name}\`:\n\`${error.message}\``, ephemeral: true});
            }
        }


    },
    async autoComplete(interaction) {
        const focusedValue = interaction.options.getFocused();
        if (permissionManager === undefined) {
            permissionManager = moduleLoader.getModule("permissions");
        }
        const user = permissionManager.getUser(interaction.user.id);
        if (user === undefined||!user.hasPermission("discord.command.reload")) {
            await interaction.respond([{ name: "No Permission", value: "invalid" }],);
            return;
        }
        const choices = [];
        interaction.client.commands.forEach(cmd=>{
            choices.push(cmd.data.name)
        })
        const filtered = choices.filter(choice => choice.startsWith(focusedValue));
        await interaction.respond(
            filtered.map(choice => ({name: choice, value: choice})),
        );
    }
}