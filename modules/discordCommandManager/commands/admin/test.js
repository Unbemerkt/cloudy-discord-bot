const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const moduleLoader = require("./../../../../moduleLoader");
const commandManager = moduleLoader.getModule("Discord CMD Manager");
var permissionManager;
const discordUserClass = require("../../DiscordUser");
module.exports = {
    data: new SlashCommandBuilder()
        .setName("test")
        .setDescription("Test command"),
    execute(interaction) {
        console.log(interaction)
        if (permissionManager===undefined)permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        // const executeUser = permissionManager.getUser(interaction.user.id);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermission("discord.test")) {
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("You are not allowed to execute this command!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        interaction.reply("YAY");
    }
}