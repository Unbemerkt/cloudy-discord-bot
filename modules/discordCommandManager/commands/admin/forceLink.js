const {SlashCommandBuilder, EmbedBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle} = require("discord.js");
const moduleLoader = require("./../../../../moduleLoader");
const discordUserClass = require("../../DiscordUser");
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder()
        .setName("forcelink")
        .setDescription("Link your discord account with your cloudy account!222")
        .addStringOption(option => {
            return option.setName("cloudy-user").setDescription("Username to link")
                .setAutocomplete(true)
                .setRequired(true);
        })
        .addUserOption(option => {
            return option.setName("member").setDescription("Discord user to link with").setRequired(true);
        }),
    async execute(interaction) {
        if (permissionManager === undefined) {
            permissionManager = moduleLoader.getModule("permissions");
        }
        var embedMessage;
        const mentionedUser = interaction.options.getMember('member');
        const userName = interaction.options.getString('cloudy-user', true).toLowerCase().replaceAll(/ /g, "_");
        if (userName === undefined) {
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("An error occurred!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        const user = permissionManager.getUser(userName);
        if (user === undefined) {
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("User `" + userName + "` not found!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        if (mentionedUser === null) {
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("Discord user not found!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        //force link
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        if (executeUser === undefined || !executeUser.hasPermission("permissions.user.edit.link")) {
            embedMessage = new EmbedBuilder()
                .setTitle("Error")
                .setDescription("You are not allowed to execute this command!")
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        const confirm = new ButtonBuilder()
            .setCustomId('confirm')
            .setLabel('Force Link')
            .setStyle(ButtonStyle.Danger);

        const cancel = new ButtonBuilder()
            .setCustomId('cancel')
            .setLabel('Cancel')
            .setStyle(ButtonStyle.Secondary);

        const row = new ActionRowBuilder()
            .addComponents(cancel, confirm);

        try {
            embedMessage = new EmbedBuilder()
                .setTitle("Are you sure?")
                .setDescription("Do you want to connect `@" + mentionedUser.displayName + "` with Cloudy-User `" + user.getName() + "`?")
                .setColor("#00FF00");
            const response = await interaction.reply({
                embeds: [embedMessage],
                components: [row],
                ephemeral: true
            });
            const collectorFilter = i => i.user.id === interaction.user.id;
            const confirmation = await response.awaitMessageComponent({ filter: collectorFilter, time: 60_000 });

            if (confirmation.customId==="confirm"){
                user.link(mentionedUser.id)
                embedMessage = new EmbedBuilder()
                    .setTitle("Success")
                    .setDescription("`" + mentionedUser.displayName + "` is now connected with the Cloudy-User `" + user.getName() + "`!")
                    .setColor("#00FF00");
                await confirmation.update({embeds: [embedMessage],components: []});
                user.addNotification(new permissionManager.Notification("force_link_" + user.getName(), "You were force linked!", "You were force linked with @" + mentionedUser.displayName + " by " + executeUser.getName() + " (@" + interaction.user.displayName + ")!", "/account", "yellow"))
                user.save()
            }else{
                embedMessage = new EmbedBuilder()
                    .setTitle("Canceled")
                    .setDescription("You canceled this process!")
                    .setColor("#c91616")
                await confirmation.update({embeds: [embedMessage],components: []});
            }
        }catch (e) {
            await interaction.editReply({ content: 'Confirmation not received within 1 minute, cancelling', components: [] });
        }
    },
    async autoComplete(interaction){
        const focusedOption = interaction.options.getFocused(true);
        let choices = [];
        if (permissionManager === undefined) {
            permissionManager = moduleLoader.getModule("permissions");
        }

        const user = permissionManager.getUser(interaction.user.id);
        if (user === undefined||!user.hasPermission("permissions.user.edit.link")) {
            await interaction.respond([{ name: "No Permission", value: "invalid" }],);
            return;
        }

        if (focusedOption.name === 'cloudy-user') {

            permissionManager.getAllUsers().forEach(user=>{
                choices.push(user.getName());
            });
        }
        const filtered = choices.filter(choice => choice.startsWith(focusedOption.value));
        await interaction.respond(
            filtered.map(choice => ({ name: choice, value: choice })));
    }
}