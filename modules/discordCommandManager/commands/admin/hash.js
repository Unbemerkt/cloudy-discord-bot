const {SlashCommandBuilder} = require("discord.js");
const md5 = require("md5");
const moduleLoader = require("./../../../../moduleLoader");
const commandManager = moduleLoader.getModule("Discord CMD Manager");
module.exports = {
    data: new SlashCommandBuilder()
        .setName("hash")
        .setDescription("Hash a word")
        .addStringOption(option=>{
            return option.setName("word").setDescription("Word to hash")
                .setRequired(true);
        }),
    execute(interaction) {
        const word = interaction.options.getString('word', true).toLowerCase();
        interaction.reply({content: word+': '+md5(word), ephemeral: true});

    },
}