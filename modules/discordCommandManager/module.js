const moduleLoader = require("./../../moduleLoader");
const {ModuleUtils} = require("../../ModuleUtils");
const {Events, Collection} = require("discord.js");
const fs = require("fs")
const path = require("path")
const moduleInfo = {
    name: "Discord CMD Manager",
    prio: 2,
    enable: true,
    requiredModules: [
        "discordCore"
    ],
    requiredNodeModules: [
        "discord.js",
    ]
}
const moduleUtils = new ModuleUtils(moduleInfo);
var dCore;
module.exports = {
    moduleInfo,
    moduleRegistered: function () {
    },
    moduleLoaded: function () {
    },
    runModule: function () {
        dCore = moduleLoader.getModule("discordCore");
        dCore.client.commands = new Collection();
        this.loadCommands();
        this.startListener();
    },
    startListener() {
        moduleUtils.log("Starting Listener...")
        dCore.client.on(Events.InteractionCreate, async interaction => {
            if (interaction.isChatInputCommand()){
                const command = interaction.client.commands.get(interaction.commandName);
                if (!command) {
                    console.error(`No command matching ${interaction.commandName} was found.`);
                    return;
                }
                try {
                    await command.resolve(interaction);
                } catch (error) {
                    console.error(error);
                    if (interaction.replied || interaction.deferred) {
                        await interaction.followUp({
                            content: 'There was an error while executing this command!',
                            ephemeral: true
                        });
                    } else {
                        await interaction.reply({
                            content: 'There was an error while executing this command!',
                            ephemeral: true
                        });
                    }
                }
            }else if (interaction.isAutocomplete()){
                const command = interaction.client.commands.get(interaction.commandName);
                if (!command) {
                    console.error(`No command matching ${interaction.commandName} was found.`);
                    return;
                }
                try {
                    await command.autoComplete(interaction);
                } catch (error) {
                    console.error(error);
                }
            }
        });
        moduleUtils.log("CommandListener Started!")
        //Refresh commands in 2 sec
        // setTimeout(()=>{
        //     require("./commandRefresher.js").refresh(moduleUtils);
        // },2000)
    },
    commands: [],
    externCommands: {},
    registerExternCommand(absolutePath) {
        if (dCore.client.commands===undefined){
            dCore.client.commands = new Collection();
        }
        const cmd = require(absolutePath);
        if (cmd === undefined) {
            moduleUtils.error("Command not found! (" + absolutePath + ")");
            return false;
        }
        const command = new this.Command(cmd.data, cmd.execute, true, absolutePath);
        if (cmd.autoComplete!==undefined) command.setAutoComplete(cmd.autoComplete);
        if (!this.validateCommand(command)) {
            moduleUtils.error("Invalid command!")
            return false;
        }
        this.externCommands[absolutePath] = command;
        dCore.client.commands.set(command.data.name, command)
        moduleUtils.log("External command '" + command.data.name + "' registered!")
        return command;
    },
    registerCommand(command) {
        if (!this.validateCommand(command)) {
            moduleUtils.error("Invalid command!")
            return;
        }
        moduleUtils.log("Command '" + command.data.name + "' in category '" + command.getCategory() + "' registered!")
        this.commands.push(command);
        dCore.client.commands.set(command.data.name, command)
    },
    /**
     * @param command Command object or the absolute path of the command file
     * */
    removeExternCommand(command) {
        if (this.externCommands === undefined) {
            this.externCommands = {};
            return;
        }
        if (!this.validateCommand(command)) {
            command = this.externCommands[command];
        }
        if (!this.validateCommand(command)) {
            return;
        }
        dCore.client.commands.sweep((name) => {
            return command.data.name === name;
        })
        delete this.externCommands[command.absolutePath];
    },
    removeCommand(command) {
        if (this.commands === undefined) {
            this.commands = [];
            return;
        }
        dCore.client.commands.sweep((name) => {
            return command.data.name === name;
        })
        if (this.commands.length === 1) {
            if (this.commands.includes(command)) {
                this.commands = [];
            }
            return;
        }
        const index = this.commands.indexOf(command);
        this.commands = this.commands.splice(index, 1);
    },
    loadCommands() {
        if (dCore.client.commands === undefined) {
            dCore.client.commands = new Collection();
        }
        const foldersPath = path.join(__dirname, 'commands');
        const commandFolders = fs.readdirSync(foldersPath);

        for (const folder of commandFolders) {
            const commandsPath = path.join(foldersPath, folder);
            const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
            for (const file of commandFiles) {
                const filePath = path.join(commandsPath, file);
                const commandFile = require(filePath);
                // Set a new item in the Collection with the key as the command name and the value as the exported module
                if ('data' in commandFile && 'execute' in commandFile) {
                    var cmd = new this.Command(commandFile.data, commandFile.execute);
                    if (commandFile.autoComplete!==undefined)cmd.setAutoComplete(commandFile.autoComplete);
                    cmd.setCategory(folder)
                    this.registerCommand(cmd)
                } else {
                    if ('command' in commandFile) {
                        commandFile.command.setCategory(folder);
                        this.registerCommand(commandFile.command)
                    } else {
                        moduleUtils.log(`[WARNING] The command at ${filePath} is missing a required "data" and "execute" or "command" property.`);
                    }

                }
            }
        }

    },
    reloadALLCommands() {
        moduleUtils.pureLog("")
        moduleUtils.pureLog("=========================================")
        moduleUtils.log("Reloading ALL Commands...")
        dCore.client.commands.sweep(() => {
            return true;
        })
        for (let i = 0; i < this.commands.length; i++) {
            var cmd = this.commands[i];
            delete require.cache[require.resolve("./commands/" + cmd.getCategory() + "/" + cmd.data.name + ".js")];
            moduleUtils.log("Command '" + cmd.data.name + "' unloaded!", undefined, "color: #ebdb2f")

        }
        var tmpMap = [];
        Object.keys(this.externCommands).forEach(path => {
            tmpMap.push(path);
        })


        moduleUtils.pureLog("")
        moduleUtils.log("Unloading extern commands...")
        Object.keys(this.externCommands).forEach(path => {
            const cmd = this.externCommands[path];
            delete require.cache[require.resolve(path)];
            moduleUtils.log("Command '" + cmd.data.name + "' unloaded!", undefined, "color: #ebdb2f")
        })
        this.commands = [];
        this.externCommands = {};
        moduleUtils.pureLog("")
        moduleUtils.log("Loading commands...")
        moduleUtils.pureLog("")
        moduleUtils.pureLog("")

        this.loadCommands();

        moduleUtils.pureLog("")
        moduleUtils.log("Loading extern commands...")
        tmpMap.forEach(path => {
            this.registerExternCommand(path);
        })
        moduleUtils.pureLog("=========================================")
        moduleUtils.pureLog("")
        moduleUtils.log("All commands reloaded!", undefined, "color: #00FF00")


    },
    validateCommand(cmd) {
        if (typeof cmd.setAction !== "function") return false;
        if (typeof cmd.isCmd !== "function") return false;
        if (typeof cmd.resolve !== "function") return false;
        return true;
    },
    Command: class {
        data // SlashCommandBuilder
        action
        category
        extern = false
        absolutePath = ""
        autoComplete
        constructor(data, action, extern, absolutePath) {
            this.data = data
            this.action = action
            this.extern = extern
            this.absolutePath = absolutePath
        }

        setAutoComplete(autoComplete){
            this.autoComplete = autoComplete;
        }
        isExtern(){
            return this.extern;
        }

        getAbsolutePath(){
            return this.absolutePath;
        }


        setAction(action) {
            this.action = action;
        }

        autoComplete(interaction){
            if (this.action !== undefined) {
                this.action(interaction);
            } else {
                interaction.respond([{ name: "undefined", value: "undefined" }]);
            }
        }
        resolve(interaction) {
            if (this.action !== undefined) {
                this.action(interaction);
            } else {
                interaction.reply("undefined");
            }

        }

        setCategory(category) {
            this.category = category;
        }

        getCategory() {
            return this.category;
        }

        isCmd(cmd) {
            return cmd === this.data.name;
        }
    }
};