const moduleLoader = require("./../../moduleLoader");
const dCore = moduleLoader.getModule("discordCore")
const cmdManager = moduleLoader.getModule("Discord CMD Manager")

const { REST, Routes } = require('discord.js');
const { clientId, guildId, BOT_TOKEN } = dCore.config;
var commands = [];





// Construct and prepare an instance of the REST module
const rest = new REST().setToken(BOT_TOKEN);
module.exports = {
    refreshGlobal(moduleUtils){
        this.refresh(moduleUtils,true)
    },
    refresh(moduleUtils, isGlobal) {
        commands=[];
        for (let i = 0; i < cmdManager.commands.length; i++) {
            const cmd = cmdManager.commands[i];
            commands.push(cmd.data.toJSON());
        }
        Object.keys(cmdManager.externCommands).forEach(path=>{
            const cmd = cmdManager.externCommands[path];
            commands.push(cmd.data.toJSON());
        })
        if (isGlobal){
            this.updateGlobal(moduleUtils);
            return;
        }
        this.update(moduleUtils);
    },

    async update(moduleUtils){
        try {
            moduleUtils.log(`Started refreshing ${commands.length} application (/) commands.`);

            // The put method is used to fully refresh all commands in the guild with the current set
            const data = await rest.put(
                Routes.applicationGuildCommands(clientId, guildId),
                { body: commands },
            );

            moduleUtils.log(`Successfully refreshing ${data.length} application (/) commands.`);
        } catch (error) {
            // And of course, make sure you catch and log any errors!
            moduleUtils.error(error);
        }
    },
    async updateGlobal(moduleUtils){
        try {
            moduleUtils.log(`Started refreshing ${commands.length} (/) commands Globally.`);

            // The put method is used to fully refresh all commands globally
            const data = await rest.put(
                Routes.applicationCommands(clientId),
                { body: commands },
            );

            moduleUtils.log(`Successfully refreshing ${data.length} (/) commands Globally.`);
        } catch (error) {
            // And of course, make sure you catch and log any errors!
            moduleUtils.error(error);
        }
    }
}
