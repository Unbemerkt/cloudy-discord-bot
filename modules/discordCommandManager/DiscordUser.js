module.exports = class {
    guildMember
    guild
    permissionUser
    groups = []
    constructor(permissionModule, guildMember) {
        this.permissionModule = permissionModule;
        this.guildMember = guildMember;

        this.loadUser();
        this.loadGroups();
    }

    loadGroups(){
        // console.log(this.guildMember);
        // console.log(this.guildMember.guild);
        // console.log(this.guildMember.guild.id);
        this.groups = this.permissionModule.getGroupsByRoles(this.guildMember.guild.id,
            this.guildMember.roles.cache.map(role=>role.id));
    }

    loadUser(){
        this.permissionUser = this.permissionModule.getUser(this.guildMember.id);
    }

    hasPermission(permission){
        if (this.permissionUser!==undefined){
            if (this.permissionUser.hasPermission(permission)) return true;
        }

        if (this.groups.length!==0){
            for (let i = 0; i < this.groups.length; i++) {
                if (this.groups[i].hasPermission(permission))return true;
            }
        }
        return false;
    }

    hasPermissions(permissionList){
        if (this.permissionUser!==undefined){
            if (this.permissionUser.hasPermissions(permissionList)) return true;
        }

        if (this.groups.length!==0){
            for (let i = 0; i < this.groups.length; i++) {
                if (this.groups[i].hasPermissions(permissionList))return true;
            }
        }
        return false;
    }

    getUser() {
        return this.permissionUser;
    }

    isUser(){
        return this.permissionUser!==undefined;
    }
}