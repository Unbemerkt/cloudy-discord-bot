const moduleLoader = require("./../../moduleLoader");
const fs = require("fs");
const {ModuleUtils} = require("../../ModuleUtils");
const moduleInfo = require("./moduleInfo.json");
const config = require("../discordCore/config.json");
const moduleUtils = new ModuleUtils(moduleInfo);
module.exports = {
    moduleInfo,
    moduleRegistered: function () {

    },
    moduleLoaded: function () {

    },
    runModule: function () {
        const settingsModule = moduleLoader.getModule("settings");
        const builder = new settingsModule.SettingsBuilder(moduleInfo);

        const builder3 = new settingsModule.SettingsBuilder({name: "test2"});
        const row1 = new settingsModule.SettingsRow();
        row1.setTitle("Config");
        row1.addSelect("enableModule","Enable module",moduleInfo.enable.toString(),[{value: "true", label: "enable"},{value: "false", label: "disable"}],"*Note: Please restart the app for the settings to take effect!");
        builder.add(row1)
        builder.add(new settingsModule.SettingsRow().addText("Info","This module is just a example for this app! You can disable this module!"))


        const row2 = new settingsModule.SettingsRow();
        row2.setTitle("Row 2");
        row2.addString("stringID3","None hidden String","defaultValue","None hidden string with default value",false,"*Small Test")
        row2.addString("stringID4","Hidden String","defaultValue","Hidden string with default value",true,"*Small Test")
        row2.addText("Title","Some long or not so long text","*Small Test")

        const builder2 = new settingsModule.SettingsBuilder({name: "Tips"});
        builder2.add(new settingsModule.SettingsRow().addText("Show/Hide Protected Inputs","Right click on a password input to show it's content"))
        builder2.add(new settingsModule.SettingsRow().addText("Reload Data","You can reload the bot data by pressing Ctrl+Alt+R"))
        builder2.add(new settingsModule.SettingsRow().addText("Show Crash-Logs","You can see all crash-logs by pressing Ctrl+Alt+C"))
        builder2.hideSave();

        builder3.add(row2.setTitle("Row1"))
        builder3.add(row2.setTitle("Row2"))

        builder.setCallback(module.exports.receiveSettings)
        builder3.setCallback((data)=>{
            console.log("3: ",data);
        })
        settingsModule.registerSettingsBuilder(builder);
        settingsModule.registerSettingsBuilder(builder2);
        settingsModule.registerSettingsBuilder(builder3);

    },
    receiveSettings(settingsResult){
        moduleInfo.enable = settingsResult.getSelect("enableModule")==="true";
        fs.writeFileSync(__dirname+"/moduleInfo.json", JSON.stringify(moduleInfo,null,2));
    }
};