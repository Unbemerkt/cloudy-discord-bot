class TextFieldModule {
    id = ""
    title = ""
    text = ""
    options = ""
    small = ""
    value = ""
    placeholder = ""
    type = "text"
    constructor(id) {
        this.id = id || "";
    }

    setTitle(title) {
        this.title = title;
        return this;
    }

    setText(text) {
        this.text = text;
        return this;
    }

    setOptions(options) {
        this.options = options
        return this;
    }

    setSmall(small) {
        this.small = small;
        return this;
    }

    setValue(value) {
        this.value = value;
        return this;
    }

    setPlaceholder(placeholder){
        this.placeholder = placeholder;
        return this;
    }
    setType(type){
        this.type = type;
        return this;
    }

    build(isFooter) {
        if (isFooter) return "<h4>Oops..</h4><p>Something went wrong...</p><small>This Module is not available for the footer!</small><br><br><small>Module: TextFieldModule</small>";
        var html = ""
        var id = "";
        if (this.id.length > 0) id = " id='" + this.id + "'";
        if (this.title.length !== 0) {
            html = html + "<h4>" + this.title + "</h4>";
        }
        if (this.text.length !== 0) {
            html = html + "<p>" + this.text + "</p>";
        }
        html+='<div class="sidebar--form"><input style="width: 100%" type="'+this.type+'" '+id+' placeholder="'+this.placeholder+'" '+this.options+' value="'+this.value+'"></div>'
        if (this.small.length !== 0) {
            html = html + "<small>" + this.small + "</small>";
        }

        return html;
    }
}