class TextModule {
    title = ""
    text = ""
    small = ""
    constructor() {
    }


    setTitle(title) {
        this.title = title;
        return this;
    }

    setText(text) {
        this.text = text;
        return this;
    }

    setSmall(small) {
        this.small = small;
        return this;
    }

    build(isFooter){
        if (isFooter)return "<h4>Oops..</h4><p>Something went wrong...</p><small>This Module is not available for the footer!</small><br><br><small>Module: TextModule</small>";
        var html = ""
        if (this.title.length>0) {
            html += "<h4>" + this.title + "</h4>";
        }
        if (this.text.length>0) {
            html += "<p>" + this.text + "</p>";
        }
        if (this.small.length>0) {
            html += "<small>" + this.small + "</small>";
        }
        return html;
    }
}