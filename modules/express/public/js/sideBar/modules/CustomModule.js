class CustomModule {
    html
    constructor(html) {
        this.html = html;
    }

    build(){
        return this.html;
    }
}