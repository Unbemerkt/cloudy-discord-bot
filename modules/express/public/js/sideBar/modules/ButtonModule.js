class ButtonModule {
    id = ""
    title = ""
    text = ""
    options = ""
    small = ""
    value = ""
    type = "button";
    constructor(id) {
        this.id = id||"";
    }

    setTitle(title) {
        this.title = title;
        return this;
    }

    setText(text) {
        this.text = text;
        return this;
    }

    setOptions(options) {
        this.options = options
        return this;
    }

    setSmall(small) {
        this.small = small;
        return this;
    }
    setType(type){
        this.type = type;
        return this;
    }

    setValue(value) {
        this.value = value;
        return this;
    }

    build(){
        var html = ""
        var id = "";
        if (this.id.length > 0) id = " id='" + this.id + "'";
        if (this.type === "button") {
            if (this.title.length !== 0) {
                html = html + "<h4>" + this.title + "</h4>";
            }
            if (this.text.length !== 0) {
                html = html + "<p>" + this.text + "</p>";
            }
            if (this.options !== undefined) {
                html = html + "<div class=\"sidebar--form\"><button class='btn btn-outline-info'" + id + " " + this.options + ">" + this.value + "</button></div>";
            } else {
                html = html + "<div class=\"sidebar--form\"><button class='btn btn-outline-info'" + id + ">" + this.value + "</button></div>";
            }
            if (this.small !== undefined) {
                html = html + "<small>" + this.small + "</small>";
            }
        }else{
            if (this.title.length !== 0) {
                html = html + "<h4>" + this.title + "</h4>";
            }
            if (this.text.length !== 0) {
                html = html + "<p>" + this.text + "</p>";
            }
            if (this.options.length>0) {
                html = html + "<div class=\"sidebar--form\"><a class='btn btn-outline-info'"+id+" " + this.options + ">" + this.value + "</a></div>";
            } else {
                html = html + "<div class=\"sidebar--form\"><a class='btn btn-outline-info'"+id+">" + this.value + "</a></div>";
            }
            if (this.small !== undefined) {
                html = html + "<small>" + this.small + "</small>";
            }
        }
        return html;
    }
}