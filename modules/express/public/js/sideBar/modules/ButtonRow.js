class ButtonRow {
    id = ""
    title = ""
    text = ""
    small = ""
    buttons = []; //max 3
    constructor(id) {
        this.id = id||"";
    }

    addButton(buttonObj){
        if (this.buttons.length>=3)return this;
        if (buttonObj.options===undefined) buttonObj.options = "";
        if (buttonObj.value===undefined) buttonObj.value = "Button";
        this.buttons.push(buttonObj);
        return this;
    }

    build(){
        var html = ""

        if (this.title.length>0) {
            html += "<h4>" + this.title + "</h4>";
        }
        if (this.text.length>0) {
            html += "<p>" + this.text + "</p>";
        }
        html += "<div class='sidebar--btnLine'>";
        this.buttons.forEach(btn=>{
            html += "<button class='btn btn-outline-info' "+btn.options+">"+btn.value+"</button>"
        })
        html += "</div>";
        if (this.small.length>0) {
            html += "<small>" + this.small + "</small>";
        }
        return html;
    }
}