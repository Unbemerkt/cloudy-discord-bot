class TextAreaModule {
    id = ""
    title = ""
    text = ""
    options = ""
    small = ""
    value = ""

    constructor(id) {
        this.id = id || "";
    }

    setTitle(title) {
        this.title = title;
        return this;
    }

    setText(text) {
        this.text = text;
        return this;
    }

    setOptions(options) {
        this.options = options
        return this;
    }

    setSmall(small) {
        this.small = small;
        return this;
    }

    setValue(value) {
        this.value = value;
        return this;
    }

    build(isFooter) {
        if (isFooter) return "<h4>Oops..</h4><p>Something went wrong...</p><small>This Module is not available for the footer!</small><br><br><small>Module: TextArea</small>";
        var html = "";
        var id = "";
        if (this.id.length > 0) id = " id='" + this.id + "'";
        if (this.title.length !== 0) {
            html = html + "<h4>" + this.title + "</h4>";
        }
        if (this.text.length !== 0) {
            html = html + "<p>" + this.text + "</p>";
        }
        html = html + "<div class=\"sidebar--form\"><textarea" + id + " " + this.options + " " + (this.id.length > 0 ? "id='" + this.id + "'" : "") + ">" + this.value + "</textarea></div>";
        if (this.small.length !== 0) {
            html = html + "<small>" + this.small + "</small>";
        }
        return html;
    }
}