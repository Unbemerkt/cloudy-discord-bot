class HrModule {
    constructor() {
    }

    build(isFooter){
        if (isFooter)return "<h4>Oops..</h4><p>Something went wrong...</p><small>This Module is not available for the footer!</small><br><br><small>Module: HrModule</small>";
        return "<div class='sidebar--item'><hr></div>";
    }
}