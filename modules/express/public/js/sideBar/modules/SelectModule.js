class SelectModule {
    id = ""
    title = ""
    text = ""
    options = ""
    small = ""
    selected = ""
    selectOptions = [];
    constructor(id) {
        this.id = id||"";
    }

    setTitle(title) {
        this.title = title;
        return this;
    }

    setText(text) {
        this.text = text;
        return this;
    }

    setOptions(options) {
        this.options = options
        return this;
    }

    setSmall(small) {
        this.small = small;
        return this;
    }

    setSelectValues(list) {
        this.selectOptions = list;
        return this;
    }
    addSelect(item){
        this.selectOptions.push(item);
        return this;
    }

    setSelected(value) {
        this.selected = value;
        return this;
    }

    build(isFooter){
        if (isFooter)return "<h4>Oops..</h4><p>Something went wrong...</p><small>This Module is not available for the footer!</small><br><br><small>Module: SelectModule</small>";
        var html = ""
        var id = "";
        if (this.id.length > 0) id = " id='" + this.id + "'";
        if (this.title !== undefined) {
            html += "<h4>" + this.title + "</h4>";
        }
        if (this.text.length !== 0) {
            html = html + "<p>" + this.text + "</p>";
        }
        html += "<div class=\"sidebar--form\"><div class='checkbox'><select"+id+" style='width: 100%' "+this.options+">";
        this.selectOptions.forEach(e => {
            if (typeof e === "string"){
                if (e===this.selected){
                    html += "<option selected value='" + e + "'>" + e + "</option>";
                }else{
                    html += "<option value='" + e + "'>" + e + "</option>";
                }
            }else{
                if (e.value===this.selected){
                    html += "<option selected value='" + e.value + "'>" + e.name + "</option>";
                }else{
                    html += "<option value='" + e.value + "'>" + e.name + "</option>";
                }
            }
        });
        if (this.selectOptions.length===0){
            html += "<option value='INVALID'>No Options given!</option>";
        }
        html += "</select></div></div>";
        if (this.small !== undefined) {
            html += "<small>" + this.small + "</small>";
        }
        return html;
    }
}