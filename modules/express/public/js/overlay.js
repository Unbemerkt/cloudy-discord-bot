const overlay = {
    overlays: {},
    init() {
        this.show("INIT", "Loading...")
    },
    show(id, status) {
        this.overlays[id] = status||"";
        this.updateList()
    },
    hide(id) {
        if (this.overlays[id] !== undefined) {
            delete this.overlays[id]
        }
        this.updateList()
    },
    updateList(){
        if (Object.keys(this.overlays).length < 1){
            document.getElementById("overlay").style.display = "none";
            return;
        }
        document.getElementById("overlay").style.display = "block";
        var html = "";
        Object.keys(this.overlays).forEach((entry)=>{
            const state = this.overlays[entry];
            if (state!==undefined&&state.length!==0)html+="<li>"+state+"</li>";
        })
        if (html.length===0){
            document.getElementById("overlay_text").style.display = "none";
            document.getElementById("overlay_text").innerHTML = "";
        }else{
            document.getElementById("overlay_text").style.display = "block";
            document.getElementById("overlay_text").innerHTML = html;
        }
    }
}
overlay.init();