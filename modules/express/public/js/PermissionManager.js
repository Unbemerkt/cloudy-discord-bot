class PermissionManager {

    groups = {}
    users = {}

    constructor() {
    }

    setGroups(groupMap){
        this.groups = groupMap;
        Object.keys(this.groups).forEach(gName=>{
            const group = this.groups[gName];
            group.loadImportPermissions();
        })
    }

    setUsers(userMap){
        this.users = userMap;
    }
    getUsers(){
        return this.users;
    }

    getGroups(){
        return this.groups;
    }

    getGroup(groupName){
        return this.groups[groupName.toLowerCase()];
    }

    getUser(userName){
        return this.users[userName.toLowerCase()];
    }
    addUser(userObject){
        if (userObject===undefined)return false;
        if (this.users[userObject.getName().toLowerCase()]!==undefined)return false;
        this.users[userObject.getName().toLowerCase()] = userObject;
        return true;
    }
    removeUser(userName){
        if (this.users[userName.toLowerCase()]===undefined)return;
        delete this.users[userName.toLowerCase()];
    }
    addGroup(groupObject){
        if (groupObject===undefined)return false;
        if (this.groups[groupObject.getName().toLowerCase()]!==undefined)return false;
        this.groups[groupObject.getName().toLowerCase()] = groupObject;
        return true;
    }
    removeGroup(groupName){
        if (this.groups[groupName.toLowerCase()]===undefined)return;
        delete this.groups[groupName.toLowerCase()];
    }

}