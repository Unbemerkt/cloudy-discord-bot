var botData = {};
var userData = {};
var currentUser;
var listeners = {};
var moduleNavs = [];
const permissionManager = new PermissionManager();
const discordManager = new DiscordManager();
const dataUpdateIntervalTime = 30000;
var dataLoading = [];
const dataTypes = {
    USER: "USER",
    ALL: "ALL",
    DISCORD: "DISCORD",
    BOT: "BOT",
    MODULES_NAV: "MODULES_NAV"
}
var errorCount = 0;
var dataUpdateInterval;
function loadData(cb, type){
    if (type===undefined)type="ALL";
    if (dataLoading.includes(type))return;
    addLoading("DATA_"+type)
    dataLoading.push(type);
    $.get("/getData/"+type, (result) => {
        if (result.type === "OK") {
            switch (type){
                case "MODULES_NAV":
                    moduleNavs = result.modulesNav;
                    if (cb)cb();
                    break;
                case "BOT":
                    botData = result.botData;
                    triggerEvents(dataTypes.BOT,botData);
                    if (cb)cb();
                    break;
                case "DISCORD":
                    discordManager.setData(result.discordData)
                    triggerEvents(dataTypes.DISCORD,discordManager);
                    if (cb)cb();
                    break;
                case "USER":
                    if (currentUser!==undefined){
                        const newUser = new PermissionUser(userData, permissionManager);
                        if (!currentUser.equal(newUser))
                            currentUser = newUser;
                        if (cb)cb();
                    }else{
                        currentUser = new PermissionUser(userData,permissionManager);
                        if (cb)cb();
                    }
                    triggerEvents(dataTypes.USER,currentUser);
                    break
                case "ALL":
                default:
                    botData = result.botData;
                    userData = result.userData;
                    discordManager.setData(result.discordData)
                    if (currentUser!==undefined){
                        const newUser = new PermissionUser(userData, permissionManager);
                        if (!currentUser.equal(newUser))
                            currentUser = newUser;
                        if (cb)cb();
                    }else{
                        currentUser = new PermissionUser(userData,permissionManager);
                        if (cb)cb();
                    }
                    triggerEvents(dataTypes.ALL,botData);
                    break;
            }
        }else{
            if (result.data==="TOKEN_INVALID"){
                window.location.href = "/login";
                console.error("TOKEN_INVALID")
                return;
            }
            console.error(result)
            console.error("Some error occurred! ",result.data);
            showError(result.data);
        }
        setTimeout(()=>{
            removeLoading("DATA_"+type)
            dataLoading.remove(type);
        },500)
        errorCount = 0;
        overlay.hide("DATA_REC");
    }).catch((e)=>{
        if (errorCount===4){
            removeLoading("DATA_"+type)
            dataLoading.remove(type);
            return;
        }
        if (errorCount===3){
            showError("Failed to load <i>"+type+"</i> data (Status code "+e.status+")");
            console.error(e)
            setTimeout(()=>{
                removeLoading("DATA_"+type)
                dataLoading.remove(type);
                overlay.hide("DATA_REC");
            },500);
            errorCount++;
            return;
        }
        if (errorCount===1){
            overlay.show("DATA_REC","Reconnecting...")
        }
        removeLoading("DATA_"+type)
        dataLoading.remove(type);
        errorCount++;
    });

}
function loadPermissions(cb){
    addLoading("DATA_PERMISSIONS")
    $.get("/groups", (result) => {
        if (result.type === "OK") {
            //build list
            var groups = {};
            result.groups.forEach(g=>{
                const group = new PermissionGroup(g, permissionManager);
                groups[group.getName()] = group;
            })
            permissionManager.setGroups(groups);
            $.get("/users", (result) => {
                if (result.type === "OK") {
                    //build list
                    var users = {};
                    result.users.forEach(u=>{
                        var user = new PermissionUser(u, permissionManager);
                        users[user.getName()] = user;
                    })
                    removeLoading("DATA_PERMISSIONS")
                    permissionManager.setUsers(users);
                    if (cb) cb();
                }
            })

        }
    })
}
function registerDataEvent(id, cb) {
    listeners[id]=cb;
}

function removeDataEvent(id) {
    if (listeners[id]!==undefined)delete listeners[id];
}
function triggerEvents(dataType,newData){
    Object.keys(listeners).forEach(id=>{
        listeners[id](dataType,newData);
    })
}
function init() {
    shortcut.add("CTRL+ALT+R",()=>{
        loadData();
    });
    $(".loadIndicator").show();
    loadPermissions(()=>{
        loadData(() => {
            initNavigator();
        });
    });
    dataUpdateInterval = setInterval(loadData,dataUpdateIntervalTime)
}
init();