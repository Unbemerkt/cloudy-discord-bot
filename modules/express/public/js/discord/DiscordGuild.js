class DiscordGuild {
    rawData

    constructor(guildData) {
        this.rawData = guildData;
    }

    getId() {
        return this.rawData.guildID
    }

    getName() {
        return this.rawData.guildName
    }

    getRoles() {
        return this.rawData.roles
    }

    getRole(id) {
        if (this.rawData.roles.length === 0) return undefined;
        var returnValue;
        Object.keys(this.rawData.roles).every(i => {
            if (this.rawData.roles[i].id === id) {
                returnValue = this.rawData.roles[i];
                return false;
            }
            return true;
        })
        return returnValue;
    }

    getMembers() {
        return this.rawData.members
    }
}