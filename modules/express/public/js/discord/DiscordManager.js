class DiscordManager {
    guilds
    constructor(data) {
        if (data!==undefined) this.setData(data);
    }
    setData(data){
        if (data===undefined){
            this.guilds = {};
            return;
        }
        this.guilds = data
        var tmp = {};
        Object.keys(this.guilds).forEach(gID=>{
            tmp[gID] = new DiscordGuild(this.guilds[gID]);
        })
        this.guilds = tmp;
    }
    getGuild(guildID){
        return this.guilds[guildID];
    }
    hasData(){
        return Object.keys(this.guilds).length>0;
    }

    getGuilds() {
        return this.guilds;
    }

    getAllMembers(){
        var members = {};
        Object.keys(this.guilds).forEach(gID=>{
            const guild = this.guilds[gID];
            members[gID] = guild.getMembers();
        })
        return members;
    }

    getMember(id){
        const members = this.getAllMembers();
        var member;
        Object.keys(members).every(gID=>{
            for (let i = 0; i < members[gID].length; i++) {
                if (members[gID][i].id===id){
                    member = members[gID][i];
                    return false;
                }
            }
            return true;
        })
        return member;
    }
}