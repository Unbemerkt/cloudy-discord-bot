var oneTimers = {};
function updateNotifications() {
    const notifications = currentUser.getNotifications();
    var html = "";
    if (Object.keys(notifications).length === 0) {
        setHTML("notificationsBox", "<i>No notifications found...</i>");
        return;
    }
    const alreadyBuild = []
    //important
    Object.keys(notifications).every(id => {
        if (alreadyBuild.includes(id)) return true;
        const notification = notifications[id];
        if (!notification.important) return true;
        alreadyBuild.push(id);
        html += '<div id="' + id + '" class="notification ' + notification.theme + '">';
        if (!notification.showOnce) html += '<div class="top-right"><a onclick="removeNotification(\'' + id + '\')">x</a></div>'
        html += '<div onclick="handleNotification(\'' + notification.id + '\')" class="pointer">';
        html += '<h3>' + notification.title + '</h3>';
        html += '<p>' + notification.message + '</p>';
        html += '<div class="bottom">';
        if (notification.link.length !== 0) {
            html += '<small>' + notification.link + '</small>';
        }
        html += '</div></div></div>';
        if (notification.showOnce) {
            currentUser.removeNotification(notification.id);
        }
        return true;
    })


    //onetime
    Object.keys(notifications).every(id => {
        if (alreadyBuild.includes(id)) return true;
        const notification = notifications[id];
        if (!notification.showOnce) return true;
        alreadyBuild.push(id);
        html += '<div id="' + id + '" class="notification ' + notification.theme + '">';
        html += '<div onclick="handleNotification(\'' + notification.id + '\')" class="pointer">';
        html += '<h3>' + notification.title + '</h3>';
        html += '<p>' + notification.message + '</p>';
        html += '<div class="bottom">';
        if (notification.link.length !== 0) {
            html += '<small>' + notification.link + '</small>';
        }
        html += '</div></div></div>';
        oneTimers[id] = notification;
        currentUser.removeNotification(notification.id);
        return true;
    })


    //other
    Object.keys(notifications).every(id => {
        if (alreadyBuild.includes(id)) return true;
        alreadyBuild.push(id);
        const notification = notifications[id];
        html += '<div id="' + id + '" class="notification ' + notification.theme + '">';
        html += '<div class="top-right"><a onclick="removeNotification(\'' + id + '\')">x</a></div>'
        html += '<div onclick="handleNotification(\'' + notification.id + '\')" class="pointer">';
        html += '<h3>' + notification.title + '</h3>';
        html += '<p>' + notification.message + '</p>';
        html += '<div class="bottom">';
        if (notification.link.length !== 0) {
            html += '<small>' + notification.link + '</small>';
        }
        html += '</div></div></div>';
        return true;
    })
    setHTML("notificationsBox", html);
}

function reloadNotifications(btn){
    btn = $(btn);
    btn.attr("disabled",true);
    btn.attr("class","btn btn-secondary material-symbols-outlined inline-icon");
    console.log("RELOAD DATA")
    loadData(()=>{
        console.log("RELOADED")
        updateNotifications();
        setTimeout(()=>{
            btn.attr("class","btn btn-success material-symbols-outlined inline-icon");
            setTimeout(()=>{
                btn.attr("class","btn btn-info material-symbols-outlined inline-icon");
                btn.attr("disabled",false);
            },4000)
        },500)
    },dataTypes.USER)
}

function handleNotification(id) {
    var notification = currentUser.getNotification(id);
    if (notification === undefined) notification = oneTimers[id];
    if (notification === undefined) return;
    if (notification.closeOnClick) {
        currentUser.removeNotification(id, () => {
            updateNotifications();
            if (notification.link !== undefined && notification.link.length !== 0) {
                if (notification.link.startsWith("/")) {
                    loadSite(notification.link.replace(/\//g, ''));
                } else {
                    window.open(notification.link, '_blank').focus();
                }
            }
        });
    } else {
        if (notification.link !== undefined && notification.link.length !== 0) {
            if (notification.link.startsWith("/")) {
                loadSite(notification.link.replace(/\//g, ''));
            } else {
                window.open(notification.link, '_blank').focus();
            }
        }
    }

}

function removeNotification(id) {
    document.getElementById(id).remove();
    currentUser.removeNotification(id, () => {
        updateNotifications();
    });
}