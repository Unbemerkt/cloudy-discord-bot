const loadings = [];
function addLoading(id){
    if (!loadings.includes(id)){
        loadings.push(id);
        updateIndicator();
    }
}

function removeLoading(id){
    if (loadings.includes(id)){
        loadings.remove(id);
        updateIndicator();
    }
}

function updateIndicator(){
    if (loadings.length===0){
        $(".loadIndicatorElement").hide();
    }else{
        $(".loadIndicatorElement").show();
    }
}