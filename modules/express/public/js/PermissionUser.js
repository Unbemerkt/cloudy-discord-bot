class PermissionUser {
    jsonData = {}
    permissionManager;
    discord = {}
    name = ""
    groups = []
    permissions = []
    groupsPermissions = []
    web = {}
    notifications = {}

    constructor(jsonData, permissionManager) {
        this.permissionManager = permissionManager;
        this.jsonData = jsonData;
        this.discord = jsonData.discord;
        this.name = jsonData.name;
        if (jsonData.web === undefined) {
            this.web = {
                canLogin: true,
                password: "",
                token: "",
                timeout: 0
            }

        } else {
            this.web = jsonData.web;
        }
        this.permissions = jsonData.permissions || [];
        this.notifications = jsonData.notifications || {};
        this.groups = jsonData.groups || [];
        this.groupsPermissions = jsonData.groupsPermissions || [];
        this.loadGroupPermissions();
    }

    equal(userObject) {
        if (userObject.name !== this.name) return false;
        //check discord
        if (userObject.discord.userID !== this.discord.userID) return false;
        if (userObject.groups.length !== this.groups.length) return false;
        var notificationChange = false;
        if (Object.keys(userObject.notifications).length !== Object.keys(this.notifications).length) return false;
        Object.keys(userObject.notifications).every(id=>{
            if (this.notifications[id]!==undefined){
                const notification = this.notifications[id];
                const newNotification = userObject.notifications[id];
                if (notification.title!==newNotification.title){
                    notificationChange = true;
                    return false;
                }
                if (notification.message!==newNotification.message){
                    notificationChange = true;
                    return false;
                }
                if (notification.link!==newNotification.link){
                    notificationChange = true;
                    return false;
                }
                if (notification.theme!==newNotification.theme){
                    notificationChange = true;
                    return false;
                }
                return true;
            }
            notificationChange = true;
            return false;
        })
        if (notificationChange)return false;
        if (userObject.permissions.length !== this.permissions.length) return false;
        if (userObject.groupsPermissions.length !== this.groupsPermissions.length) return false;
        if (userObject.web.canLogin !== this.web.canLogin) return false;
        if (userObject.web.password !== this.web.password) return false;
        if (userObject.web.token !== this.web.token) return false;
        return userObject.web.timeout === this.web.timeout;

    }

    getNotifications() {
        return this.notifications;
    }

    getNotification(id) {
        return this.notifications[id];
    }

    removeNotification(id, cb) {
        $.post("/user", {
            action: "CLOSE_NOTIFICATION",
            userData: id
        }, (result) => {
            console.log(result)
            if (result.type === "OK") {
                delete this.notifications[id];
            }
            if (cb) cb(result);
        });
    }

    addPermission(permission) {
        if (permission === undefined) return false;
        if (this.permissions.includes(permission)) return false;
        this.permissions.push(permission);
        return true;
    }

    removePermission(permission) {
        if (!this.permissions.includes(permission)) return false;
        if (this.permissions.length === 1) {
            this.permissions = [];
            return true;
        }
        const index = this.permissions.indexOf(permission);
        this.permissions.splice(index, 1);
        return true;
    }

    getPermissions() {
        return this.permissions;
    }

    getAllPermissions() {
        var permList = this.getPermissions();
        if (this.getGroupsPermissions().length > 0) {
            this.getGroupsPermissions().forEach(perm => {
                if (!this.getPermissions().includes(perm)) {
                    permList.push(perm);
                }
            })
        }
        return permList;
    }

    setDiscordID(id) {
        this.discord.userID = id;
    }

    getJsonData() {
        return this.jsonData;
    }

    isInGroup(groupName) {
        return this.groups.includes(groupName);
    }

    getGroups() {
        return this.groups;
    }

    getPassword() {
        return this.web.password;
    }

    isWebEnabled() {
        return !(this.web === undefined || (this.web.canLogin !== undefined && !this.web.canLogin));
    }

    getToken() {
        return this.web.token;
    }

    getLinkToken(){
        return this.discord.linkToken;
    }

    setPassword(password) {
        this.web.password = password;
    }

    addGroup(group) {
        if (typeof group === "string") {
            if (this.groups.includes(group)) return false;
            this.groups.push(group);
            this.loadGroupPermissions()
            return true;
        }
        if (this.groups.includes(group.getName())) return false;
        this.groups.push(group.getName());
        this.loadGroupPermissions()
        return true;
    }

    removeGroup(group) {
        if (typeof group === "string") {
            if (!this.groups.includes(group)) return false;
            this.groups.remove(group)
            this.loadGroupPermissions()
            return true;
        }
        if (!this.groups.includes(group.getName())) return false;
        this.groups.remove(group.getName())
        this.loadGroupPermissions()
        return true;
    }

    loadGroupPermissions() {
        this.groupsPermissions = [];
        if (this.permissionManager === undefined) {
            console.error("[" + this.getName() + "] PermissionManager is undefined!");
            return;
        }
        this.groups.forEach(group => {
            const tmpGroup = this.permissionManager.getGroup(group);
            if (tmpGroup !== undefined) {
                tmpGroup.getPermissions().forEach(perm => {
                    if (!this.getGroupsPermissions().includes(perm)) this.groupsPermissions.push(perm)
                });
                tmpGroup.getImportPermissions().forEach(perm => {
                    if (!this.getGroupsPermissions().includes(perm)) this.groupsPermissions.push(perm)
                });
            }
        });
    }

    getGroupsPermissions() {
        return this.groupsPermissions;
    }

    isPermissionExcluded(permission) {
        permission = "-" + permission;
        if (this.permissions.includes(permission)) return true;
        if (this.groupsPermissions.includes(permission)) return true;
        return false;
    }

    hasPermission(permission) {
        if (this.isPermissionExcluded(permission)) return false;
        if (this.permissions.includes("*")) return true;
        if (this.groupsPermissions.includes("*")) return true;
        if (this.permissions.includes(permission)) return true;
        if (!this.groupsPermissions.includes(permission)) {
            const tmp = permission.split(".");
            if (tmp.length > 1) {
                try {
                    var count = tmp.length - 1;
                    for (let i = count; i > 0; i--) {
                        var permStr = "";
                        if (i === count) {
                            tmp[i] = "*";
                        } else {
                            tmp.pop();
                            tmp[i] = "*";
                        }
                        for (let j = 0; j < tmp.length; j++) {
                            if (j === 0) {
                                permStr = tmp[j];
                            } else {
                                permStr += "." + tmp[j];
                            }
                        }
                        if (this.permissions.includes(permStr) || this.groupsPermissions.includes(permStr)) return true;
                    }
                } catch (e) {
                    console.error(e);
                }
            } else {
                var permStr = tmp[0] + ".*";
                if (this.permissions.includes(permStr) || this.groupsPermissions.includes(permStr)) return true;
            }
            return false;
        }
        return true;
    }

    hasPermissions(permissions, mustHaveAll) {
        if (permissions.length === 0) return true;
        if (this.permissions.length === 0 && this.groupsPermissions.length === 0) return false;
        var ii = 0;
        for (let i = 0; i < permissions.length; i++) {
            if (this.hasPermission(permissions[i])) {
                if (mustHaveAll) {
                    ii++;
                } else {
                    return true;
                }
            }
        }
        if (mustHaveAll) {
            return permissions.length === ii;
        }
        return false;
    }

    getName() {
        return this.name;
    }

    setWebEnabled(bool) {
        this.web.canLogin = bool;
        return true;
    }

    getId() {
        return this.discord.userID;
    }

    getLinkToken() {
        return this.discord.linkToken;
    }

    save(cb) {
        //save group to file
        this.jsonData.groups = this.groups;
        this.jsonData.permissions = this.permissions;
        this.jsonData.discord = this.discord;
        //send data
        $.post("/user", {
            action: "SAVE",
            userData: JSON.stringify(this.jsonData)
        }, (result) => {
            if (cb) cb(result);
        });
    }

    startLink(cb) {
        $.post("/user", {
            action: "START_LINK",
            userData: this.getName()
        }, (result) => {
            this.discord.linkToken = result.token;
            if (cb) cb(result);
        });
    }

    removeLink(cb){
        $.post("/user", {
            action: "REMOVE_LINK",
            userData: this.getName()
        }, (result) => {
            this.discord.userID = "";
            this.discord.linkToken = "";
            if (cb) cb(result);
        });
    }

    delete(cb) {
        //send request
        $.post("/user", {
            action: "DELETE",
            userData: this.getName()
        }, (result) => {
            if (result.type === "OK") {
                this.permissionManager.removeUser(this.getName());
            }
            if (cb) cb(result);
        });
    }

    create(cb) {
        $.post("/user", {
            action: "CREATE",
            userData: JSON.stringify(this.jsonData)
        }, (result) => {
            if (cb) cb(result);
        });
    }
}