function initNavigator() {
    const navs = document.querySelectorAll("[data-nav]");
    navs.forEach(nav => {
        nav.addEventListener("click", (e) => {
            loadSite(e.target.getAttribute("data-nav"))
        });
    })

    shortcut.add("CTRL+ALT+C",()=>{
        loadSite("crashes");
    });

    var href = window.location.href;
    if (href.includes("#")){
        loadSite(href.split("#")[1]);
    }else{
        loadSite("home");
    }
    overlay.hide("INIT");

}
var currentSite = "";
var unsaved = false;

function showError(reason){
    if (reason!==undefined){
        setHTML("error_reason",reason)
        $("#error_reasonDiv").show();
    }else{
        $("#error_reasonDiv").hide();
    }
    $("#contentLoading").hide();
    $("#content").hide();
    $("#error_container").show();
}

function loadSite(site) {
    var loadID = "LOADING_SITE";
    const contentDiv = $("#content");
    const errorDiv = $("#error_container");
    const loadingDiv = $("#contentLoading");
    const title = $("#title");
    // if (currentSite===site)return;
    if (unsaved){
        const result = confirm("You may have some unsaved data, do you want to continue?");
        if (!result)return;
        unsaved = false;
    }
    contentDiv.hide();
    errorDiv.hide();
    loadingDiv.show();
    addLoading(loadID)
    title.html(document.getElementById("title").getAttribute("data-title")+" » Loading...");
    var href = window.location.href;
    if (site!=="error"&&site!=="account"&&(currentUser===undefined||!currentUser.hasPermission("web."+site))){
        if (!href.includes("#")){
            window.location.href = href + "#" + site;
        }else{
            window.location.href = href.split("#")[0] + "#" + site;
        }
        contentDiv.load("/pages/noPermission.html",()=>{
            setTimeout(()=>{
                removeLoading(loadID)
                title.html(document.getElementById("title").getAttribute("data-title")+" » "+(site.toUpperCase()));
                loadingDiv.hide();
                errorDiv.hide();
                contentDiv.show();
            },250);
        });
        return;
    }
    triggerNavEvents(currentSite,site);
    currentSite = site;

    if (href.includes("#")){
        window.location.href = href.split("#")[0] + "#" + site;
    }else{
        window.location.href = href + "#" + site;
    }
    contentDiv.load("/pages/" + site + ".html",()=>{
        setTimeout(()=>{
            removeLoading(loadID)
            title.html(document.getElementById("title").getAttribute("data-title")+" » "+(site.toUpperCase()));
            loadingDiv.hide();
            errorDiv.hide();
            contentDiv.show();
        },250);
    });
}
var navListener = {};
function registerNavigatorListener(id, cb) {
    navListener[id]=cb;
}

function removeNavigatorListener(id) {
    if (navListener[id]!==undefined)delete navListener[id];
}
function triggerNavEvents(old,newSite){
    Object.keys(navListener).forEach(id=>{
        navListener[id](old,newSite);
    })
}

