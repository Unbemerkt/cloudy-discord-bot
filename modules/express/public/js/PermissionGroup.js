class PermissionGroup {
    jsonData = {}
    name = ""
    permissions = []
    importFrom = []
    importPermissions =  []
    permissionManager
    discord = {}
    constructor(jsonData, permissionManager) {
        this.jsonData = jsonData;
        this.name = jsonData.name;
        this.permissions = jsonData.permissions||[];
        this.importFrom = jsonData.importFrom||[];
        this.discord = jsonData.discord||{};
        this.permissionManager = permissionManager;
    }
    addPermission(permission){
        if (permission===undefined)return false;
        if (this.permissions.includes(permission))return false;
        this.permissions.push(permission);
        return true;
    }

    updateUsers(){
        Object.keys(this.permissionManager.getUsers()).forEach(userName=>{
            var user = this.permissionManager.getUser(userName);
            if (user.getGroups().includes(this.getName())){
                user.loadGroupPermissions();
            }
        });
    }

    getDiscordData(){
        return this.discord;
    }

    addRole(guildID, roleID){
        if (this.discord[guildID]===undefined){
            this.discord[guildID] = [];
        }
        if (!this.discord[guildID].includes(roleID)){
            this.discord[guildID].push(roleID);
        }
    }

    removeRole(guildID, roleID){
        if (this.discord[guildID]===undefined)return;
        if (this.discord[guildID].includes(roleID))this.discord[guildID].remove(roleID);
        if (this.discord[guildID].length===0)delete this.discord[guildID];
    }

    loadImportPermissions(){
        this.importPermissions = [];
        this.importFrom.forEach(gName=>{
            const tmpGroup = this.permissionManager.getGroup(gName);
            if (tmpGroup!==undefined){
                tmpGroup.getPermissions().forEach(perm=>{
                    if (!this.importPermissions.includes(perm))this.importPermissions.push(perm)
                });
            }
        })
    }

    removePermission(permission){
        if (!this.permissions.includes(permission))return false;
        const index = this.permissions.indexOf(permission);
        this.permissions.splice(index, 1);
        return true;
    }
    getPermissions(){
        return this.permissions;
    }

    getJsonData(){
        return this.jsonData;
    }

    getImports(){
        return this.importFrom;
    }
    addImport(group){
        if (typeof group === "string"){
            if (this.importFrom.includes(group))return false;
            this.importFrom.push(group);
            this.loadImportPermissions();
            return true;
        }
        if (this.importFrom.includes(group.getName()))return false;
        this.importFrom.push(group.getName());
        this.loadImportPermissions();
        return true;
    }

    removeImport(group){
        if (typeof group === "string"){
            if (!this.importFrom.includes(group))return false;
            this.importFrom.remove(group);
            this.loadImportPermissions();
            return true;
        }
        if (!this.importFrom.includes(group.getName()))return false;
        this.importFrom.remove(group.getName());
        this.loadImportPermissions();
        return true;
    }
    getImportPermissions(){
        return this.importPermissions;
    }

    getName(){
        return this.name;
    }


    save(cb){
        this.updateUsers();
        //save group to file
        this.jsonData.importFrom = this.importFrom;
        this.jsonData.permissions = this.permissions;
        this.jsonData.discord = this.discord;
        //send data
        $.post("/group", {
            action: "SAVE",
            groupData: JSON.stringify(this.jsonData)
        }, (result) => {
            console.log(result);
            if (cb) cb(result);
        });
    }

    delete(cb){
        //send request

        $.post("/group", {
            action: "DELETE",
            groupData: this.getName()
        }, (result) => {
            if (result.type==="OK"){
                this.permissionManager.removeGroup(this.getName());
                this.updateUsers();
            }
            if (cb) cb(result);
        });
    }

    create(cb) {
        $.post("/group", {
            action: "CREATE",
            groupData: JSON.stringify(this.jsonData)
        }, (result) => {
            if (cb) cb(result);
        });
    }
}