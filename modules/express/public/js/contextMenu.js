class ContextMenu {
    id
    targets
    onCloseCB
    constructor(id, targets) {
        this.id = id;
        this.targets = targets;
        this.listen();
    }

    listen() {
        const menuID = this.id + "-menu";
        this.targets.forEach(element => {
            this.addTarget(element);
        });
        document.getElementsByTagName("body")[0].addEventListener("click", (event) => {
            if (!hasAttribute(event.target,"data-context")&&elementExists(menuID)){
                document.getElementById(menuID).className = "context hide";
                if (this.onCloseCB)this.onCloseCB();
            }
        })
    }
    addTarget(element){
        const menuID = this.id + "-menu";
        element.addEventListener("contextmenu", (event) => {
            this.build(event.currentTarget);
            if (!elementExists(menuID)) return;
            event.preventDefault();
            document.getElementById(menuID).className = "context show";
            document.getElementById(menuID).style.top = this.mouseY(event) + 'px';
            document.getElementById(menuID).style.left = this.mouseX(event) + 'px';
        })
    }

    setBuilder(builder) {
        this.builder = builder;
    }

    build(target) {
        var menu;
        if (!elementExists(this.id + "-menu")) {
            menu = document.createElement("div");
            menu.id = this.id + "-menu";
            document.getElementsByTagName("body")[0].appendChild(menu);

        }
        menu = document.getElementById(this.id + "-menu");

        if (!this.builder) {
            console.error("[" + this.id + "] No builder set!")
            return;
        }
        const html = this.builder(target, menu, this);
        if (html===undefined){
            menu.setAttribute("style","display: none;");
        }else{
            menu.innerHTML = html;
        }

    }

    close(){
        const menuID = this.id + "-menu";
        if (elementExists(menuID)){
            document.getElementById(menuID).className = "context hide";
            if (this.onCloseCB)this.onCloseCB();
        }
    }

    mouseX(evt) {
        if (evt.pageX) {
            return evt.pageX;
        } else if (evt.clientX) {
            return evt.clientX + (document.documentElement.scrollLeft ?
                document.documentElement.scrollLeft :
                document.body.scrollLeft);
        } else {
            return null;
        }
    }

    mouseY(evt) {
        if (evt.pageY) {
            return evt.pageY;
        } else if (evt.clientY) {
            return evt.clientY + (document.documentElement.scrollTop ?
                document.documentElement.scrollTop :
                document.body.scrollTop);
        } else {
            return null;
        }
    }

    setOnClose(cb) {
        this.onCloseCB = cb;
    }
}

