const moduleLoader = require("./../../moduleLoader");
const config = require("./config.json");
const fs = require("fs");
const {SettingsRow, SettingsBuilder, SettingsContainer, SettingsResult} = require("../settings/module");
var settingsModule;
var expressModule;
module.exports = {
    init(mod){
        expressModule = mod;
        if (settingsModule===undefined){
            settingsModule = moduleLoader.getModule("settings");
        }
        settingsModule.registerUpdateFunction(expressModule.moduleInfo.name, module.exports.updateSettings);
        this.updateSettings();
    },
    updateSettings(){
        const builder = new SettingsBuilder(expressModule.moduleInfo);
        const row_1 = new SettingsRow();
        row_1.addString("title","Title",config.title,"Cloudy",false,"",true)
        const row_2 = new SettingsRow();
        row_2.addNumber("port","port",config.port,8080,"",undefined,undefined,true)
        row_2.addString("bindHost","bindHost",config.bindHost,"Cloudy",false,"",false)

        builder.add(row_1)
        builder.add(row_2)

        builder.add(new SettingsContainer("dev").add(new SettingsRow().addSelect("devMode","devMode",
            config.devMode.toString(),[{label: "true", value: "true"},{label: "false", value: "false"}],
            "*Note: Please restart the app for the settings to take effect!","")))

        builder.setCallback(module.exports.receiveSettings);
        settingsModule.registerSettingsBuilder(builder);
    },
    receiveSettings(settingsResult) {
        config.title = settingsResult.getString("title");
        config.port = settingsResult.getNumber("port");
        config.devMode = settingsResult.getSelect("devMode")==="true";
        config.bindHost = settingsResult.getString("bindHost");
        expressModule.config = config;
        fs.writeFileSync(__dirname+"/config.json",JSON.stringify(config,null,2));
    }
}