const express = require('express');
const router = express.Router();
const fs = require("fs");
const moduleLoader = require("./../../../../moduleLoader");
const {ModuleUtils} = require("../../../../ModuleUtils");
const app = require("../../module");
var consoleModule;
var permissionModule;
const path = __dirname + "/../../../../crash-logs/";
router.routeInfo = {
    path: "/crashLog"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req, res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }
    const result = []
    fs.readdir(path, (err, files) => {
        if (err) {
            new ModuleUtils().error(err.toString());
            res.json({type: "ERR", data: err.toString()})
        } else {
            files.forEach(file=>{
                if (file.endsWith(".html")){
                    const rawLogsFileName = file.replace(".html",".txt");
                    if (fs.existsSync(path+rawLogsFileName)){
                        result.push({html: file, raw: rawLogsFileName});
                    }else{
                        result.push(file);
                    }
                }
            })
            res.json({type: "OK", data: result, size: result.length});
        }
    })

});


router.post("/", function (req, res, next) {
    if (!app.validateRequest(req, res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    if (req.body.log === undefined) {
        res.json({type: "ERR", data: "log is missing!"})
        return;
    }
    const log = req.body.log;
    if (req.body.action === undefined) {
        res.json({type: "ERR", data: "action is missing!"})
        return;
    }
    switch (req.body.action){
        case "get":
            console.log(path+log)
            if (!fs.existsSync(path+log)){
                res.json({type: "ERR", data: "INVALID_LOG"})
                return;
            }
            fs.readFile(path+log,(err,data)=>{
                if (err) {
                    new ModuleUtils().error(err.toString());
                    res.json({type: "ERR", data: err.toString()})
                } else {
                    if (log.endsWith(".txt")){
                        data = data.toString().replaceAll(/\n/g,"<br>")
                    }
                    res.send(data);
                }
            })
            break
        case "delete":
            if (!fs.existsSync(path+log)){
                res.json({type: "ERR", data: "INVALID_LOG"})
                return;
            }
            fs.unlink(path+log,(err)=>{
                if (err) {
                    new ModuleUtils().error(err.toString());
                    res.json({type: "ERR", data: err.toString()})
                } else {
                    res.json({type: "OK", data: "LOG_DELETED"});
                }
            });
            break
        default:
            res.json({type: "ERR", data: "INVALID_ACTION"})
            break;
    }
});
module.exports = router;
