const express = require('express');
const router = express.Router();

const moduleLoader = require("./../../../../moduleLoader");
const {ModuleUtils} = require("../../../../ModuleUtils");
const app = require("../../module");
const AutocompleteManager = require("../../../discordMusic/AutocompleteManager");
var consoleModule;
var permissionModule;
router.routeInfo = {
    path: "/console"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }
    try {
        res.send(moduleLoader.moduleUtils.getHtmlLog());
    } catch (e) {
        new ModuleUtils().error(e);
        res.json({type: "ERR", data: e})
    }

});

router.post("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    if (consoleModule === undefined) {
        consoleModule = moduleLoader.getModule("console");
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    try {
        if (req.body.autocomplete!==undefined){
            var result = consoleModule.getAutocomplete(req.body.autocomplete,user);
            result["type"] = "OK";
            res.json(result);
            return;
        }
        if (req.body.command === undefined) {
            res.json({type: "ERR", data: "command is undefined!"})
            return;
        }

        if (req.body.command === "getCommands") {
            res.json({type: "OK", commands: consoleModule.consoleCommands})
            return;
        }

        if (consoleModule.runCommand(req.body.command,user)){
            res.json({type: "OK", command: req.body.command})
        }else{
            res.json({type: "ERR", command: req.body.command, data: "COMMAND_FAILED"})
        }

    } catch (e) {
        new ModuleUtils().error(e);
        res.json({type: "ERR", data: e})
    }
});
module.exports = router;
