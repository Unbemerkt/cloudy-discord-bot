const express = require('express');
const app = require('../../module.js');
const router = express.Router();
const config = app.config;
const moduleLoader = require("../../../../moduleLoader");
var expressModule;
router.routeInfo = {
    path: "/shell"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (expressModule === undefined) {
        expressModule = moduleLoader.getModule("ExpressCore");
    }
    expressModule.moduleUtils.warn("Request to /shell detected! Sending response... :D");
    res.json({data: "TF u doing!?!??"});
});
router.post("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (expressModule === undefined) {
        expressModule = moduleLoader.getModule("ExpressCore");
    }
    expressModule.moduleUtils.warn("POST Request to /shell detected! Sending response... :D");
    res.json({data: "TF u doing!?!??"});
});
module.exports = router;
