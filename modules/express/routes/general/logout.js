const express = require('express');
const router = express.Router();


router.routeInfo = {
    path: "/logout"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    res.clearCookie("TOKEN");
    res.redirect("/login");
});


router.post("/", function (req, res, next) {
    res.json({type: "ERR", data: "ONLY_GET"})
});
module.exports = router;
