const express = require('express');
const fs = require('fs');
const app = require('../../module.js');
const router = express.Router();
const config = app.config;
router.routeInfo = {
    path: "/imprint"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (!fs.existsSync(__dirname+"/../../views/imprint.html")){
        fs.writeFileSync(__dirname+"/../../views/imprint.html","This imprint will be available soon!")
    }
    res.render("imprint.html", {title: config.title});
});
router.post("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    res.json({type: "ERR", data: "ONLY_GET"});
});

module.exports = router;
