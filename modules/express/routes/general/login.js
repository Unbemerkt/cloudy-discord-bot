const express = require('express');
const app = require('../../module.js');
const router = express.Router();
const config = app.config;

const moduleLoader = require("./../../../../moduleLoader");
const {ModuleUtils} = require("../../../../ModuleUtils");
var permissionModule;

router.routeInfo = {
    path: "/login"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule===undefined){
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token!==undefined){
        const user = permissionModule.authenticateToken(token);
        if (user!==undefined){
            res.redirect("/");
            return;
        }
    }


    res.render("login.html", {title: config.title});

});


router.post("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule===undefined){
        permissionModule = moduleLoader.getModule("permissions");
    }
    const user = permissionModule.authenticate(req.body.username, req.body.password);
    if (user===undefined){
        res.json({type: "ERR", data: "LOGIN_FAILED"})
        return;
    }
    res.json({type: "OK", data: user.getToken()})
});
module.exports = router;
