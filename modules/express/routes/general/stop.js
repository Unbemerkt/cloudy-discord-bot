const express = require('express');
const app = require('../../module.js');
const dCore = require('../../../discordCore/module.js');
const router = express.Router();
const config = app.config;
const path = require("path");

const moduleLoader = require("./../../../../moduleLoader");
var discordModule;
var permissionModule;
router.routeInfo = {
    path: "/stop"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    if (discordModule === undefined) {
        discordModule = moduleLoader.getModule("discordCore");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }

    if (!user.hasPermission("app.shutdown")){
        res.json({type: "ERR", data: "NO_PERMISSION", permission: "app.shutdown"})
        return;
    }
    res.json({type: "OK", data: "SHUTTING_DOWN"});
    moduleLoader.shutdown();
});
module.exports = router;
