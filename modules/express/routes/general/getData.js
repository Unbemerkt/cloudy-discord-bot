const express = require('express');
const app = require('../../module.js');
const dCore = require('../../../discordCore/module.js');
const router = express.Router();
const config = app.config;
const path = require("path");

const moduleLoader = require("./../../../../moduleLoader");
var discordModule;
var expressModule;
var permissionModule;
router.routeInfo = {
    path: "/getData"
}
/* GET home page. */
router.get("/:request", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    if (discordModule === undefined) {
        discordModule = moduleLoader.getModule("discordCore");
    }
    if (expressModule === undefined) {
        expressModule = moduleLoader.getModule("ExpressCore");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }
    const dataRequest = req.params.request||"ALL";
    const resultMap = {
        type: "OK"
    };
    try {
        switch (dataRequest) {
            case "BOT":
                resultMap.botData = {
                    devMode: config.devMode,
                    name: "Cloudy",
                    onlineState: dCore.getCurrentState(),
                    modules: {
                        loaded: moduleLoader.getSimpleModules(),
                        registered: moduleLoader.getRegisteredSimpleModules(),
                        unloaded: moduleLoader.unloaded
                    }
                }
                break;
            case "DISCORD":
                resultMap.discordData = discordModule.getGuilds();
                break;
            case "USER":
                resultMap.userData = user.getJsonData();
                break;
            case "MODULES_NAV":
                var tmp = [];
                Object.keys(expressModule.moduleRoutes).forEach(moduleName=>{
                    expressModule.moduleRoutes[moduleName].forEach(info=>{
                        if (info.nav!==undefined){
                            tmp.push({
                                nav: info.nav,
                                path: info.path,
                                right: info.right
                            })
                        }
                    })
                })
                resultMap.modulesNav = tmp
                break
            case "ALL":
            default:
                resultMap.botData = {
                    devMode: config.devMode,
                    name: "Cloudy",
                    onlineState: dCore.getCurrentState(),
                    modules: {
                        loaded: moduleLoader.getSimpleModules(),
                        registered: moduleLoader.getRegisteredSimpleModules(),
                        unloaded: moduleLoader.unloaded
                    }
                }
                resultMap.userData = user.getJsonData();
                resultMap.discordData = discordModule.getGuilds();
                break;
        }
        res.json(resultMap);
    }catch (e){
        console.error(e);
        res.json({type: "ERR", data: e})
    }
});
module.exports = router;
