const express = require('express');
const app = require('../../module.js');
const router = express.Router();
const config = app.config;
const moduleLoader = require("../../../../moduleLoader");
var permissionModule;
router.routeInfo = {
    path: "/"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.redirect("/login");
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        console.log("user not found by token!")
        res.redirect("/login");
        return;
    }
    res.render("index.html", {title: config.title});
});
router.post("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    res.json({type: "ERR", data: "ONLY_GET"});
});

module.exports = router;
