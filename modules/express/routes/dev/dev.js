const express = require('express');
const app = require('../../module.js');
const router = express.Router();
const config = app.config;

const moduleLoader = require("./../../../../moduleLoader");
const {ModuleUtils} = require("../../../../ModuleUtils");
var permissionModule;
router.routeInfo = {
    path: "/dev"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID"})
        return;
    }
        if (!config.devMode){
            res.json({type: "ERR", data: "DevMode is disabled!"})
            return;
        }
    res.json({type: "ERR", data: "GET requests forbidden!"})
});


router.post("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID"})
        return;
    }
    if (!config.devMode){
        res.json({type: "ERR", data: "DevMode is disabled!"})
        return;
    }
    if (req.body.action===undefined){
        res.json({type: "ERR", data: "action is undefined!"})
        return;
    }
    try {
        console.log(req.body.action);
        switch (req.body.action) {
            case "RL_CMDS":
                moduleLoader.getModule("Discord CMD Manager").reloadALLCommands();
                res.json({type: "OK"})
                return;
            case "RG_CMDS":
                //reload commands
                require("../../../discordCommandManager/commandRefresher").refresh(new ModuleUtils(moduleLoader.getModule("Discord CMD Manager").moduleInfo))
                break;
            case "RG_CMDS_G":
                //reload GLOBAL commands
                require("../../../discordCommandManager/commandRefresher").refreshGlobal(new ModuleUtils(moduleLoader.getModule("Discord CMD Manager").moduleInfo))
                break;
        }
        res.json({type:"OK"})
    }catch (e){
        console.error(e);
        res.json({type:"ERR", data: e})
    }

});
module.exports = router;
