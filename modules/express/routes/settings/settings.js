const express = require('express');
const router = express.Router();

const moduleLoader = require("./../../../../moduleLoader");
const {ModuleUtils} = require("../../../../ModuleUtils");
const app = require("../../module");
var settingsModule;
var permissionModule;
router.routeInfo = {
    path: "/settings"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    if (settingsModule === undefined) {
        settingsModule = moduleLoader.getModule("settings");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }
    if (!user.hasPermission("web.settings")) {
        res.json({type: "ERR", data: "NO_PERMISSIONS", permission: "web.settings"})
        return;
    }
    try {
        res.json({type: "OK", data: settingsModule.getSettings(true)});
    } catch (e) {
        new ModuleUtils().error(e);
        res.json({type: "ERR", data: e})
    }
});


router.post("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    if (settingsModule === undefined) {
        settingsModule = moduleLoader.getModule("settings");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    if (!user.hasPermission("web.settings")) {
        res.json({type: "ERR", data: "NO_PERMISSIONS", permission: "web.settings"})
        return;
    }
    if (req.body.action!==undefined&&req.body.action==="UPDATE"){
        try{
            settingsModule.updateSettings();
            res.json({type: "OK", data: "SETTINGS_UPDATED"})
        }catch (e){
            res.json({type: "ERR", data: e.toString()})
            console.error(e)
        }
        return;
    }
    if (req.body.settingsID===undefined||req.body.resultMap===undefined){
        res.json({type: "ERR", data: "MISSING_ARGUMENTS"})
        return;
    }

    try {
        const setting = settingsModule.getSetting(req.body.settingsID);
        if (setting===undefined){
            res.json({type: "ERR", data: "INVALID_SETTINGS"})
            return;
        }
        console.log(JSON.parse(req.body.resultMap))
        setting.resolve(new settingsModule.SettingsResult(req.body.settingsID,JSON.parse(req.body.resultMap)))
        settingsModule.updateSetting(req.body.settingsID);
        res.json({type: "OK", data: "SETTINGS_RECEIVED"})
    } catch (e) {
        new ModuleUtils().error(e);
        console.error(e);
        res.json({type: "ERR", data: e.toString()})
    }
});
module.exports = router;
