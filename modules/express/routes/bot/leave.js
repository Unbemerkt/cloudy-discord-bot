const express = require('express');
const router = express.Router();

const moduleLoader = require("./../../../../moduleLoader");
const {ModuleUtils} = require("../../../../ModuleUtils");
const app = require("../../module");
var discordModule;
var permissionModule;
router.routeInfo = {
    path: "/leave"
}
/* GET home page. */
router.get("/", function (req, res, next) {
        res.json({type: "ERR", data: "INVALID_METHODE"})
});


router.post("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    if (!user.hasPermission("discord.leave")){
        res.json({type: "ERR", data: "NO_PERMISSIONS", permission: "discord.leave"})
        return;
    }
    try {
        if (req.body.guildID === undefined) {
            res.json({type: "ERR", data: "guildID is undefined!"})
            return;
        }
        if (discordModule === undefined) {
            discordModule = moduleLoader.getModule("discordCore");
        }
        const guild = discordModule.getGuild(req.body.guildID);
        if (guild===undefined){
            res.json({type: "ERR", data: "INVALID_GUILD"})
            return;
        }
        guild.leave().then((g)=>{
            res.json({type: "OK", data: "GUILD_LEAVED", guildID: g.id})
        }).catch((err)=>{
            res.json({type: "ERR", data: err})
        });
    } catch (e) {
        new ModuleUtils().error(e);
        res.json({type: "ERR", data: e})
    }
});
module.exports = router;
