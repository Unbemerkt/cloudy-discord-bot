const express = require('express');
const router = express.Router();

const moduleLoader = require("./../../../../moduleLoader");
const app = require("../../module");
var permissionModule;
router.routeInfo = {
    path: "/groups"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }


    try {
        var groups = [];
        permissionModule.getAllGroups().forEach(g => {
            const gData = g.getJsonData();
            gData["importPermissions"] = g.getImportPermissions();
            groups.push(gData);
        })
        res.json({
            type: "OK",
            groups: groups
        });
    } catch (e) {
        console.error(e);
        res.json({type: "ERR", data: e})
    }
});
module.exports = router;
