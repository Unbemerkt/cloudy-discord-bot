const express = require('express');
const router = express.Router();

const moduleLoader = require("./../../../../moduleLoader");
const app = require("../../module");
var permissionModule;
router.routeInfo = {
    path: "/group"
}
/* GET home page. */
router.post("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID"})
        return;
    }
    const action = req.body.action;
    if (action===undefined){
        res.json({type: "ERR", data: "INVALID_REQUEST"})
        return;
    }
    try {
        var groupData = req.body.groupData;
        if (groupData===undefined){
            res.json({type: "ERR", data: "INVALID_GROUPDATA"})
            return;
        }
        if (action==="SAVE"){
            groupData = JSON.parse(req.body.groupData);
            const editGroup = permissionModule.getGroup(groupData.name);
            if (editGroup===undefined){
                res.json({type: "ERR", data: "INVALID_GROUP"})
                return;
            }
            if (!editGroup.setPermissions(groupData.permissions)) console.error("Couldn't save permissions")
            if (!editGroup.setImports(groupData.importFrom)) console.error("Couldn't save imports")
            if (!editGroup.setDiscordData(groupData.discord)) console.error("Couldn't save Discord data")
            editGroup.save();
            res.json({type: "OK", data: "GROUP_SAVED"})
            return;
        }else if (action==="DELETE"){
            const editGroup = permissionModule.getGroup(groupData);
            if (editGroup===undefined){
                res.json({type: "ERR", data: "INVALID_GROUP"})
                return;
            }
            editGroup.delete();
            res.json({type: "OK", data: "GROUP_DELETED"})
            return;
        }else if (action==="CREATE"){
            groupData = JSON.parse(req.body.groupData);
            groupData.name = groupData.name.toLowerCase();
            if (permissionModule.getGroup(groupData.name)!==undefined){
                res.json({type: "ERR", data: "GROUP_ALREADY_EXISTS"});
                return;
            }
            new permissionModule.PermissionGroup(groupData, permissionModule).save();
            res.json({type: "OK", data: "GROUP_CREATED"})
            return;
        }
        res.json({type: "ERR", data: "INVALID_ACTION"})
    }catch (e){
        res.json({type: "ERR", data: "REQUEST_FAILED", error: e})
        console.error(e);
    }
});
module.exports = router;
