const express = require('express');
const router = express.Router();

const moduleLoader = require("./../../../../moduleLoader");
const app = require("../../module");
var permissionModule;
router.routeInfo = {
    path: "/users"
}
/* GET home page. */
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }


    try {
        var users = [];
        permissionModule.getAllUsers().forEach(u => {
            const uData = u.getJsonData();
            uData["groupsPermissions"] = u.getGroupsPermissions();
            users.push(uData);
        })
        res.json({
            type: "OK",
            users: users
        });
    } catch (e) {
        console.error(e);
        res.json({type: "ERR", data: e})
    }
});
module.exports = router;
