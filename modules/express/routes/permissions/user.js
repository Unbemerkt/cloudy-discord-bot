const express = require('express');
const router = express.Router();
const moduleLoader = require("./../../../../moduleLoader");
const randomString = require("randomstring");
const app = require("../../module");
var permissionModule;
router.routeInfo = {
    path: "/user"
}
/* GET home page. */
router.post("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID"})
        return;
    }
    const action = req.body.action;
    if (action === undefined) {
        res.json({type: "ERR", data: "INVALID_REQUEST"})
        return;
    }
    try {
        var userData = req.body.userData;
        if (userData === undefined) {
            res.json({type: "ERR", data: "INVALID_USERDATA"})
            return;
        }
        var editUser;
        switch (action){
            case "SAVE":
                userData = JSON.parse(req.body.userData);
                if (userData.name === "console") {
                    res.json({type: "ERR", data: "INVALID_USER"})
                    return;
                }
                editUser = permissionModule.getUser(userData.name);
                if (editUser === undefined) {
                    res.json({type: "ERR", data: "INVALID_USER"})
                    return;
                }
                editUser.setDiscordID(userData.discord.userID);
                if (userData.web.password.length > 0) {
                    //save password
                    if (!editUser.setPassword(userData.web.password)) console.error("Couldn't save password")
                }
                if (!editUser.setWebEnabled(userData.web.canLogin)) console.error("Couldn't save canLogin (Password: " + editUser.getPassword() + ")")
                if (!editUser.setGroups(userData.groups)) console.error("Couldn't save groups")
                if (!editUser.setPermissions(userData.permissions)) console.error("Couldn't save permissions")
                editUser.save();
                res.json({type: "OK", data: "USER_SAVED"})
                return;
            case "DELETE":
                editUser = permissionModule.getUser(userData);
                if (userData === "console") {
                    res.json({type: "ERR", data: "INVALID_USER"})
                    return;
                }
                if (editUser === undefined) {
                    res.json({type: "ERR", data: "INVALID_USER"})
                    return;
                }
                editUser.delete();
                res.json({type: "OK", data: "USER_DELETED"})
                return;
            case "CREATE":
                userData = JSON.parse(req.body.userData);
                userData.name = userData.name.toLowerCase();
                if (permissionModule.getUser(userData.name) !== undefined) {
                    res.json({type: "ERR", data: "USER_ALREADY_EXISTS"});
                    return;
                }
                const newUser = new permissionModule.PermissionUser(userData, permissionModule);
                newUser.addNotification(new permissionModule.Notification("wlc_message","Welcome!", "Welcome " + newUser.getName() + "! You can now link your account with your discord account!", "/account", "blue"));
                newUser.save();
                res.json({type: "OK", data: "USER_CREATED"})
                return;
            case "CLOSE_NOTIFICATION":
                user.removeNotification(userData)
                user.save();
                res.json({type: "OK", data: "NOTIFICATION_CLOSED", id: userData})
                return;
            case "START_LINK":
                const str = randomString.generate(24);
                if (user.startLink(str)) {
                    res.json({type: "OK", data: "LINK_STARTED", token: str})
                }else{
                    res.json({type: "ERR", data: "FAILED_TO_START_LINK", token: str})
                }
                return;
            case "REMOVE_LINK":
                user.discord.linkToken = "";
                user.discord.userID = "";
                user.save();
                res.json({type: "OK", data: "LINK_REMOVED"})
                return;
            default:
                res.json({type: "ERR", data: "INVALID_ACTION"})
                return;
        }
    } catch (e) {
        res.json({type: "ERR", data: "REQUEST_FAILED", error: e})
        console.error(e);
    }
});
module.exports = router;
