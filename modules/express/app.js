const moduleLoader = require("./../../moduleLoader");
const {ModuleUtils} = require("../../ModuleUtils");
const config = require("./config.json");
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fs = require("fs");
const {engines} = require("express/lib/application");
let mustacheExpress = require('mustache-express');
var app = express();


module.exports = {
    config,
    moduleInfo: {},
    moduleUtils: undefined,
    registerRoutes(mI) {
        this.moduleInfo = mI;
        this.moduleUtils = new ModuleUtils(this.moduleInfo);
        app.set('views', path.join(__dirname, 'views'));
        app.set('view engine', 'html');
        app.engine('html', mustacheExpress());

        app.use(logger('dev'));
        app.use(express.json());
        app.use(express.urlencoded({extended: false}));
        app.use(cookieParser());
        app.use(express.static(path.join(__dirname, 'public')));

        // app.use('/', require('./routes/index'));
        //disable cache
        app.use(express.json({ limit: "5mb" }));
        app.set('etag', false)
        app.use((req, res, next) => {
            res.set('Cache-Control', 'no-store')
            next()
        })

        const routeFiles = fs.readdirSync(path.join(__dirname, 'routes'));
        for (const file of routeFiles) {
            const filePath = path.join(__dirname + "/routes", file);
            if (fs.lstatSync(filePath).isDirectory()){
                const categoryFiles = fs.readdirSync(filePath);
                for (const catFile of categoryFiles){
                    const catFilePath = path.join(filePath, catFile);
                    const routeFile = require(catFilePath);
                    // Set a new item in the Collection with the key as the command name and the value as the exported module
                    if ('routeInfo' in routeFile) {
                        app.use(routeFile.routeInfo.path,routeFile);
                        this.moduleUtils.log("Registered route for '"+routeFile.routeInfo.path+"' in category '"+file+"'");
                    } else {
                        this.moduleUtils.log(`[WARNING] The route at ${filePath} is missing a required "routeInfo" property.`);
                    }
                }
            }else{
                const routeFile = require(filePath);
                // Set a new item in the Collection with the key as the command name and the value as the exported module
                if ('routeInfo' in routeFile) {
                    app.use(routeFile.routeInfo.path,routeFile);
                    this.moduleUtils.log("Registered route for '"+routeFile.routeInfo.path+"'");
                } else {
                    this.moduleUtils.log(`[WARNING] The route at ${filePath} is missing a required "routeInfo" property.`);
                }
            }
        }


        // catch 404 and forward to error handler
        // app.use(function (req, res, next) {
        //     next(createError(404));
        // });

        // error handler
        app.use(function (err, req, res, next) {
            // set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = req.app.get('env') === 'development' ? err : {};

            // render the error page
            res.status(err.status || 500);
            res.render('error', {title: config.title+" » Error", error: err});
        });

    },
    app
};