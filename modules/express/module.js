#!/usr/bin/env node

const moduleLoader = require("./../../moduleLoader");
const {ModuleUtils} = require("../../ModuleUtils");
const config = require("./config.json");
const moduleInfo = {
    name: "ExpressCore",
    prio: 2,
    requiredModules: [
        // "discordCore",
        "settings"
    ],
    requiredNodeModules: [
        "discord.js",
        "express",
        "http-errors",
        "cookie-parser",
        "debug",
        "morgan",
        "mustache-express"
    ]
}
const moduleUtils = new ModuleUtils(moduleInfo);

/**
 * Module dependencies.
 */

var app = require('./app.js');
var debug = require('debug')('cloudy:server');
var http = require('http');
const express = require("express");
const path = require("path");
const logger = require("morgan");
const fs = require("fs");
const createError = require("http-errors");

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(config.port || '3000');
app.app.set('port', port);
/**
 * Create HTTP server.
 */

var server = http.createServer(app.app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port,process.env.ADDRESS);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}

function validateRequest(req,res){
    if (config.bindHost.length!==0){
        if (config.bindHost!==req.get("host")){
            res.json({type: "ERR", data: "INVALID_REQUEST"});
            // moduleUtils.warn("Invalid request! ("+req.get("host")+")");
            return false;
        }
    }
    return true;
}
const expressSettings = require("./ExpressSettings");
module.exports = {
    moduleInfo,
    config,
    moduleUtils,
    moduleRoutes: {},
    moduleRegistered: function () {

    },
    moduleLoaded: function () {

    },
    runModule: function () {
        expressSettings.init(module.exports)
        app.registerRoutes(moduleInfo);
    },
    validateRequest,
    registerModulePage(moduleInfo, routeFilePath){
        if (this.moduleRoutes[moduleInfo.name]===undefined){
            this.moduleRoutes[moduleInfo.name] = [];
        }
        const extendedRoute = require(routeFilePath);
        if (extendedRoute.routeInfo===undefined)return false;
        if (this.moduleRoutes[moduleInfo.name].includes(extendedRoute.routeInfo)) return false;
        this.moduleRoutes[moduleInfo.name].push(extendedRoute.routeInfo);
        app.app.use(extendedRoute.routeInfo.path,extendedRoute);
        moduleUtils.log("Registered Module route ["+moduleInfo.name+"] => "+extendedRoute.routeInfo.path)
    }
};

