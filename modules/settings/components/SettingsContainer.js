module.exports = class {
    components = [];
    opened = false
    id
    title
    constructor(id) {
        this.id = id;
    }
    /**
     * @deprecated please use add function instead
     * @param settingsRow any Settings Structure components e.g. SettingsContainer, SettingsRow... Buttons and other inputs are not allowed!
     * */
    addRow(settingsRow){
        return this.add(settingsRow);
    }

    /**
     * @param settingsComponent any Settings Structure components e.g. SettingsContainer, SettingsRow... Buttons and other inputs are not allowed!
     * */
    add(settingsComponent){
        if (settingsComponent===undefined||typeof settingsComponent.build !== "function") return this;
        this.components.push(settingsComponent.build());
        return this;
    }
    setTitle(title){
        this.title = title;
        return this;
    }

    setOpened(opened){
        this.opened = opened;
        return this;
    }

    build(){
        if (this.id === undefined) {
            console.warn("No id set! Returning empty container!")
            return {};
        }
        return {
            type: "CONTAINER",
            components: this.components,
            id: this.id+"_container",
            opened: this.opened,
            title: this.title||this.id
        }
    }
}