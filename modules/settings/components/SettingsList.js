module.exports = class {

    colTitles = []
    rows = []
    id
    inputs = []
    disableInput = true;
    disableRemove = false;
    singleInput = false;
    title = "";
    small = "";
    constructor(id) {
        this.id = id
    }

    /**
     * Must be set before adding rows
     * @param colTitles arrayList e.g. ["Col1", "Col2", ...]
     * @returns this
     * */
    setColTitles(colTitles){
        if (colTitles===undefined)return this;
        this.colTitles = colTitles;
        return this;
    }

    /**
     * This function is the equivalent to defaultValue in addString
     * */
    addRow(listRow){
        if (typeof listRow.build !== "function")return this;
        const buildRow = listRow.build();
        if (buildRow.type!=="LIST_ROW")return this;
        if (buildRow.row.length!==this.colTitles.length)return this;
        this.rows.push(buildRow);
    }

    /**
     * @param input SettingsListInput
     * */
    addInput(input){
        this.disableInput = false;
        if (typeof input.build !== "function")return this;
        var build = input.build();
        if (build.type!=="LIST_INPUT")return this;
        this.inputs.push(build);
        return this;
    }

    setSmall(value){
        this.small = value;
        return this;
    }


    build(){
        return {
            type: "LIST",
            title: this.title,
            colTitles: this.colTitles,
            id: this.id+"_list",
            rows: this.rows,
            inputs: this.inputs,
            disableInput: this.disableInput,
            disableRemove: this.disableRemove,
            singleInput: this.singleInput,
            small: this.small
        }
    }
}