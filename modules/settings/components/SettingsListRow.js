module.exports = class {

    row = [];
    extra = ""
    constructor(rowArray) {
        this.row = rowArray;
    }

    /**
     * @param rowArray e.g. ["Col_1 Value", "Col_2 Value", "..."]
     * */
    setRow(rowArray){
        this.row = rowArray;
        return this;
    }
    /**
     * Can be used to store data
     * @param extra data to store
     * */
    setExtra(extra){
        this.extra = extra;
    }

    build(){
        return {
            type: "LIST_ROW",
            row: this.row,
            extra: this.extra
        }
    }
}