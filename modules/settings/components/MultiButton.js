module.exports = class {
    id
    states = {}
    disabled = false;
    small = ""
    selectedID = ""
    label = ""
    extra = ""

    constructor(id) {
        this.id = id;
    }

    addState(id, color, txt, value) {
        this.states[id] = {id, color, txt, value}
    }

    setSelected(id) {
        this.selectedID = id;
    }

    setSmall(txt) {
        this.small = txt;
    }

    setExtra(extra) {
        this.extra = extra
    }

    disable() {
        this.disabled = true;
    }

    enable() {
        this.disabled = false;
    }

    setLabel(label) {
        this.label = label;
    }

    build() {
        return {
            id: this.id, states: this.states, selectedID: this.selectedID, small: this.small, label: this.label,
            disabled: this.disabled, extra: this.extra, type: "MULTI_BTN"
        };
    }
}