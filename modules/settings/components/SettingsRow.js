module.exports = class {
    columns = []
    title
    constructor() {
    }

    setTitle(title) {
        this.title = title;
        return this;
    }

    addBool(id, label, btnTxT, defaultValue, small, trueText, falseText, disabled, extra) {
        if (defaultValue === undefined) defaultValue = false;
        if (label === undefined) label = "";
        if (btnTxT === undefined) btnTxT = "";
        if (small === undefined) small = "";
        if (trueText === undefined) trueText = "";
        if (falseText === undefined) falseText = "";
        if (disabled === undefined) disabled = false;
        if (extra === undefined) extra = "";
        this.columns.push({
            id,
            label,
            btnTxT,
            defaultValue,
            small,
            falseText,
            trueText,
            disabled,
            extra,
            type: "BOOL"
        });
        return this;
    }

    addLink(id, label, txt, link, small,className,style, _blank){
        if (id === undefined) return this;
        if (label === undefined) label = "";
        if (txt === undefined) txt = "";
        if (link === undefined) link = "";
        if (small === undefined) small = "";
        if (className === undefined) className = "btn btn-link";
        if (style === undefined) style = "";
        if (_blank === undefined) _blank = true;
        this.columns.push({
            id,
            label,
            txt,
            link,
            small,
            style,
            className,
            _blank,
            type: "LINK"
        });
        return this;
    }

    addMultiButton(multiButton) {
        if (multiButton === undefined) return;
        this.columns.push(multiButton.build());
        return this;
    }

    addText(label, text, small, extra) {
        if (label === undefined) label = "";
        if (text === undefined) text = "";
        if (small === undefined) small = "";
        if (extra === undefined) extra = "";
        this.columns.push({label, text, small, extra, type: "TEXT"})
        return this;
    }

    addString(id, label, defaultValue, placeholder, hidden, small, required, extra) {
        if (id === undefined) return this;
        if (defaultValue === undefined) defaultValue = "";
        if (placeholder === undefined) placeholder = "placeholder";
        if (hidden === undefined) hidden = false;
        if (required === undefined) required = false;
        if (label === undefined) label = "";
        if (small === undefined) small = "";
        if (extra === undefined) extra = "";
        this.columns.push({id, label, defaultValue, placeholder, hidden, small, required, extra, type: "STRING"});
        return this;
    }

    addNumber(id, label, defaultValue, placeholder, small, min, max,required,extra) {
        if (id === undefined) return this;
        if (label === undefined) label = "";
        if (defaultValue === undefined) defaultValue = 0;
        if (placeholder === undefined) placeholder = "placeholder";
        if (small === undefined) small = "placeholder";
        if (extra === undefined) extra = "";
        if (required === undefined) required = false;
        this.columns.push({id, label, defaultValue, placeholder, small, extra,min,max,required, type: "NUMBER"});
        return this;
    }

    addSelect(id, label, selectedValue, options, small, extra) {
        if (id === undefined) return this;
        if (label === undefined) label = "";
        if (selectedValue === undefined) selectedValue = "";
        if (options === undefined) options = [];
        if (small === undefined) small = "";
        if (extra === undefined) extra = "";
        this.columns.push({id, label, selectedValue, options, small, extra, type: "SELECT"});
        return this;
    }

    build() {
        return {
            type: "ROW",
            title: this.title || "",
            columns: this.columns
        };
    }
}