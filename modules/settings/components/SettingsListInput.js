module.exports = class {

    row = [];
    isStringInput =  true;
    placeholder =  "";
    selectValues = [];
    constructor() {
    }

    setPlaceholder(value){
        this.placeholder = value;
        return this;
    }

    /**
     * @param value object {label: "Label", value: "Value"} or just a string "Value/Label"
     * */
    addSelectValue(value){
        this.isStringInput = false;
        this.selectValues.push(value);
        return this;
    }

    build(){
        return {
            type: "LIST_INPUT",
            isStringInput: this.isStringInput,
            selectValues: this.selectValues,
            placeholder: this.placeholder
        }
    }
}