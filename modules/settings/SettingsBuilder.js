module.exports = class {
    moduleInfo
    components = [];
    title;
    cb;
    updateCB
    autoReload = false;
    hideSaveBTN = false;

    constructor(moduleInfo) {
        this.moduleInfo = moduleInfo;
    }

    setTile(title) {
        this.title = title;
        return this;
    }
    setAutoReload(bool){
        this.autoReload = bool;
    }

    getName() {
        return this.moduleInfo.name;
    }

    /**
     * @deprecated please use add function instead
     * @param settingsRow any Settings Structure components e.g. SettingsContainer, SettingsRow... Buttons and other inputs are not allowed!
     * */
    addRow(settingsRow){
        return this.add(settingsRow);
    }

    /**
     * @param settingsComponent any Settings Structure components e.g. SettingsContainer, SettingsRow... Buttons and other inputs are not allowed!
     * */
    add(settingsComponent) {
        if (settingsComponent===undefined||typeof settingsComponent.build !== "function") return this;
        this.components.push(settingsComponent.build())
        return this;
    }

    setCallback(cb) {
        this.cb = cb;
        return this;
    }

    resolve(settingResult) {
        if (this.cb) this.cb(settingResult);
        return this;
    }

    getID() {
        return this.getName().toLowerCase().replaceAll(/ /g, "_");
    }

    hideSave() {
        this.hideSaveBTN = true;
        return this;
    }

    build() {
        return {
            title: this.title || this.getName(),
            components: this.components,
            id: this.getID(),
            name: this.getName(),
            hideSaveBTN: this.hideSaveBTN,
            autoReload: this.autoReload
        };
    }
}