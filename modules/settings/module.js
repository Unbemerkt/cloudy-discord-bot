const {ModuleUtils} = require("../../ModuleUtils");
const moduleInfo = {
    name: "settings",
    prio: 0,
    enable: true,
    requiredModules: [],
    requiredNodeModules: []
}
const moduleUtils = new ModuleUtils(moduleInfo);
module.exports = {
    moduleInfo,
    moduleRegistered: function () {

    },
    moduleLoaded: function () {

    },
    runModule: function () {

    },
    updateSettings() {
        moduleUtils.log("Reloading settings...")
        Object.keys(this.functions).forEach(id => {
            this.functions[id]();
        })
        moduleUtils.success("Settings reloaded!")
    },
    functions: {},
    registerUpdateFunction(id, func) {
        this.functions[id] = func;
    },
    settings: {},
    getSettings(build) {
        if (build) {
            const map = {};
            Object.keys(this.settings).forEach(id => {
                map[id] = this.settings[id].build();
            })
            return map;
        }
        return this.settings;
    },
    getSetting(moduleName) {
        return this.settings[moduleName];
    },
    updateSetting(noduleName){
        if (this.functions[noduleName]!==undefined)this.functions[noduleName]();
    },
    registerSettingsBuilder: function (settingsBuilder) {
        this.settings[settingsBuilder.getName()] = settingsBuilder;
    },
    SettingsBuilder: require("./SettingsBuilder"),
    SettingsContainer: require("./components/SettingsContainer"),
    SettingsRow: require("./components/SettingsRow"),
    MultiButton: require("./components/MultiButton"),
    SettingsList: require("./components/SettingsList"),
    SettingsListRow: require("./components/SettingsListRow"),
    SettingsListInput: require("./components/SettingsListInput"),
    SettingsResult: require("./SettingsResult")
};