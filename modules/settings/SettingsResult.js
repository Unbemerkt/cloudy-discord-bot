module.exports = class {
    resultMap
    name

    constructor(name,resultMap) {
        this.name = name;
        this.resultMap = resultMap;
    }
    getName(){
        return this.name;
    }

    getString(id) {
        const item = this.resultMap[id];
        if (item === undefined) return undefined;
        if (item.type !== "STRING") return undefined;
        return item.value;
    }

    getNumber(id, parser) {
        const item = this.resultMap[id];
        if (item === undefined) return undefined;
        if (item.type !== "NUMBER") return undefined;
        if (parser !== undefined) {
            return parser(item.value);
        }
        return parseInt(item.value);
    }

    getSelect(id) {
        const item = this.resultMap[id];
        if (item === undefined) return undefined;
        if (item.type !== "SELECT") return undefined;
        return item.value;
    }

    getBool(id) {
        const item = this.resultMap[id];
        if (item === undefined) return undefined;
        if (item.type !== "BOOL") return undefined;
        return item.value === "true";
    }

    getMultiBtn(id) {
        const item = this.resultMap[id];
        if (item === undefined) return undefined;
        if (item.type !== "MULTI_BTN") return undefined;
        return item.value;
    }

    getList(id){
        if (id===undefined)return undefined;
        id+="_list";
        const item = this.resultMap[id];
        if (item === undefined) return undefined;
        if (item.type !== "LIST") return undefined;
        return item.values
    }

    getRaw(){
        return this.resultMap;
    }

}