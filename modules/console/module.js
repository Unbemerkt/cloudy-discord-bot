const {ModuleUtils} = require("../../ModuleUtils");
const {Collection} = require("discord.js");
const path = require("path");
const fs = require("fs");
const moduleLoader = require("../../moduleLoader");
const {autocomplete} = require("./commands/reload");

const moduleInfo = {
    name: "console",
    prio: 0,
    enable: true,
    requiredModules: [
        "permissions"
    ],
    requiredNodeModules: []
}
const moduleUtils = new ModuleUtils(moduleInfo);
module.exports = {
    moduleInfo,
    moduleUtils,
    permissionsManager: {},
    moduleLoaded: function () {
    },
    moduleRegistered: function () {
    },
    runModule: function () {
        this.loadCommands();
        this.permissionsManager = moduleLoader.getModule("permissions");
    },
    loadCommands() {
        if (this.consoleCommands === undefined) {
            this.consoleCommands = {};
        }
        const commandsPath = path.join(__dirname, 'commands');
        const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));

        for (const file of commandFiles) {
            const filePath = path.join(commandsPath, file);
            const commandFile = require(filePath);
            // Set a new item in the Collection with the key as the command name and the value as the exported module
            if ('id' in commandFile && 'action' in commandFile && 'info' in commandFile) {
                var cmd;
                cmd = new this.ConsoleCommand(commandFile.id, commandFile.action, commandFile.info,
                    commandFile.args, commandFile.permissions, commandFile.aliases, commandFile.optionalPermissions, commandFile.autocomplete);
                this.registerCommand(cmd)
            } else {
                if ('command' in commandFile) {
                    this.registerCommand(commandFile.command)
                } else {
                    moduleUtils.log(`[WARNING] The command at ${filePath} is missing a required "data" and "execute" or "command" property.`);
                }

            }
        }
    },
    consoleCommands: {},
    runCommand(cmd, consoleSender) {
        cmd = cmd.toLowerCase();
        cmd = cmd.split(" ");
        var id = "";
        if (cmd.length < 1) {
            cmd = cmd[0]
            id = cmd;
        } else {
            id = cmd[0]
        }
        var command = this.consoleCommands[id];
        if (command === undefined) {
            Object.keys(this.consoleCommands).every(key => {
                if (this.consoleCommands[key].getAliases() !== undefined && this.consoleCommands[key].getAliases().includes(id)) {
                    command = this.consoleCommands[key];
                    return false; //break
                }
                return true; //continue
            })
            if (command === undefined) {
                moduleUtils.error(id + " is not a valid command!")
                return false;
            }
        }
        if (consoleSender === undefined) consoleSender = this.permissionsManager.getUser("console");
        if (command.getPermissions() !== undefined && !consoleSender.hasPermissions(command.getPermissions())) {
            moduleUtils.error("You don't have the permission to run this command!")
            return false;
        }
        const cmdResult = command.resolve(new ModuleUtils({name: "CONSOLE_COMMAND"}), id, cmd.slice(1), consoleSender);
        if (cmdResult===undefined)return true;
        return cmdResult;
    },
    getAutocomplete(commandLineString, consoleSender){
        commandLineString = commandLineString.toLowerCase();
        commandLineString = commandLineString.split(" ");
        var id = "";
        if (commandLineString.length < 1) {
            commandLineString = commandLineString[0]
            id = commandLineString;
        } else {
            id = commandLineString[0]
        }
        var command = this.consoleCommands[id];
        if (command === undefined) {
            Object.keys(this.consoleCommands).every(key => {
                if (this.consoleCommands[key].getAliases() !== undefined && this.consoleCommands[key].getAliases().includes(id)) {
                    command = this.consoleCommands[key];
                    return false; //break
                }
                return true; //continue
            })
            if (command === undefined) {
                return {command: id, autocomplete: []};
            }
        }
        if (consoleSender === undefined) consoleSender = this.permissionsManager.getUser("console");
        if (command.getPermissions() !== undefined && !consoleSender.hasPermissions(command.getPermissions())) {
            return false;
        }
        const args = commandLineString.slice(1);
        const cmdResult = command.resolveAutocomplete(new ModuleUtils({name: "CONSOLE_COMMAND"}), id, args, args.length-1, consoleSender);
        if (cmdResult===undefined)return {command: id, autocomplete: []};
        return {command: id, autocomplete: cmdResult};
    },
    registerCommand(command) {
        if (!this.validateCommand(command)) {
            moduleUtils.error("Invalid command!")
            return;
        }
        this.consoleCommands[command.getId()] = command;
        moduleUtils.log("ConsoleCommand '" + command.getId() + "' registered!",undefined,"color:#00FF00")
    },
    removeCommand(id) {
        if (this.consoleCommands[id] === undefined) {
            moduleUtils.error("Command '" + id + "' not found!");
            return;
        }
        delete this.consoleCommands[id];
        moduleUtils.log("Command '" + id + "' unregistered!");
    },
    validateCommand(cmd) {
        if (typeof cmd.setAction !== "function") return false;
        if (typeof cmd.getAction !== "function") return false;
        if (typeof cmd.resolve !== "function") return false;
        if (typeof cmd.getId !== "function") return false;
        if (typeof cmd.getInfo !== "function") return false;
        if (typeof cmd.setInfo !== "function") return false;
        if (typeof cmd.getArgs !== "function") return false;
        if (typeof cmd.getAliases !== "function") return false;
        return true;
    },
    ConsoleCommand: class {
        id
        action
        info
        autocomplete
        args = []
        permissions = []
        aliases = []
        optionalPermissions = []

        constructor(id, action, info, args, permissions, aliases, optionalPermissions, autocomplete) {
            this.id = id.toLowerCase();
            this.action = action;
            this.info = info;
            this.args = args || [];
            this.permissions = permissions || [];
            this.aliases = aliases || [];
            this.autocomplete = autocomplete;
            this.optionalPermissions = optionalPermissions || [];
        }

        setAction(action) {
            this.action = action;
            return this;
        }

        setAutocomplete(callback){
            this.autocomplete = callback;
            return this;
        }

        resolveAutocomplete(moduleUtils, cmd, args, argIndex, consoleSender){
            if (this.autocomplete !== undefined) return this.autocomplete(moduleUtils, cmd, args, argIndex, consoleSender);
        }

        getAction() {
            return this.action
        }

        getInfo() {
            return this.info;
        }

        getPermissions() {
            return this.permissions;
        }

        getOptionalPermissions(){
            return this.optionalPermissions;
        }

        setInfo(info) {
            this.info = info;
            return this;
        }

        getArgs() {
            return this.args;
        }

        getAliases() {
            return this.aliases;
        }

        resolve(moduleUtils, cmd, args, consoleSender) {
            if (this.action !== undefined) this.action(moduleUtils, cmd, args, consoleSender);
        }

        getId() {
            return this.id;
        }
    }
};