const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
const permissionsManager = moduleLoader.getModule("permissions")
module.exports = {
    id: "me",
    action(moduleUtils, cmd, args, cs) {
        moduleUtils.pureLog("")
        moduleUtils.log("Current user: "+cs.getName());
        //update user
        var tokenUser = permissionsManager.loggedinUsers[cs.web.token];
        if (tokenUser!==undefined){
            tokenUser = tokenUser.updateUser();
        }
        if (args.length>0&&args[0]==="-full"){
            if (!cs.hasPermission("me.full")){
                moduleUtils.noPermissions();
                return;
            }
            moduleUtils.pureLog(JSON.stringify(cs.getJsonData(),null,2));
        }
    },
    autocomplete(moduleUtils, cmd, args, argIndex, cs){
        if (argIndex!==0)return [];
        return this.args[0].filter((word) => word.toLowerCase().startsWith(args[argIndex]));
    },
    args: [
        [
          "-full"
        ]
    ],
    optionalPermissions: [
        "me.full"
    ],
    info: "Shows loggedin user"
}