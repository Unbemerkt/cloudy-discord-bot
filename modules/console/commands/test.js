const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
const permissionsManager = moduleLoader.getModule("permissions")
module.exports = {
    id: "test",
    action(moduleUtils, cmd, args, cs) {
        if (permissionsManager === undefined) {
            moduleUtils.warn("PermissionManager is undefined!")
            return;
        }
        if (!cs.addNotification(new permissionsManager.Notification("test_red_once", "Only one time Important", "This message seems like it is important, but it isn't. So this message will only be shown once", "/console", "red", true, false))) moduleUtils.error("Failed to add notification!")
        if (!cs.addNotification(new permissionsManager.Notification("test_blue", "Blue Test", "Just some text that closes on click", "/home", "blue", false, true))) moduleUtils.error("Failed to add notification!")
        if (!cs.addNotification(new permissionsManager.Notification("test_blue_important", "Important Blue Test", "Just some text that closes on click AND IT'S IMPORTANT", "/home", "blue", false, true, true))) moduleUtils.error("Failed to add notification!")
        if (!cs.addNotification(new permissionsManager.Notification("test_blue_2", "Blue Test", "Just some text", "/home", "blue", false, false))) moduleUtils.error("Failed to add notification!")
        if (!cs.addNotification(new permissionsManager.Notification("test_blue_3", "Blue Google Test", "Go to google!", "https://google.com", "blue", false, false))) moduleUtils.error("Failed to add notification!")
        if (!cs.addNotification(new permissionsManager.Notification("test_yellow", "Yellow Test", "Wow no link! But yellow", "", "yellow", false, false))) moduleUtils.error("Failed to add notification!")
        if (!cs.addNotification(new permissionsManager.Notification("test_red", "Red Test", "This message seems like it is important, but it isn't", "", "red", false, false))) moduleUtils.error("Failed to add notification!")
        if (!cs.addNotification(new permissionsManager.Notification("test_green", "Green Test", "Uiiii.... It's green!!!!", "", "green", false, false))) moduleUtils.error("Failed to add notification!")
        cs.save()
        moduleUtils.log("Added test notifications to " + cs.getName())
    },
    info: "Test command"
}