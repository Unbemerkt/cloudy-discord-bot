const moduleLoader = require("./../../../moduleLoader");
const md5 = require("md5");
const commandManager = moduleLoader.getModule("console");
const permissionsManager = moduleLoader.getModule("permissions")
module.exports = {
    id: "hash",
    action(moduleUtils, cmd, args, cs) {
        moduleUtils.pureLog("")
        if (args.length !== 1){
            moduleUtils.warn("Usage: hash (word to hash)")
            return false;
        }
        moduleUtils.log(args[0]+": "+md5(args[0]));
    },
    args: [
        [
          "word to hash"
        ]
    ],
    permissions: [
        "hash.use"
    ],
    info: "Hash a given word"
}