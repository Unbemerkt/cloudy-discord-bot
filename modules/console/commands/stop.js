const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
const dCore = moduleLoader.getModule("discordCore")
module.exports = {
    id: "stop",
    action(moduleUtils, cmd, args, cs) {
        if (dCore.onlineState==="OFFLINE"){
            moduleUtils.log("DiscordBot is already offline!");
            return;
        }
        moduleUtils.log("DiscordBot shutting down...",undefined,"color:#ff0000");
        dCore.stop();

    },
    permissions: [
        "bot.stop"
    ],
    info: "Stops the discord bot"
}