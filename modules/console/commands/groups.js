const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
const permissionsManager = moduleLoader.getModule("permissions")
module.exports = {
    id: "groups",
    action(moduleUtils, cmd, args, cs) {
        moduleUtils.pureLog("")
        moduleUtils.log("All groups: ");
        var i = 0;
        var groupList = permissionsManager.getAllGroups();
        groupList.forEach(group=>{
            if (i===groupList.length-1){
                moduleUtils.log(" ╚› "+group.getName());
            }else{
                moduleUtils.log(" ╟› "+group.getName());
            }
            i++;
        })
    },
    args: [],
    permissions: [
        "groups.list"
    ],
    aliases: [
        "g"
    ],
    optionalPermissions: [],
    info: "Lists all groups"
}