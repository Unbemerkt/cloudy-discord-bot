const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
module.exports = {
    id: "help",
    aliases: [
        "?"
    ],
    args: [
        [
            "permissions",
            "perm",
            "perms"
        ]
    ],
    autocomplete(moduleUtils, cmd, args, argIndex, cs){
        if (argIndex!==0)return [];
        return ["permissions"].filter((word) => word.toLowerCase().startsWith(args[argIndex]));
    },
    action(moduleUtils, cmd, args, cs) {
        moduleUtils.pureLog("")
        moduleUtils.pureLog("")
        moduleUtils.pureLog("")
        moduleUtils.log("All commands:")
        Object.keys(commandManager.consoleCommands).forEach(id=>{
            var cmd = commandManager.consoleCommands[id];
            var arguments = "";
            if (cmd.getArgs()!==undefined&&cmd.getArgs().length>0){
                cmd.getArgs().forEach(argList=>{
                    arguments+=" (";
                    var i = true;
                    argList.forEach(arg=>{
                        if (i){
                            //first
                            arguments+=arg;
                            i=false;
                        }else{
                            arguments+="/"+arg;
                        }
                    });
                    arguments+=")";
                })
            }
            moduleUtils.log(id+arguments+" » "+ cmd.info);
            if (args.length>0&&(args.includes("permissions")||args.includes("perm")||args.includes("perms"))){
                if (cmd.getOptionalPermissions().length>0||cmd.getPermissions().length>0){
                    var i = 0;
                    var permList = cmd.getPermissions();
                    if (cmd.getOptionalPermissions().length>0)permList = permList.concat(cmd.getOptionalPermissions());
                    permList.forEach(perm=>{
                        if (i===permList.length-1){
                            moduleUtils.log(" ╚› "+perm,undefined,"color: #c1c1c1");
                        }else{
                            moduleUtils.log(" ╟› "+perm,undefined,"color: #c1c1c1");
                        }
                        i++;
                    })
                }
            }

        });
    },
    info: "Show all commands"
}