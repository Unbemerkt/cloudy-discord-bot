const moduleLoader = require("./../../../moduleLoader");
const {commands} = require("../../discordCommandManager/module");
var commandManager = moduleLoader.getModule("console");
const permissionsManager = moduleLoader.getModule("permissions")
module.exports = {
    id: "command",
    aliases: [
        "cmd"
    ],
    action(moduleUtils, command, args, cs) {
        if (args.length < 2) {
            moduleUtils.warn("Usage: command (load/unload/reload) (command/all)")
            return;
        }
        var cmd;
        switch (args[0]) {
            case "load":
                if (!cs.hasPermissions(["commands.*", "commands.load"])) {
                    moduleUtils.error("You don't have the permission to run this command!")
                    return;
                }
                cmd = commandManager.consoleCommands[args[1]];
                if (cmd !== undefined) {
                    moduleUtils.error("This command is already registered!")
                    return;
                }
                cmd = require("./" + args[1] + ".js");
                commandManager.registerCommand(new commandManager.ConsoleCommand(cmd.id,
                    cmd.action, cmd.info, cmd.args, cmd.permissions, cmd.aliases, cmd.optionalPermissions, cmd.autocomplete))
                break;
            case "unload":
                if (!cs.hasPermissions(["commands.*", "commands.unload"])) {
                    moduleUtils.error("You don't have the permission to run this command!")
                    return;
                }
                cmd = commandManager.consoleCommands[args[1]];
                if (cmd === undefined) {
                    moduleUtils.error("This command isn't registered!")
                    return;
                }
                commandManager.removeCommand(cmd.getId());
                break;
            case "reload":
                if (!cs.hasPermissions(["commands.*", "commands.reload"])) {
                    moduleUtils.error("You don't have the permission to run this command!")
                    return;
                }
                if (args[1] === "all") {
                    moduleUtils.log("Reloading ALL command...")
                    Object.keys(commandManager.consoleCommands).forEach(commandName => {
                        const consoleCommand = commandManager.consoleCommands[commandName];
                        if (consoleCommand === undefined) {
                            moduleUtils.error("Command '" + commandName + "' not found!");
                            return;
                        }
                        commandManager.removeCommand(commandName);
                        delete require.cache[require.resolve("./" + commandName + ".js")];
                        const newCommand = require("./" + commandName + ".js");
                        commandManager.registerCommand(new commandManager.ConsoleCommand(newCommand.id,
                            newCommand.action, newCommand.info, newCommand.args, newCommand.permissions, newCommand.aliases,
                            newCommand.optionalPermissions))
                        moduleUtils.log("Command '" + commandName + "' was reloaded!",undefined,"color:#00FF00")
                        moduleUtils.pureLog("")
                    });
                    moduleUtils.pureLog("")
                    moduleUtils.pureLog("")
                    moduleUtils.log("All command reloaded!",undefined,"color:#00FF00")
                    return;
                }
                const commandName = args[1];
                cmd = commandManager.consoleCommands[args[1]];
                if (cmd === undefined) {
                    moduleUtils.error("Command '" + args[1] + "' not found!");
                    return;
                }
                commandManager.removeCommand(commandName);
                delete require.cache[require.resolve("./" + commandName + ".js")];
                const newCommand = require("./" + commandName + ".js");
                commandManager.registerCommand(new commandManager.ConsoleCommand(newCommand.id,
                    newCommand.action, newCommand.info, newCommand.args, newCommand.permissions,
                    newCommand.optionalPermissions))
                moduleUtils.log("Command '" + commandName + "' was reloaded!",undefined,"color:#00FF00")
                break;

        }


    },
    autocomplete(moduleUtils, cmd, args, argIndex, cs){
        if (commandManager===undefined){
            commandManager = moduleLoader.getModule("console");
        }
        switch (argIndex){
            case 0:
                return this.args[0].filter((word) => word.toLowerCase().startsWith(args[argIndex]));
            case 1:
                var list = Object.keys(commandManager.consoleCommands).filter((word) => word.toLowerCase().startsWith(args[argIndex]));
                list.push("all");
                return list;
            default:
                return [];
        }
    },
    permissions: [
        "commands.*",
        "commands.load",
        "commands.unload",
        "commands.reload"
    ],
    args: [
        [
            "load",
            "unload",
            "reload"
        ],
        [
            "command",
            "all"
        ],
    ],
    info: "Manage console commands"
}