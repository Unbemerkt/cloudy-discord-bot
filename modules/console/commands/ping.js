const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
module.exports = {
    id: "ping",
    action(moduleUtils) {
        moduleUtils.log("Pong!")
    },
    info: "Pong!"
}