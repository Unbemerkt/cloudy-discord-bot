const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
const dCore = moduleLoader.getModule("discordCore")
module.exports = {
    id: "shutdown",
    action(moduleUtils, cmd, args, cs) {
        moduleUtils.warn("Shutting down NOW!");
        setTimeout(()=>{
            moduleLoader.shutdown();
        },1000)
    },
    permissions: [
        "app.shutdown"
    ],
    info: "Shutdown the application"
}