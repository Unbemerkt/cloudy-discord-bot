const moduleLoader = require("./../../../moduleLoader");
var dCore = moduleLoader.getModule("discordCore");
module.exports = {
    id: "leave",
    aliases: [],
    args: [
        [
            "guildID"
        ]
    ],
    autocomplete(moduleUtils, cmd, args, argIndex, cs) {
        if (argIndex !== 0) return [];
        if (dCore === undefined) {
            dCore = moduleLoader.getModule("discordCore");
        }
        if (dCore.getCurrentState() !== dCore.onlineStates.ONLINE){
            return [{name: "Bot is Offline", value: "INVALID"}]
        }
        const list = [];
        const guilds = dCore.getGuilds();
        Object.keys(guilds).forEach(id => {
            list.push({name: guilds[id].guildName, value: id});
        })
        return list.filter((entry) => entry.name.toLowerCase().startsWith(args[argIndex]) || entry.value.toLowerCase().startsWith(args[argIndex]));
    },
    action(moduleUtils, cmd, args, cs) {
        if (dCore === undefined) {
            dCore = moduleLoader.getModule("discordCore");
        }
        if (args.length !== 1) {
            moduleUtils.warn("Usage: leave (guild ID)")
            return;
        }
        if (dCore.getCurrentState() !== dCore.onlineStates.ONLINE||args[0]==="INVALID"){
            moduleUtils.error(`Bot is Offline!`);
            return;
        }
        dCore.client.guilds.cache.get(args[0]).leave().then(() => {
          moduleUtils.success("Guild leaved!")
        }).catch(err => {
                moduleUtils.error(`There was an error leaving the guild: \n ${err.message}`);
            });
    },
    info: "Show all commands"
}