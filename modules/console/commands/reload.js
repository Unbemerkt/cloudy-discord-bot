const moduleLoader = require("./../../../moduleLoader");
var commandManager;
var permissionsManager;
module.exports = {
    id: "reload",
    aliases: [
        "rl"
    ],
    action(moduleUtils, cmd, args, cs) {
        moduleUtils.pureLog("")
        try {
            if (commandManager===undefined){
                commandManager = moduleLoader.getModule("console");
            }
            if (args.length > 0) {
                //reload specific command
                for (let i = 0; i < args.length; i++) {
                    const commandName = args[i].toLowerCase();
                    const consoleCommand = commandManager.consoleCommands[commandName];
                    if (consoleCommand === undefined) {
                        moduleUtils.error("Command '" + commandName + "' not found!");
                        return;
                    }
                    commandManager.removeCommand(commandName);
                    delete require.cache[require.resolve("./" + commandName + ".js")];
                    const newCommand = require("./" + commandName + ".js");
                    commandManager.registerCommand(new commandManager.ConsoleCommand(newCommand.id,
                        newCommand.action, newCommand.info, newCommand.args, newCommand.permissions, newCommand.aliases,
                        newCommand.optionalPermissions, newCommand.autocomplete))
                    moduleUtils.log("Command '" + commandName + "' was reloaded!",undefined,"color:#00FF00")
                }
            } else {
                //reload all commands
                moduleUtils.log("Reloading ALL commands...")
                Object.keys(commandManager.consoleCommands).forEach(commandName => {
                    const consoleCommand = commandManager.consoleCommands[commandName];
                    if (consoleCommand === undefined) {
                        moduleUtils.error("Command '" + commandName + "' not found!");
                        return;
                    }
                    commandManager.removeCommand(commandName);
                    delete require.cache[require.resolve("./" + commandName + ".js")];
                    const newCommand = require("./" + commandName + ".js");
                    commandManager.registerCommand(new commandManager.ConsoleCommand(newCommand.id,
                        newCommand.action, newCommand.info, newCommand.args, newCommand.permissions, newCommand.aliases,
                        newCommand.optionalPermissions, newCommand.autocomplete))
                    moduleUtils.log("Command '" + commandName + "' was reloaded!",undefined,"color:#00FF00")
                    moduleUtils.pureLog("")
                });
                moduleUtils.pureLog("")
                moduleUtils.pureLog("")
                moduleUtils.log("All commands reloaded!",undefined,"color:#00FF00")
            }
        } catch (e) {
            moduleUtils.error(e);
        }


    },
    autocomplete(moduleUtils, cmd, args, argIndex, cs){
        if (permissionsManager===undefined){
            permissionsManager = moduleLoader.getModule("permissions");
        }
        if (!cs.hasPermission("commands.reload")) {
            return [{name: "No permission!", value: "INVALID"}];
        }

        if (commandManager===undefined){
            commandManager = moduleLoader.getModule("console");
        }
        return Object.keys(commandManager.consoleCommands).filter((word) => word.toLowerCase().startsWith(args[argIndex]));
    },
    args: [
        [
            "command"
        ],
        [
            "command"
        ],
        [
            "..."
        ]
    ],
    permissions: [
        "commands.reload",
    ],
    info: "Reloads all or specific console commands"
}