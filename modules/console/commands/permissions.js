const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
const permissionsManager = moduleLoader.getModule("permissions")
function fullHelp(moduleUtils) {
    moduleUtils.warn("Usage: permission (user/group) (username/groupName) (create/delete/addPermission/" +
        "removePermission/link/unlink/addGroup/removeGroup/enableWeb/setPassword) " +
        "(group/permission/discord user id/true/false/password)")
}

function groupHelp(moduleUtils) {
    moduleUtils.warn("Usage: permission (group) (groupName) (create/delete/addPermission/" +
        "removePermission/link/unlink) " +
        "(group/permission)")
}
function userHelp(moduleUtils) {
    moduleUtils.warn("Usage: permission (user) (username) (create/delete/addPermission/" +
        "removePermission/link/unlink/addGroup/removeGroup/enableWeb/setPassword) " +
        "(group/permission/discord user id/true/false/password)")
}

module.exports = {
    userHelp,
    groupHelp,
    fullHelp,
    id: "permissions",
    aliases: [
        "permission",
        "perms",
        "perm"
    ],
    autocomplete(moduleUtils, cmd, args, argIndex, cs){
        if (argIndex<0||argIndex>this.args.length-1)return [];
        var resultList;
        if (argIndex===1){
            switch (args[0]) {
                case "user":
                    var userList = []
                    permissionsManager.getAllUsers().forEach(user=>{
                        if (user.getName()!=="console")userList.push(user.getName())
                    });
                    return userList.filter((word) => word.toLowerCase().startsWith(args[argIndex]));
                case "group":
                    var groupList = []
                    permissionsManager.getAllGroups().forEach(group=>{
                        groupList.push(group.getName())
                    });
                    return groupList.filter((word) => word.toLowerCase().startsWith(args[argIndex]));
                default:
                    return this.args[argIndex].filter((word) => word.toLowerCase().startsWith(args[argIndex]));
            }
        }else if (argIndex===2){
            switch (args[0]) {
                case "user":
                    resultList = [
                        "create",
                        "delete",
                        "addPermission",
                        "removePermission",
                        "link",
                        "unlink",
                        "addGroup",
                        "removeGroup",
                        "enableWeb",
                        "setPassword"
                    ]
                    return resultList.filter((word) => word.toLowerCase().startsWith(args[argIndex]));
                case "group":
                    resultList = [
                        "create",
                        "delete",
                        "addPermission",
                        "removePermission",
                        "link",
                        "unlink"
                    ]
                    return resultList.filter((word) => word.toLowerCase().startsWith(args[argIndex]));
                default:
                    return this.args[argIndex].filter((word) => word.toLowerCase().startsWith(args[argIndex]));
            }
        }else{
            switch (args[0]) {
                case "user":
                    resultList = [
                        "(group)",
                        "(permission)",
                        "(discord user id)",
                        "true",
                        "false",
                        "(password)",
                    ]
                    return resultList.filter((word) => word.toLowerCase().startsWith(args[argIndex]));
                case "group":
                    resultList = [
                        "(group)",
                        "(permission)"
                    ]
                    return resultList.filter((word) => word.toLowerCase().startsWith(args[argIndex]));
                default:
                    return this.args[argIndex].filter((word) => word.toLowerCase().startsWith(args[argIndex]));
            }
        }
    },
    args: [
        [
            "user",
            "group",
        ],
        [
            "username",
            "groupname",
        ],
        [
            "create",
            "delete",
            "addPermission",
            "removePermission",
            "link",
            "unlink",
            "addGroup",
            "removeGroup",
            "hasPermission",
            "enableWeb",
            "setPassword"
        ],
        [
            "group",
            "permission",
            "discord user ID",
            "true",
            "false",
            "password"
        ]
    ],
    permissions: [
        "permissions.user.create",
        "permissions.user.delete",
        "permissions.user.edit.permissions",
        "permissions.user.edit.login",
        "permissions.user.checkpermissions",
        "permissions.user.edit.group",
        "permissions.user.edit.link",
        "permissions.group.edit.permissions",
        "permissions.group.edit.link",
        "permissions.group.create",
        "permissions.group.delete",
    ],
    action(moduleUtils, cmd, args, cs) {
        moduleUtils.pureLog("");
        if (args.length < 3 || args.length > 4) {
            fullHelp(moduleUtils);
            return;
        }
        var user;
        var group;
        if (args.length === 4) {
            if (args[0].toLowerCase()==="user"){
                user = permissionsManager.getUser(args[1]);
                if (user === undefined||user.getName()==="console") {
                    moduleUtils.error("User '" + args[1] + "' not found!")
                    return false;
                }
                switch (args[2].toLowerCase()){
                    case "setpassword":
                    case "password":
                    case "pw":
                        if (!cs.hasPermission("permissions.user.edit.login")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        user.setPassword(args[3]);
                        user.save();
                        moduleUtils.log("Password set!",undefined,"color:#00FF00");
                        return true;
                    case "enableweb":
                    case "web":
                        if (!cs.hasPermission("permissions.user.edit.login")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        if (args[3]==="true"){
                            if (user.setWebEnabled(true)){
                                user.save();
                                moduleUtils.log("User updated!",undefined,"color:#00FF00")
                            }else{
                                moduleUtils.error("Failed to update user! Please make sure to set a password before and try again!");
                                return false;
                            }
                        }else if (args[3]==="false"){
                            if (user.setWebEnabled(false)){
                                user.save();
                                moduleUtils.log("User updated!",undefined,"color:#00FF00")
                            }else{
                                moduleUtils.error("Failed to update user! Please try again!");
                                return false;
                            }
                        }else{
                            moduleUtils.error("Invalid input ("+args[3]+") please use true or false!");
                            return false;
                        }

                        return true;
                    case "addpermission":
                    case "addperm":
                        if (!cs.hasPermission("permissions.user.edit.permissions")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        if (user.addPermission(args[3])){
                            moduleUtils.log("User '" + args[1] + "' was given the '"+args[3]+"' permission!",undefined,"color:#00FF00")
                            user.save();
                        }else{
                            moduleUtils.log("Failed to give the user '" + args[1] + "' the '"+args[3]+"' permission!")
                            return false;
                        }
                        return;
                    case "removepermission":
                    case "removeperm":
                        if (!cs.hasPermission("permissions.user.edit.permissions")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        if (user.removePermission(args[3])){
                            moduleUtils.log("User '" + args[1] + "' was revoked the '"+args[3]+"' permission!",undefined,"color:#00FF00")
                            user.save();
                        }else{
                            moduleUtils.log("Failed to revoke the '"+args[3]+"' permission, from user '" + args[1] + "'!")
                            return false;
                        }
                        return;
                    case "link":
                        if (!cs.hasPermission("permissions.user.edit.link")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        user.setDiscordID(args[3]);
                        user.save();
                        moduleUtils.log("Linked the user with the discord user '"+args[3]+"'!",undefined,"color:#00FF00")
                        return;
                    case "addgroup":
                        if (!cs.hasPermission("permissions.user.edit.group")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        if (user.addGroup(args[3])){
                            moduleUtils.log("User '" + args[1] + "' was added to the '"+args[3]+"' group!",undefined,"color:#00FF00")
                            user.save();
                        }else{
                            moduleUtils.error("Failed to add the user '" + args[1] + "' to the '"+args[3]+"' group!")
                            return false;
                        }
                        return;
                    case "removegroup":
                        if (!cs.hasPermission("permissions.user.edit.group")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        if (user.removeGroup(args[3])){
                            moduleUtils.log("User '" + args[1] + "' was removed from the '"+args[3]+"' group!",undefined,"color:#00FF00")
                            user.save();
                        }else{
                            moduleUtils.error("Failed to remove the user '" + args[1] + "' from the '"+args[3]+"' group!")
                            return false;
                        }
                        return;
                    case "haspermission":
                    case "perm":
                        if (!cs.hasPermission("permissions.user.checkpermissions")){
                            moduleUtils.noPermissions();
                            return false;
                        }

                        if (user.hasPermission(args[3])){
                            moduleUtils.log("User '" + args[1] + "' has the '"+args[3]+"' permission!",undefined,"color:#00FF00")
                        }else{
                            moduleUtils.log("User '" + args[1] + "' hasn't the '"+args[3]+"' permission!",undefined,"color:#FF0000")
                            return false;
                        }
                        return;
                    default:
                        userHelp(moduleUtils);
                        return false;
                }
            }else if (args[0].toLowerCase()==="group"){
                group = permissionsManager.getGroup(args[1]);
                if (group === undefined) {
                    moduleUtils.error("Group '" + args[1] + "' not found!")
                    return false;
                }
                var tmp;
                switch (args[2].toLowerCase()){
                    case "addpermission":
                    case "addperm":
                        if (!cs.hasPermission("permissions.group.edit.permissions")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        if (group.addPermission(args[3])){
                            moduleUtils.log("Added '" + args[3] + "' permission to '"+args[1]+"'!",undefined,"color:#00FF00")
                            group.save();
                        }else{
                            moduleUtils.error("Couldn't add '" + args[3] + "' permission to '"+args[1]+"'!");
                            return false;
                        }
                        return;
                    case "removepermission":
                    case "removeperm":
                        if (!cs.hasPermission("permissions.group.edit.permissions")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        if (group.removePermission(args[3])){
                            moduleUtils.log("Revoked permission '" + args[3] + "' from '"+args[1]+"'!",undefined,"color:#00FF00")
                            group.save();
                        }else{
                            moduleUtils.error("Couldn't remove '" + args[3] + "' permission from '"+args[1]+"'!");
                            return false;
                        }
                        return;
                    case "link":
                        if (!cs.hasPermission("permissions.group.edit.link")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        tmp = permissionsManager.getGroup(args[3]);
                        if (tmp===undefined){
                            moduleUtils.error("Group '" + args[3] + "' not found!")
                            return false;
                        }
                        if (group.addImport(args[3])){
                            moduleUtils.log("Group '"+args[1]+"' linked with '"+args[3]+"'!",undefined,"color:#00FF00")
                            group.save();
                        }else{
                            moduleUtils.error("Couldn't link group '"+args[1]+"' with '"+args[3]+"'!")
                            return false;
                        }
                        return;
                    case "unlink":
                        if (!cs.hasPermission("permissions.group.edit.link")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        if (group.removeImport(args[3])){
                            moduleUtils.log("Group '"+args[1]+"' unlinked from '"+args[3]+"'!",undefined,"color:#00FF00")
                            group.save();
                        }else{
                            moduleUtils.error("Couldn't unlink group '"+args[1]+"' from '"+args[3]+"'!")
                            return false;
                        }
                        return;
                    default:
                        groupHelp(moduleUtils);
                        return;
                }
            }else{
                fullHelp(moduleUtils);
            }
        }else if (args.length===3){
            if (args[0].toLowerCase()==="user"){
                switch (args[2].toLowerCase()){
                    case "unlink":
                        if (!cs.hasPermission("permissions.user.edit.link")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        user = permissionsManager.getUser(args[1]);
                        if (user === undefined||user.getName()==="console") {
                            moduleUtils.error("User '" + args[1] + "' not found!")
                            return;
                        }
                        user.setDiscordID("");
                        user.save();
                        moduleUtils.log("Unlinked the user from discord!",undefined,"color:#00FF00")
                        return;
                    case "create":
                        if (!cs.hasPermission("permissions.user.create")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        args[1] = args[1].toLowerCase();
                        user = permissionsManager.getUser(args[1]);
                        console.log(user);
                        if (user !== undefined) {
                            moduleUtils.error("User '" + args[1] + "' already exists!")
                            return;
                        }
                        new permissionsManager.PermissionUser({
                            discord: {
                                userID: ""
                            },
                            groups: [],
                            permissions: [],
                            name: args[1],
                            web: {
                                canLogin: false,
                                password: "",
                                token: "",
                                timeout: 0
                            }
                        }, permissionsManager).save();
                        moduleUtils.log("User '" + args[1] + "' was created!",undefined,"color:#00FF00")
                        return;
                    case "delete":
                        if (!cs.hasPermission("permissions.user.delete")){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        user = permissionsManager.getUser(args[1]);
                        if (user === undefined||user.getName()==="console") {
                            moduleUtils.error("User '" + args[1] + "' not found!")
                            return;
                        }
                        user.delete();
                        moduleUtils.log("User '" + args[1] + "' was deleted!",undefined,"color:#00FF00")
                        return;
                    default:
                        userHelp(moduleUtils);
                        return;
                }
            }else if (args[0].toLowerCase()==="group"){
                switch (args[2].toLowerCase()){
                    case "create":
                        if (!cs.hasPermissions(["permissions.group.create"])){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        args[1] = args[1].toLowerCase();
                        group = permissionsManager.getGroup(args[1]);
                        if (group !== undefined) {
                            moduleUtils.error("Group '" + args[1] + "' already exists!")
                            return;
                        }
                        new permissionsManager.PermissionGroup({
                            importFrom: [],
                            permissions: [],
                            name: args[1]
                        }, permissionsManager).save();
                        moduleUtils.log("Group '" + args[1] + "' was created!",undefined,"color:#00FF00")
                        return;
                    case "delete":
                        if (!cs.hasPermissions(["permissions.group.delete"])){
                            moduleUtils.noPermissions();
                            return false;
                        }
                        group = permissionsManager.getGroup(args[1]);
                        if (group === undefined) {
                            moduleUtils.error("Group '" + args[1] + "' not found!")
                            return;
                        }
                        moduleUtils.log("Deleting Group '" + args[1] + "'...");
                        group.delete();
                        moduleUtils.log("Group '" + args[1] + "' was deleted!",undefined,"color:#00FF00");
                        return;
                    default:
                        groupHelp(moduleUtils);
                        return;
                }
            }else{
                fullHelp(moduleUtils);
            }
        }


    },
    info: "Manage groups and users"
}