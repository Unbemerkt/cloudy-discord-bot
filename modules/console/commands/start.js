const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
const dCore = moduleLoader.getModule("discordCore")
module.exports = {
    id: "start",
    action(moduleUtils, cmd, args, cs) {
        if (dCore.onlineState!=="OFFLINE"){
            moduleUtils.log("DiscordBot is already online!");
            return;
        }
        moduleUtils.log("Starting DiscordBot...",undefined,"color:#00FF00");
        dCore.start();
    },
    permissions: [
        "bot.start"
    ],
    info: "Starts the discord bot"
}