const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
const dCore = moduleLoader.getModule("discordCore")
module.exports = {
    id: "clear",
    aliases: [
        "cl"
    ],
    action(moduleUtils, cmd, args, cs) {
        moduleUtils.clear(cs);
    },
    info: "Deletes the log",
    permissions: [
        "console.clear"
    ]
}