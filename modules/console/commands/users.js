const moduleLoader = require("./../../../moduleLoader");
const commandManager = moduleLoader.getModule("console");
const permissionsManager = moduleLoader.getModule("permissions")
module.exports = {
    id: "users",
    action(moduleUtils, cmd, args, cs) {
        moduleUtils.pureLog("")
        moduleUtils.log("All users: ");
        var i = 0;
        var userList = permissionsManager.getAllUsers();
        userList.forEach(user=>{
            if (i===userList.length-1){
                moduleUtils.log(" ╚› "+user.getName());
            }else{
                moduleUtils.log(" ╟› "+user.getName());
            }
            i++;
        })
    },
    args: [],
    permissions: [
        "users.list"
    ],
    aliases: [
        "u"
    ],
    optionalPermissions: [],
    info: "Lists all users"
}