const fs = require("fs");
const path = require("path");
module.exports = class {
    moduleUtils

    constructor(moduleUtils) {
        this.moduleUtils = moduleUtils;
    }

    getPlaylists(guildID, advanced) {
        if (!fs.existsSync(__dirname + "/data/playlists/" + guildID + "/")) {
            return {};
        }
        const foldersPath = __dirname + "/data/playlists/" + guildID + "/";
        const playLists = fs.readdirSync(foldersPath).filter(file => file.endsWith('.json'));
        const result = {};
        for (const file of playLists) {
            const plPath = path.join(foldersPath, file);
            delete require.cache[require.resolve(plPath)];
            const name = file.replace(".json", "");
            result[name] = require(plPath);
            if (!advanced) delete result[name].advanced;
        }
        return result;
    }

    getPlaylistsSimple(guildID) {
        if (!fs.existsSync(__dirname + "/data/playlists/" + guildID + "/")) {
            return {};
        }
        const foldersPath = __dirname + "/data/playlists/" + guildID + "/";
        const playLists = fs.readdirSync(foldersPath).filter(file => file.endsWith('.json'));
        const result = {};
        for (const file of playLists) {
            const plPath = path.join(foldersPath, file);
            delete require.cache[require.resolve(plPath)];
            const tmp = require(plPath);
            result[file.replace(".json", "")] = {
                trackCount: tmp.simple.length,
                updateTracks: tmp.updateTracks
            }
        }
        return result;
    }

    createPlaylist(guildID, name, simpleTracks) {
        if (!fs.existsSync(__dirname + "/data/playlists/" + guildID + "/")) {
            fs.mkdirSync(__dirname + "/data/playlists/" + guildID + "/")
        }
        if (fs.existsSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json")) {
            return undefined;
        }
        const newPL = {
            simple: simpleTracks,
            advanced: {
                updateTracks: true,
                playlist: []
            }
        }
        fs.writeFileSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json", JSON.stringify(newPL, null, 2));
        newPL.name = name;
        return newPL;
    }

    updatePlaylist(guildID, name, simpleTracks) {
        if (!fs.existsSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json")) {
            return undefined;
        }
        const pl = this.getPlaylist(guildID, name);
        pl.simple = simpleTracks;
        pl.advanced.updateTracks = true;
        pl.advanced.playlist = [];
        fs.writeFileSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json", JSON.stringify(pl, null, 2));
        pl.name = name;
        return pl;
    }

    getPlaylist(guildID, name) {
        if (!fs.existsSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json")) {
            return undefined;
        }
        delete require.cache[require.resolve(__dirname + "/data/playlists/" + guildID + "/" + name + ".json")];
        const map = require(__dirname + "/data/playlists/" + guildID + "/" + name + ".json");
        map.name = name
        return map;
    }

    setEncodedTracks(guildID, name, encodedTracks){
        const pl = this.getPlaylist(guildID,name);
        if (pl===undefined)return;
        pl.advanced.updateTracks = false;
        pl.advanced.playlist = encodedTracks;
        fs.writeFileSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json", JSON.stringify(pl, null, 2));
    }

    deleteList(guildID, name){
        if (!fs.existsSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json")) {
            return false;
        }
        fs.unlinkSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json");
        return true;
    }
    renamePlaylist(guildID, name, newName){
        if (!fs.existsSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json")) {
            return undefined;
        }
        if (fs.existsSync(__dirname + "/data/playlists/" + guildID + "/" + newName + ".json")) {
            return false;
        }
        fs.renameSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json",
            __dirname + "/data/playlists/" + guildID + "/" + newName + ".json")

        delete require.cache[require.resolve(__dirname + "/playlists/" + guildID + "/" + name + ".json")];
        delete require.cache[require.resolve(__dirname + "/playlists/" + guildID + "/" + newName + ".json")];
        const map = require(__dirname + "/data/playlists/" + guildID + "/" + newName + ".json");
        map.name = newName
        delete map.advanced;
        return map;
    }

    copyPlaylist(guildID, name, destName){
        if (!fs.existsSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json")) {
            return undefined;
        }
        if (fs.existsSync(__dirname + "/data/playlists/" + guildID + "/" + destName + ".json")) {
            return false;
        }
        fs.copyFileSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json",
            __dirname + "/data/playlists/" + guildID + "/" + destName + ".json")

        delete require.cache[require.resolve(__dirname + "/data/playlists/" + guildID + "/" + destName + ".json")];
        const map = require(__dirname + "/data/playlists/" + guildID + "/" + destName + ".json");
        map.name = destName
        delete map.advanced;
        return map;
    }

    copyPlaylistToGuild(guildID, name, destGuild){
        if (!fs.existsSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json")) {
            return undefined;
        }
        if (!fs.existsSync(__dirname + "/data/playlists/" + destGuild + "/")) {
            fs.mkdirSync(__dirname + "/data/playlists/" + destGuild + "/")
        }
        if (fs.existsSync(__dirname + "/data/playlists/" + destGuild + "/" + name + ".json")) {
            return false;
        }
        fs.copyFileSync(__dirname + "/data/playlists/" + guildID + "/" + name + ".json",
            __dirname + "/data/playlists/" + destGuild + "/" + name + ".json")

        delete require.cache[require.resolve(__dirname + "/data/playlists/" + destGuild + "/" + name + ".json")];
        const map = require(__dirname + "/data/playlists/" + destGuild + "/" + name + ".json");
        map.name = name
        delete map.advanced;
        return map;
    }


}