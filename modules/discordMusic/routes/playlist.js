const express = require("express");
const app = require("../../express/module");
const moduleLoader = require("../../../moduleLoader");
const dCore = require("../../discordCore/module");
const {ModuleUtils} = require("../../../ModuleUtils");
const router = express.Router();
router.routeInfo = {
    path: "/playList",
}
// https://discord-player.js.org/guide/faq/serialization-and-deserialization
// https://discord-player.js.org/guide/examples/common-actions
var permissionModule;
var musicModule;
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    res.json({type: "ERR", data: "INVALID_REQUEST"});
});
router.get("/:guildID",function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    if (musicModule === undefined) {
        musicModule = moduleLoader.getModule("discordMusic");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }
    const guildID = req.params.guildID||"GLOBAL";
    const playlistManager = musicModule.playlistManager;
    res.json({type: "OK", guild: guildID, playlists: playlistManager.getPlaylists(guildID)});
});
router.post("/:guildID", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    if (musicModule === undefined) {
        musicModule = moduleLoader.getModule("discordMusic");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const guildID = req.params.guildID;
    if (guildID===undefined||guildID==="undefined"||guildID==="INVALID"){
        res.json({type: "ERR", data: "INVALID_GUILD"});
        return;
    }
    const playlist = req.body.playlist;
    if (playlist===undefined){
        res.json({type: "ERR", data: "INVALID_PLAYLIST"});
        return;
    }
    const action = req.body.action;
    if (action===undefined){
        res.json({type: "ERR", data: "INVALID_ACTION"});
        return;
    }
    var pl;
    switch (action) {
        case "DELETE":
            pl = musicModule.playlistManager.deleteList(guildID,playlist)
            if (pl){
                res.json({type: "OK", data: "PL_DELETED"});
            }else{
                res.json({type: "ERR", data: "PL_DOES_NOT_EXISTS"});
            }
            break;
        case "CREATE":
            pl = musicModule.playlistManager.createPlaylist(guildID,playlist,req.body.tracks||[])
            if (pl===undefined){
                res.json({type: "ERR", data: "PL_ALREADY_EXISTS"});
            }else{
                res.json({type: "OK", data: "PL_CREATED", playList: pl});
            }
            break;
        case "UPDATE":
            if (req.body.tracks===undefined){
                res.json({type: "ERR", data: "INVALID_PLAYLIST_DATA"});
                return;
            }
            const tracks = JSON.parse(req.body.tracks);
            pl = musicModule.playlistManager.updatePlaylist(guildID,playlist,tracks)
            if (pl===undefined){
                res.json({type: "ERR", data: "PL_DOES_NOT_EXISTS"});
            }else{
                res.json({type: "OK", data: "PL_UPDATED", playList: pl});
            }
            break;
        case "RENAME":
            const newName = req.body.newName;
            if (newName===undefined){
                res.json({type: "ERR", data: "INVALID_NAME"});
                return;
            }
            pl = musicModule.playlistManager.renamePlaylist(guildID,playlist,newName);
            if (pl===undefined){
                res.json({type: "ERR", data: "PL_DOES_NOT_EXISTS"});
            }else if (pl===false){
                res.json({type: "ERR", data: "PL_ALREADY_EXISTS"});
            }else{
                res.json({type: "OK", data: "PL_RENAMED", playList: pl});
            }
            break
        case "COPY":
            const destName = req.body.newName;
            if (destName===undefined){
                res.json({type: "ERR", data: "INVALID_NAME"});
                return;
            }
            pl = musicModule.playlistManager.copyPlaylist(guildID,playlist,destName);
            if (pl===undefined){
                res.json({type: "ERR", data: "PL_DOES_NOT_EXISTS"});
            }else if (pl===false){
                res.json({type: "ERR", data: "PL_ALREADY_EXISTS"});
            }else{
                res.json({type: "OK", data: "PL_COPIED", playList: pl});
            }
            break
        case "COPY_SERVER":
            const destGuild = req.body.newName;
            if (destGuild===undefined){
                res.json({type: "ERR", data: "INVALID_GUILD"});
                return;
            }
            pl = musicModule.playlistManager.copyPlaylistToGuild(guildID,playlist,destGuild);
            if (pl===undefined){
                res.json({type: "ERR", data: "PL_DOES_NOT_EXISTS"});
            }else if (pl===false){
                res.json({type: "ERR", data: "PL_ALREADY_EXISTS"});
            }else{
                res.json({type: "OK", data: "PL_COPIED_TO_GUILD", playList: pl});
            }
            break
        default:
            res.json({type: "ERR", data: "INVALID_ACTION"});
            return;
    }
});
module.exports = router;