const express = require("express");
const app = require("../../express/module");
const moduleLoader = require("../../../moduleLoader");
const dCore = require("../../discordCore/module");
const { useQueue, useHistory} = require('discord-player');
const router = express.Router();
router.routeInfo = {
    path: "/discordMusic",
    nav: "DiscordMusic",
    right: false
}
var permissionModule;
var musicModule;
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req, res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }
    res.sendFile(__dirname + "/pages/index.html");
});

router.post("/:guildID", async function (req, res, next) {
    if (!app.validateRequest(req, res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    if (musicModule === undefined) {
        musicModule = moduleLoader.getModule("discordMusic");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const guildID = req.params.guildID;
    if (guildID === undefined || guildID === "undefined" || guildID === "INVALID") {
        res.json({type: "ERR", data: "INVALID_GUILD"});
        return;
    }
    if (musicModule.playingGuilds[guildID] === undefined) {
        res.json({type: "ERR", data: "GUILD_NOT_PLAYING"});
        return;
    }
    const playingData = musicModule.moduleUtils.copyMap(musicModule.playingGuilds[guildID]);
    delete playingData.interaction;
    const action = req.body.action;
    if (action === undefined) {
        res.json({type: "ERR", data: "INVALID_ACTION"});
        return;
    }
    var queue;
    var history;
    delete playingData.tracks;
    queue = useQueue(guildID);
    if (queue === null || queue.node === null) {
        res.json({type: "ERR", data: "REQUEST_FAILED", guildID, playingData});
        return;
    }
    playingData.current.isPaused = queue.node.isPaused();
    playingData.current.volume = queue.node.volume;
    playingData.current.mode = queue.repeatMode;
    playingData.current.duration = queue.currentTrack.durationMS;
    playingData.current.progress = queue.node.getTimestamp().current.value;
    switch (action) {
        case "get":
            res.json({type: "OK", data: playingData, guildID});
            break;
        case "pause":
            if (queue.node.isPaused()) {
                queue.node.setPaused(false);
                res.json({type: "OK", data: "RESUMED", guildID, playingData});
            } else {
                queue.node.setPaused(true);
                res.json({type: "OK", data: "PAUSED", guildID, playingData});
            }
            break;
        case "stop":
            queue.delete();
            delete musicModule.playingGuilds[guildID];
            res.json({type: "OK", data: "STOPPED", guildID});
            break;
        case "volume":
            if (req.body.volume===undefined){
                res.json({type: "ERR", data: "MISSING_VOLUME", guildID, playingData});
                return;
            }
            var vol = musicModule.moduleUtils.clamp(parseInt(req.body.volume),100,0);
            queue.node.setVolume(vol)
            res.json({type: "OK", data: "VOLUME_SET", volume: vol, guildID, playingData});
            break;
        case "skip":
            queue.node.skip()
            res.json({type: "OK", data: "SKIPPED", guildID, playingData});
            break;
        case "back":
            history = useHistory(guildID);
            if (history === null) {
                res.json({type: "ERR", data: "REQUEST_FAILED", guildID, playingData});
                return;
            }
            if (history.tracks.size === 0) {
                res.json({type: "ERR", data: "HISTORY_EMPTY", guildID, playingData});
                return;
            }
            await history.previous();
            res.json({type: "OK", data: "GOING_BACK", guildID, playingData});
            break;
        case "loop":
            if (req.body.mode===undefined){
                res.json({type: "ERR", data: "MISSING_MODE", guildID, playingData});
                return;
            }
            var mode = parseInt(req.body.mode);
            switch (mode){
                case 0:
                    queue.setRepeatMode(mode)
                    res.json({type: "OK", data: "UPDATED_MODE", mode: "DISABLED", guildID, playingData});
                    break;
                case 1:
                    queue.setRepeatMode(mode)
                    res.json({type: "OK", data: "UPDATED_MODE", mode: "LOOP_TRACK", guildID, playingData});
                    break;
                case 2:
                    queue.setRepeatMode(mode)
                    res.json({type: "OK", data: "UPDATED_MODE", mode: "LOOP_QUEUE", guildID, playingData});
                    break;
                case 3:
                    queue.setRepeatMode(mode)
                    res.json({type: "OK", data: "UPDATED_MODE", mode: "AUTOPLAY", guildID, playingData});
                    break;
                default:
                    res.json({type: "ERR", data: "INVALID_MODE", min: "0", max: "3", guildID, playingData});
                    break;
            }
            break;
        default:
            res.json({type: "ERR", data: "INVALID_ACTION"});
            return;
    }
});

router.post("/", function (req, res, next) {
    if (!app.validateRequest(req, res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    if (musicModule === undefined) {
        musicModule = moduleLoader.getModule("discordMusic");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    var guilds = musicModule.moduleUtils.copyMap(musicModule.playingGuilds)
    Object.keys(guilds).forEach(id=>{
        delete guilds[id].interaction
    })
    res.json({type: "OK", data: guilds});
});

module.exports = router;