const express = require("express");
const app = require("../../express/module");
const moduleLoader = require("../../../moduleLoader");
const dCore = require("../../discordCore/module");
const AutocompleteManager = require("./../AutocompleteManager");
const {ModuleUtils} = require("../../../ModuleUtils");
const router = express.Router();
router.routeInfo = {
    path: "/autocomplete",
}
// https://discord-player.js.org/guide/faq/serialization-and-deserialization
// https://discord-player.js.org/guide/examples/common-actions
var permissionModule;
var musicModule;
router.get("/", function (req, res, next) {
    res.json({type: "ERR", data: "INVALID_REQUEST"});
});
router.get("/:guildID",function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    if (musicModule === undefined) {
        musicModule = moduleLoader.getModule("discordMusic");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }
    const guildID = req.params.guildID||"GLOBAL";

    const autocompleteManager = new AutocompleteManager(guildID);
    res.json({type: "OK", guild: guildID, songs: autocompleteManager.getList(true).songs});
});
router.post("/:guildID", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    if (musicModule === undefined) {
        musicModule = moduleLoader.getModule("discordMusic");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const guildID = req.params.guildID;
    if (guildID===undefined||guildID==="undefined"||guildID==="INVALID"){
        res.json({type: "ERR", data: "INVALID_GUILD"});
        return;
    }
    const autocompleteManager = new AutocompleteManager(guildID);
    const action = req.body.action;
    if (action===undefined){
        res.json({type: "ERR", data: "INVALID_ACTION"});
        return;
    }
    var title;
    var url;
    var tmpManager;
    switch (action) {
        case "DELETE":
            title = req.body.title;
            if (title===undefined){
                res.json({type: "ERR", data: "INVALID_TITLE"});
                return;
            }
            autocompleteManager.remove(title);
            res.json({type: "OK", data: "SONG_REMOVED", title: title});
            break;
        case "SAVE":
            //save complete list
            if (req.body.songs===undefined){
                res.json({type: "ERR", data: "INVALID_DATA"});
                return;
            }
            const songs = JSON.parse(req.body.songs);
            autocompleteManager.save(songs);
            res.json({type: "OK", data: "LIST_SAVED", songs: songs});
            break;
        case "CREATE":
            title = req.body.title;
            if (title===undefined){
                res.json({type: "ERR", data: "INVALID_TITLE"});
                return;
            }
            url = req.body.url;
            if (url===undefined){
                res.json({type: "ERR", data: "INVALID_URL"});
                return;
            }
            autocompleteManager.addToList(title,url);
            res.json({type: "OK", data: "SONG_ADDED", title: title});
            break;
        case "RENAME":
            const newTitle = req.body.newTitle;
            if (newTitle===undefined){
                res.json({type: "ERR", data: "INVALID_TITLE"});
                return;
            }
            title = req.body.title;
            if (title===undefined){
                res.json({type: "ERR", data: "INVALID_TITLE"});
                return;
            }
            url = autocompleteManager.getUrl(title,true);
            if (url===undefined){
                res.json({type: "ERR", data: "SONG_NOT_FOUND"});
                return;
            }
            autocompleteManager.remove(title);
            autocompleteManager.addToList(title,url);
            res.json({type: "OK", data: "SONG_RENAMED", new_title: newTitle, old_title: title});
            break
        case "COPY":
            const destName = req.body.newName;
            if (destName===undefined){
                res.json({type: "ERR", data: "INVALID_NAME"});
                return;
            }
            title = req.body.title;
            if (title===undefined){
                res.json({type: "ERR", data: "INVALID_TITLE"});
                return;
            }
            url = autocompleteManager.getUrl(title,true);
            if (url===undefined){
                res.json({type: "ERR", data: "SONG_NOT_FOUND"});
            }else if (autocompleteManager.getUrl(destName,true)!==undefined){
                res.json({type: "ERR", data: "SONG_ALREADY_EXISTS"});
            }else{
                autocompleteManager.addToList(destName,url);
                res.json({type: "OK", data: "PL_COPIED", playList: pl});
            }
            break
        case "COPY_SERVER":
            const destGuild = req.body.destGuild;
            if (destGuild===undefined){
                res.json({type: "ERR", data: "INVALID_GUILD"});
                return;
            }
            tmpManager = new AutocompleteManager(destGuild);
            title = req.body.title;
            if (title===undefined){
                res.json({type: "ERR", data: "INVALID_TITLE"});
                return;
            }
            url = autocompleteManager.getUrl(title,true);
            if (url===undefined){
                res.json({type: "ERR", data: "SONG_NOT_FOUND"});
            }else if (tmpManager.getUrl(title,true)!==undefined){
                res.json({type: "ERR", data: "SONG_ALREADY_EXISTS"});
            }else{
                tmpManager.addToList(title,url);
                res.json({type: "OK", data: "PL_COPIED_TO_GUILD", title: title});
            }
            break
        default:
            res.json({type: "ERR", data: "INVALID_ACTION"});
            return;
    }
});
module.exports = router;