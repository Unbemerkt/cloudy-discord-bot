const express = require("express");
const app = require("../../express/module");
const moduleLoader = require("../../../moduleLoader");
const dCore = require("../../discordCore/module");
const router = express.Router();
router.routeInfo = {
    path: "/playlistEditor",
    nav: "Playlist Editor",
    right: false
}
var permissionModule;
router.get("/", function (req, res, next) {
    if (!app.validateRequest(req,res)) return;
    if (permissionModule === undefined) {
        permissionModule = moduleLoader.getModule("permissions");
    }
    const token = req.cookies.TOKEN;
    if (token === undefined) {
        res.json({type: "ERR", data: "AUTH_NEEDED"})
        return;
    }
    const user = permissionModule.authenticateToken(token);
    if (user === undefined) {
        res.json({type: "ERR", data: "TOKEN_INVALID", token})
        return;
    }
    res.sendFile(__dirname+"/pages/playlistEditor.html");
});

module.exports = router;