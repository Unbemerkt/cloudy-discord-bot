const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const {useMainPlayer, useQueue} = require('discord-player');
const moduleLoader = require("./../../../moduleLoader");
const AutocompleteManager = require("../AutocompleteManager");
const discordUserClass = require("../../discordCommandManager/DiscordUser");
var musicModule;
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder().setName("play").setDescription("Play some music")
        .addStringOption(option => {
            return option.setName("query").setDescription("Search for music or url")
                .setAutocomplete(true).setRequired(true);
        })
        .addNumberOption(option => {
            return option.setName("volume").setDescription("Set Volume)").setMaxValue(100).setMinValue(0)
        }),
    async execute(interaction) {
        if (musicModule === undefined) musicModule = moduleLoader.getModule("discordMusic");

        var localisation = musicModule.localisation;
        const loc = localisation[interaction.locale] ?? localisation["en"];

        if (permissionManager === undefined) permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermissions(["discord.command.play", "music.play"])) {
            embedMessage = new EmbedBuilder()
                .setTitle(loc["errorTitle"])
                .setDescription(loc["noPermissions"])
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }

        const player = useMainPlayer();
        const channel = interaction.member.voice.channel;
        if (!channel) return await interaction.reply(loc["notConnected"]); // make sure we have a voice channel

        // let's defer the interaction as things can take time to process
        await interaction.deferReply();
        if (interaction.guild.members.me.voice.channel &&
            interaction.member.voice.channelId !== interaction.guild.members.me.voice.channelId) {
            return await interaction.followUp({content: loc["notSameChannel"], ephemeral: true});
        }

        var query = interaction.options.getString('query', true); // we need input/query to play
        var usedSaved = false;
        var realQuery = query;
        const autocompleteManager = new AutocompleteManager(interaction.guildId);
        const autoResult = autocompleteManager.getUrl(query);
        if (autoResult !== undefined) {
            query = autoResult;
            usedSaved = true;
        }


        try {
            var firstPlay = false;
            if (musicModule.playingGuilds[interaction.guildId] === undefined) {
                firstPlay = true;
                musicModule.playingGuilds[interaction.guildId] = {}
                musicModule.playingGuilds[interaction.guildId].followUp = true
                musicModule.playingGuilds[interaction.guildId]["tracks"] = {}
            }
            const volumeOption = interaction.options.getNumber("volume");
            const options = {
                nodeOptions: {
                    // nodeOptions are the options for guild node (aka your queue in simple word)
                    metadata: interaction, // we can access this metadata object using queue.metadata later on
                    channel: channel,
                    bufferingTimeout: 15000,
                    leaveOnStop: true,
                    leaveOnStopCooldown: 5000,
                    leaveOnEnd: true,
                    leaveOnEndCooldown: 15000,
                    leaveOnEmpty: true,
                    leaveOnEmptyCooldown: 300000,
                    skipOnNoStream: true,
                }
            }
            if (firstPlay){
                if (volumeOption === null) {
                    options.nodeOptions.volume = musicModule.config.defaultVolume;
                } else {
                    options.nodeOptions.volume = volumeOption;
                }
            }
            const {track, queue} = await player.play(channel, query, options);
            var title = track.title;
            if (usedSaved) {
                title = realQuery;
            }
            if (firstPlay) {
                if (musicModule.playingGuilds[interaction.guildId]["tracks"]===undefined)musicModule.playingGuilds[interaction.guildId]["tracks"] = {};
                musicModule.playingGuilds[interaction.guildId]["tracks"][track.title] = title
                musicModule.playingGuilds[interaction.guildId]["interaction"] = interaction
            } else {
                if (volumeOption !== null) {
                    queue.node.setVolume(volumeOption)
                }
                musicModule.playingGuilds[interaction.guildId]["tracks"][track.title] = title
                musicModule.playingGuilds[interaction.guildId]["interaction"] = interaction
                musicModule.playingGuilds[interaction.guildId].followUp = false
                await interaction.followUp(loc["trackQueued"].replaceAll(/%TRACK_TITLE%/g, title));
            }
        } catch (e) {
            // let's return error if something failed
            console.error(e);
            return interaction.followUp(loc["error"].replaceAll(/%ERROR%/g, e.toString()));
        }
    },
    async autoComplete(interaction) {
        if (musicModule === undefined) musicModule = moduleLoader.getModule("discordMusic");
        if (permissionManager === undefined) {
            permissionManager = moduleLoader.getModule("permissions");
        }
        const user = permissionManager.getUser(interaction.user.id);
        if (user === undefined || !user.hasPermissions(["discord.command.play","music.play"])) {
            await interaction.respond([{name: "No Permission", value: "invalid"}],);
            return;
        }
        const focusedValue = interaction.options.getFocused().toLowerCase();
        const autocompleteManager = new AutocompleteManager(interaction.guildId);
        var choices = Object.keys(autocompleteManager.getList().songs);
        if (choices.length===0){
            await interaction.respond([{name: "No saved songs found!", value: "invalid"}]);
            return;
        }
        choices = choices.filter(choice => choice.toLowerCase().includes(focusedValue));
        if (choices.length>25){
            var tmp = [];
            for (let i = 0; i < 25&&i<choices.length; i++) {
                tmp.push(choices[i]);
            }
            choices = tmp;
        }
        await interaction.respond(
            choices.map(choice => ({name: choice, value: choice})),
        );
    }
}