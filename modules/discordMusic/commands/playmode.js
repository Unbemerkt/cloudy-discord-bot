const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const {useMainPlayer, useQueue} = require('discord-player');
const moduleLoader = require("../../../moduleLoader");
const discordUserClass = require("../../discordCommandManager/DiscordUser");
var musicModule;
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder().setName("playmode").setDescription("Loop the queue or enable autoplay")
        .addIntegerOption(option => {
            return option.setName("mode").setDescription("Mode")
                .setAutocomplete(true)
                .setMinValue(0).setMaxValue(3);
        }),
    async execute(interaction) {
        if (musicModule === undefined) musicModule = moduleLoader.getModule("discordMusic");
        var localisation = musicModule.localisation;
        const loc = localisation[interaction.locale] ?? localisation["en"];
        if (permissionManager === undefined) permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermissions(["discord.command.playmode","music.playmode"])) {
            embedMessage = new EmbedBuilder()
                .setTitle(loc["errorTitle"])
                .setDescription(loc["noPermissions"])
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        await interaction.deferReply();
        if (!interaction.guild.members.me.voice.channel){
            return void interaction.followUp(loc["noPlaying"]);
        }
        if (interaction.member.voice.channelId !== interaction.guild.members.me.voice.channelId){
            return await interaction.followUp({ content: loc["notSameChannel"], ephemeral: true });
        }

        const queue = useQueue(interaction.guildId);
        const mode = interaction.options.getInteger("mode");
        // let's defer the interaction as things can take time to process
        if (mode === null) {
            //send help
            return interaction.followUp({
                "embeds": [
                    {
                        "type": "rich",
                        "title": `Cloudy » PlayMode help`,
                        "description": "",
                        "color": 0x40abf3,
                        "fields": [
                            {
                                "name": `Disable Loop/Autoplay`,
                                "value": `\`playmode 0\``,
                                "inline": true
                            },
                            {
                                "name": `Loop current track`,
                                "value": `\`playmode 1\``,
                                "inline": true
                            },
                            {
                                "name": `Loop current queue`,
                                "value": `\`playmode 2\``,
                                "inline": true
                            },
                            {
                                "name": `Autoplay`,
                                "value": `\`playmode 3\``,
                                "inline": true
                            }
                        ],
                        "author": {
                            "name": `Cloudy`,
                            "icon_url": `https://cdn.discordapp.com/app-icons/959806596618256434/ec164ae07c8414ae624efdfa5dae7118.png`,
                            "proxy_icon_url": `https://cdn.discordapp.com/app-icons/959806596618256434/ec164ae07c8414ae624efdfa5dae7118.png`
                        }
                    }
                ]
            });
        }


        try {
            if (queue === null) return void interaction.followUp({content: loc["unable"]});
            if (musicModule.playingGuilds[interaction.guildId]===undefined)musicModule.playingGuilds[interaction.guildId] = {};
            switch (mode) {
                case 0:
                    // musicModule.playingGuilds[interaction.guildId].playMode = mode;
                    queue.setRepeatMode(mode)
                    return interaction.followUp(loc["playMode_0"]);
                case 1:
                    // musicModule.playingGuilds[interaction.guildId].playMode = mode;
                    queue.setRepeatMode(mode)
                    return interaction.followUp(loc["playMode_1"]);
                case 2:
                    // musicModule.playingGuilds[interaction.guildId].playMode = mode;
                    queue.setRepeatMode(mode)
                    return interaction.followUp(loc["playMode_2"]);
                case 3:
                    // musicModule.playingGuilds[interaction.guildId].playMode = mode;
                    queue.setRepeatMode(mode)
                    return interaction.followUp(loc["playMode_3"]);
                default:
                    return interaction.followUp(loc["playMode_invalid"]);
            }
        } catch (e) {
            // let's return error if something failed
            console.error(e);
            return interaction.followUp(loc["error"].replaceAll(/%ERROR%/g, e.toString()));
        }
    },
    async autoComplete(interaction) {
        const focusedValue = interaction.options.getFocused().toLowerCase();
        const choices = [
            {name: "Loop/Autoplay disabled", value: 0},
            {name: "Loop current track", value: 1},
            {name: "Loop current queue", value: 2},
            {name: "Autoplay", value: 3}
        ];

        const filtered = choices.filter(choice => choice.name.toLowerCase().startsWith(focusedValue));
        await interaction.respond(
            filtered.map(choice => ({name: choice.name, value: choice.value})),
        );
    }
}