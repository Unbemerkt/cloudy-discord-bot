const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const {useMainPlayer, useQueue} = require('discord-player');
const moduleLoader = require("./../../../moduleLoader");
const fs = require("fs");
const savedURLs = require("../savedURLs.json");
const AutocompleteManager = require("../AutocompleteManager");
const discordUserClass = require("../../discordCommandManager/DiscordUser");
var musicModule;
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder().setName("autocomplete").setDescription("Edit autocomplete for /play")
        .addSubcommand(subCommand =>
            subCommand.setName("add").setDescription("add a url")
                .addStringOption(option => {
                    return option.setName("name").setDescription("Name")
                        .setRequired(true).setAutocomplete(true)
                })
                .addStringOption(option => {
                    return option.setName("url").setDescription("url")
                        .setRequired(true);
                }))
        .addSubcommand(subCommand => {
            return subCommand.setName("remove")
                .setDescription("remove a entry")
                .addStringOption(option => {
                    return option.setName("name").setDescription("entry name").setRequired(true).setAutocomplete(true)
                })
        }),
    async execute(interaction) {
        if (musicModule === undefined) musicModule = moduleLoader.getModule("discordMusic");
        var localisation = musicModule.localisation;
        const loc = localisation[interaction.locale] ?? localisation["en"];

        if (permissionManager === undefined) permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermissions(["discord.command.autocomplete","music.autocomplete"])) {
            embedMessage = new EmbedBuilder()
                .setTitle(loc["errorTitle"])
                .setDescription(loc["noPermissions"])
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        const subCommand = interaction.options.getSubcommand();
        await interaction.deferReply();
        const name = interaction.options.getString("name", true);
        var url = interaction.options.getString("url", false);
        if (name === null) {
            embedMessage = new EmbedBuilder()
                .setTitle(loc["errorTitle"])
                .setDescription(loc["invalidParameters"])
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        const autocompleteManager = new AutocompleteManager(interaction.guildId);
        if (subCommand === "add") {
            if (url === null) {
                embedMessage = new EmbedBuilder()
                    .setTitle(loc["errorTitle"])
                    .setDescription(loc["invalidParameters"])
                    .setColor("#c91616")
                interaction.reply({embeds: [embedMessage], ephemeral: true});
                return;
            }
            autocompleteManager.addToList(name,url);
        } else {
            //remove
            url = autocompleteManager.remove(name);
        }
        try {
            //save
            if (subCommand === "add") {
                embedMessage = new EmbedBuilder()
                    .setTitle(loc["successTitle"])
                    .setDescription(loc["list_add"].replaceAll(/%NAME%/g,name).replaceAll(/%URL%/g,url))
                    .setColor("#00FF00");
                await interaction.followUp({embeds: [embedMessage]});
            } else {
                embedMessage = new EmbedBuilder()
                    .setTitle(loc["successTitle"])
                    .setDescription(loc["list_remove"].replaceAll(/%NAME%/g,name).replaceAll(/%URL%/g,url))
                    .setColor("#00FF00");
                await interaction.followUp({embeds: [embedMessage]});
            }
        } catch (e) {
            // let's return error if something failed
            console.error(e);
            return interaction.followUp(loc["error"].replaceAll(/%ERROR%/g, e.toString()));
        }
    },
    async autoComplete(interaction) {
        if (musicModule === undefined) musicModule = moduleLoader.getModule("discordMusic");
        if (permissionManager === undefined) {
            permissionManager = moduleLoader.getModule("permissions");
        }
        const user = permissionManager.getUser(interaction.user.id);
        if (user === undefined || !user.hasPermissions(["discord.command.autocomplete","music.autocomplete"])) {
            await interaction.respond([{name: "No Permission", value: "invalid"}],);
            return;
        }
        const focusedValue = interaction.options.getFocused().toLowerCase();
        const autocompleteManager = new AutocompleteManager(interaction.guildId);
        var choices = Object.keys(autocompleteManager.getList().songs);
        if (choices.length===0){
            await interaction.respond([{name: "No saved songs found!", value: "invalid"}]);
            return;
        }
        choices = choices.filter(choice => choice.toLowerCase().includes(focusedValue));
        if (choices.length>25){
            var tmp = [];
            for (let i = 0; i < 25&&i<choices.length; i++) {
                tmp.push(choices[i]);
            }
            choices = tmp;
        }
        await interaction.respond(
            choices.map(choice => ({name: choice, value: choice})),
        );
    }
}