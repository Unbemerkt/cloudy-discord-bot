const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const { useQueue } = require('discord-player');
const moduleLoader = require("../../../moduleLoader");
const discordUserClass = require("../../discordCommandManager/DiscordUser");
var musicModule;
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder().setName("stop").setDescription("Stop playing music"),
    async execute(interaction) {
        if (musicModule===undefined)musicModule = moduleLoader.getModule("discordMusic");
        var localisation = musicModule.localisation;
        const loc = localisation[interaction.locale]??localisation["en"];
        if (permissionManager===undefined)permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermissions(["discord.command.stop","music.stop"])) {
            embedMessage = new EmbedBuilder()
                .setTitle(loc["errorTitle"])
                .setDescription(loc["noPermissions"])
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }

        // let's defer the interaction as things can take time to process
        await interaction.deferReply();
        try {
            if (!interaction.guild.members.me.voice.channel){
                return void interaction.followUp(loc["noPlaying"]);
            }
            if (interaction.member.voice.channelId !== interaction.guild.members.me.voice.channelId) {
                return await interaction.followUp({content: loc["notSameChannel"], ephemeral: true});
            }
            const queue = useQueue(interaction.guildId);
            if (queue===null||queue.currentTrack===undefined) return void interaction.followUp(loc["noPlaying"]);
            queue.delete();
            if (musicModule.playingGuilds[interaction.guildId]!==undefined){
                delete musicModule.playingGuilds[interaction.guildId];
            }

            return void interaction.followUp({ content: loc["stop"] });
        } catch (e) {
            // let's return error if something failed
            console.error(e);
            return interaction.followUp(loc["error"].replaceAll(/%ERROR%/g,e.toString()));
        }
    },
}