const {SlashCommandBuilder, EmbedBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle} = require("discord.js");
const { useMainPlayer, useQueue } = require('discord-player');
const moduleLoader = require("../../../moduleLoader");
const discordUserClass = require("../../discordCommandManager/DiscordUser");
var musicModule;
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder().setName("volume").setDescription("Play some music")
        .addIntegerOption(option => {
            return option.setName("volume").setDescription("Volume 0-100")
                .setMinValue(0).setMaxValue(100);
        }),
    async execute(interaction) {
        if (musicModule===undefined)musicModule = moduleLoader.getModule("discordMusic");
        var localisation = musicModule.localisation;
        const loc = localisation[interaction.locale]??localisation["en"];
        if (permissionManager===undefined)permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermissions(["discord.command.volume","music.volume"])) {
            embedMessage = new EmbedBuilder()
                .setTitle(loc["errorTitle"])
                .setDescription(loc["noPermissions"])
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }

        const queue = useQueue(interaction.guildId);
        const volume = interaction.options.getInteger("volume");
        try {
            if (!interaction.guild.members.me.voice.channel){
                return void interaction.reply(loc["noPlaying"]);
            }
            if (interaction.member.voice.channelId !== interaction.guild.members.me.voice.channelId) {
                return await interaction.reply({content: loc["notSameChannel"], ephemeral: true});
            }
            if (queue===null||queue.node===null) return void interaction.reply({ content: loc["unable"] });
            if (volume!==null)queue.node.setVolume(volume)
            updateMessage(interaction,queue,loc,undefined,volume!==null)
        } catch (e) {
            // let's return error if something failed
            console.error(e);
            return interaction.reply(loc["error"].replaceAll(/%ERROR%/g,e.toString()));
        }
    },
}

async function updateMessage(interaction,queue,loc,confirm,setVolume){


    var data = {};
    const volume = queue.node.volume;
    var emoji = "";
    if (volume>50) {
        emoji = loc["volume_loud"];
    }else if (volume===0){
        emoji = loc["volume_muted"];
    }else{
        emoji = loc["volume_medium"];
    }

    const removeOne = new ButtonBuilder()
        .setCustomId('removeOne')
        .setLabel("-1")
        .setStyle(ButtonStyle.Danger);
    const removeFive = new ButtonBuilder()
        .setCustomId('removeFive')
        .setLabel("-5")
        .setStyle(ButtonStyle.Danger);
    const removeTen = new ButtonBuilder()
        .setCustomId('removeTen')
        .setLabel("-10")
        .setStyle(ButtonStyle.Danger);
    const removeTwentyFive = new ButtonBuilder()
        .setCustomId('removeTwentyFive')
        .setLabel("-25")
        .setStyle(ButtonStyle.Danger);
    const removeFifty = new ButtonBuilder()
        .setCustomId('removeFifty')
        .setLabel("-50")
        .setStyle(ButtonStyle.Danger);


    const setZero = new ButtonBuilder()
        .setCustomId('setZero')
        .setLabel("0")
        .setStyle(ButtonStyle.Secondary);
    const setTwentyFive = new ButtonBuilder()
        .setCustomId('setTwentyFive')
        .setLabel("25")
        .setStyle(ButtonStyle.Secondary);
    const setFifty = new ButtonBuilder()
        .setCustomId('setFifty')
        .setLabel("50")
        .setStyle(ButtonStyle.Secondary);
    const setSeventyFive = new ButtonBuilder()
        .setCustomId('setSeventyFive')
        .setLabel("75")
        .setStyle(ButtonStyle.Secondary);
    const setHundred = new ButtonBuilder()
        .setCustomId('setHundred')
        .setLabel("100")
        .setStyle(ButtonStyle.Secondary);


    const addOne = new ButtonBuilder()
        .setCustomId('addOne')
        .setLabel("+1")
        .setStyle(ButtonStyle.Primary);
    const addFive = new ButtonBuilder()
        .setCustomId('addFive')
        .setLabel("+5")
        .setStyle(ButtonStyle.Primary);
    const addTen = new ButtonBuilder()
        .setCustomId('addTen')
        .setLabel("+10")
        .setStyle(ButtonStyle.Primary);
    const addTwentyFive = new ButtonBuilder()
        .setCustomId('addTwentyFive')
        .setLabel("+25")
        .setStyle(ButtonStyle.Primary);
    const addFifty = new ButtonBuilder()
        .setCustomId('addFifty')
        .setLabel("+50")
        .setStyle(ButtonStyle.Primary);
    const row = new ActionRowBuilder()
        .addComponents(addOne,addFive,addTen,addTwentyFive,addFifty);
    const row2 = new ActionRowBuilder()
        .addComponents(setZero,setTwentyFive,setFifty,setSeventyFive,setHundred);
    const row3 = new ActionRowBuilder()
        .addComponents(removeOne,removeFive,removeTen,removeTwentyFive,removeFifty);

    var response;
    data = {
        content: "",
        components: [row,row2,row3]
    }
    if (!interaction.guild.members.me.voice.channel){
        data = {content: loc["noPlaying"], ephemeral: true};
        if (!interaction.replied){
            await interaction.reply(data);
        }else if (confirm!==undefined){
            await confirm.update(data);
        }else{
            await interaction.editReply(data);
        }
        return;
    }
    if (interaction.member.voice.channelId !== interaction.guild.members.me.voice.channelId){
        data = {content: loc["notSameChannel"], ephemeral: true};
        if (!interaction.replied){
            await interaction.reply(data);
        }else if (confirm!==undefined){
            await confirm.update(data);
        }else{
            await interaction.editReply(data);
        }
        return;
    }
    if (!setVolume){
        data.content = loc["currentVolume"].replaceAll(/%EMOJI%/g,emoji).replaceAll(/%VOLUME%/g,volume.toString())
    }else{
        data.content = loc["volume"].replaceAll(/%EMOJI%/g,emoji).replaceAll(/%VOLUME%/g,volume.toString())
    }
    if (!interaction.replied){
        response = await interaction.reply(data);
    }else if (confirm!==undefined){
        response = await confirm.update(data);
    }else{
        response = await interaction.editReply(data);
    }
    const collectorFilter = i => i.user.id === interaction.user.id;
    try {
        const confirmation = await response.awaitMessageComponent({filter: collectorFilter, time: 30_000});
        switch (confirmation.customId){
            case "removeOne":
                queue.node.setVolume(clamp(volume-1,100,0))
                break;
            case "removeFive":
                queue.node.setVolume(clamp(volume-5,100,0))
                break;
            case "removeTen":
                queue.node.setVolume(clamp(volume-10,100,0))
                break;
            case "removeTwentyFive":
                queue.node.setVolume(clamp(volume-25,100,0))
                break;
            case "removeFifty":
                queue.node.setVolume(clamp(volume-50,100,0))
                break;
            case "addOne":
                queue.node.setVolume(clamp(volume+1,100,0))
                break;
            case "addFive":
                queue.node.setVolume(clamp(volume+5,100,0))
                break;
            case "addTen":
                queue.node.setVolume(clamp(volume+10,100,0))
                break;
            case "addTwentyFive":
                queue.node.setVolume(clamp(volume+25,100,0))
                break;
            case "addFifty":
                queue.node.setVolume(clamp(volume+50,100,0))
                break;
            case "setZero":
                queue.node.setVolume(0)
                break;
            case "setTwentyFive":
                queue.node.setVolume(25)
                break;
            case "setFifty":
                queue.node.setVolume(50)
                break;
            case "setSeventyFive":
                queue.node.setVolume(75)
                break;
            case "setHundred":
                queue.node.setVolume(100)
                break;
        }

        updateMessage(interaction,queue,loc,confirmation,true);
    }catch (e){
        cancelInteraction(interaction,queue,loc);
    }
}
function clamp(value, max, min){
    if (value>max)return max;
    if (value<min)return min;
    return value;
}
function cancelInteraction(interaction,queue,loc){
    const volume = queue.node.volume;
    var emoji = "";
    if (volume>50) {
        emoji = loc["volume_loud"];
    }else if (volume===0){
        emoji = loc["volume_muted"];
    }else{
        emoji = loc["volume_medium"];
    }
    interaction.editReply({
        content: loc["volume"].replaceAll(/%EMOJI%/g,emoji).replaceAll(/%VOLUME%/g,volume.toString()),
        components: []
    })
}