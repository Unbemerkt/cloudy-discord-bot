const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const {useMainPlayer, useQueue, QueueRepeatMode} = require('discord-player');
const moduleLoader = require("../../../moduleLoader");
const discordUserClass = require("../../discordCommandManager/DiscordUser");
var musicModule;
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder().setName("loop").setDescription("Loop current track"),
    async execute(interaction) {
        if (musicModule === undefined) musicModule = moduleLoader.getModule("discordMusic");
        var localisation = musicModule.localisation;
        const loc = localisation[interaction.locale] ?? localisation["en"];
        if (permissionManager === undefined) permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermissions(["discord.command.loop","music.loop"])) {
            embedMessage = new EmbedBuilder()
                .setTitle(loc["errorTitle"])
                .setDescription(loc["noPermissions"])
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }

        const queue = useQueue(interaction.guildId);
        // let's defer the interaction as things can take time to process
        await interaction.deferReply();
        try {
            if (!interaction.guild.members.me.voice.channel){
                return void interaction.followUp(loc["noPlaying"]);
            }
            if (interaction.member.voice.channelId !== interaction.guild.members.me.voice.channelId){
                return await interaction.followUp({ content: loc["notSameChannel"], ephemeral: true });
            }
            if (queue === null) return void interaction.followUp({content: loc["unable"]});
            if (musicModule.playingGuilds[interaction.guildId]===undefined)musicModule.playingGuilds[interaction.guildId] = {};
            if (queue.repeatMode===QueueRepeatMode.TRACK){
                if (musicModule.playingGuilds[interaction.guildId].playMode===undefined){
                    queue.setRepeatMode(QueueRepeatMode.OFF);
                }else if (musicModule.playingGuilds[interaction.guildId].playMode!==QueueRepeatMode.TRACK){
                    queue.setRepeatMode(musicModule.playingGuilds[interaction.guildId].playMode);
                }
            }else{
                musicModule.playingGuilds[interaction.guildId].playMode = queue.repeatMode;
                queue.setRepeatMode(QueueRepeatMode.TRACK);
            }
            switch (queue.repeatMode) {
                case 0:
                    return interaction.followUp(loc["playMode_0"]);
                case 1:
                    return interaction.followUp(loc["playMode_1"]);
                case 2:
                    return interaction.followUp(loc["playMode_2"]);
                case 3:
                    return interaction.followUp(loc["playMode_3"]);
                default:
                    return interaction.followUp(loc["playMode_invalid"]);
            }
        } catch (e) {
            // let's return error if something failed
            console.error(e);
            return interaction.followUp(loc["error"].replaceAll(/%ERROR%/g, e.toString()));
        }
    }
}