const {SlashCommandBuilder, EmbedBuilder, ButtonBuilder, ButtonStyle, ActionRowBuilder} = require("discord.js");
const { useQueue } = require('discord-player');
const moduleLoader = require("../../../moduleLoader");
const discordUserClass = require("../../discordCommandManager/DiscordUser");
var musicModule;
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder().setName("pause").setDescription("Toggle pause"),
    async execute(interaction) {
        if (musicModule===undefined)musicModule = moduleLoader.getModule("discordMusic");
        var localisation = musicModule.localisation;
        const loc = localisation[interaction.locale]??localisation["en"];
        if (permissionManager===undefined)permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermissions(["discord.command.pause","music.pause"])) {
            embedMessage = new EmbedBuilder()
                .setTitle(loc["errorTitle"])
                .setDescription(loc["noPermissions"])
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        // await interaction.deferReply();
        try {
            if (!interaction.guild.members.me.voice.channel){
                return void interaction.followUp(loc["noPlaying"]);
            }
            if (interaction.member.voice.channelId !== interaction.guild.members.me.voice.channelId){
                return await interaction.followUp({ content: loc["notSameChannel"], ephemeral: true });
            }
            const queue = useQueue(interaction.guildId);
            if (queue===null||queue.node===null) return void interaction.followUp({ content: loc["unable"] });
            if (queue.node.isPaused()){
                queue.node.setPaused(false);
                return interaction.reply({ content: loc["resumed"] });
            }else{
                queue.node.setPaused(true);
                const resume = new ButtonBuilder()
                    .setCustomId('resume')
                    .setLabel(loc["resumeBtn"])
                    .setStyle(ButtonStyle.Success);
                const row = new ActionRowBuilder()
                    .addComponents(resume);
                const response = await interaction.reply({
                    content: loc["paused"],
                    components: [row]
                });
                const collectorFilter = i => i.user.id === interaction.user.id;
                try {
                    const confirmation = await response.awaitMessageComponent({filter: collectorFilter, time: 60_000});
                    queue.node.setPaused(false);
                    await confirmation.update({content: loc["resumed"],components: []});
                }catch (e){
                    //timeout
                    await interaction.editReply({ content: loc["paused"], components: [] });
                }
            }

        } catch (e) {
            // let's return error if something failed
            console.error(e);
            return interaction.followUp(loc["error"].replaceAll(/%ERROR%/g,e.toString()));
        }
    },
}