const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const {useQueue, serialize, useMainPlayer, decode, deserialize, encode} = require('discord-player');
const moduleLoader = require("../../../moduleLoader");
const {playlistManager} = require("../module");
const discordUserClass = require("../../discordCommandManager/DiscordUser");
var musicModule;
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder().setName("playlist").setDescription("Load a Playlist")
        .addStringOption((option) => {
            return option.setName("name").setDescription("Playlist to load").setRequired(true).setAutocomplete(true)
        }).addNumberOption(option=>{
            return option.setName("volume").setDescription("Set Volume)").setMaxValue(100).setMinValue(0)
        }),
    async execute(interaction) {
        if (musicModule === undefined) musicModule = moduleLoader.getModule("discordMusic");
        var localisation = musicModule.localisation;
        const loc = localisation[interaction.locale] ?? localisation["en"];
        if (permissionManager === undefined) permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermissions(["discord.command.playlist", "music.playlist"])) {
            embedMessage = new EmbedBuilder()
                .setTitle(loc["errorTitle"])
                .setDescription(loc["noPermissions"])
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }
        const plName = interaction.options.getString("name", true);
        const channel = interaction.member.voice.channel;
        if (!channel) return interaction.reply(loc["notConnected"]);
        // let's defer the interaction as things can take time to process
        await interaction.deferReply();
        try {
            if (interaction.guild.members.me.voice.channel &&
                interaction.member.voice.channelId !== interaction.guild.members.me.voice.channelId){
                return await interaction.followUp({ content: loc["notSameChannel"], ephemeral: true });
            }
            var playlist = musicModule.playlistManager.getPlaylist(interaction.guildId, plName);
            if (playlist===undefined){
                playlist = musicModule.playlistManager.getPlaylist("GLOBAL", plName);
            }

            const player = useMainPlayer();
            if (playlist === undefined) {
                return void interaction.followUp({content: loc["invalidPlaylist"]});
            }
            var setVolume = false;
            if (musicModule.playingGuilds[interaction.guildId] === undefined) {
                setVolume = true
                musicModule.playingGuilds[interaction.guildId] = {}
                musicModule.playingGuilds[interaction.guildId].loadingPL = true;
                musicModule.playingGuilds[interaction.guildId]["tracks"] = {}
                musicModule.playingGuilds[interaction.guildId]["interaction"] = interaction
            }else if (musicModule.playingGuilds[interaction.guildId].loadingPL){
                musicModule.playingGuilds[interaction.guildId].followUp = false
                await interaction.followUp({content: loc["playlistAlreadyLoading"]})
                return;
            }
            musicModule.playingGuilds[interaction.guildId].followUp = false
            const tracks = [];
            const invalidTracks = [];
            var queue;
            const volume = interaction.options.getNumber("volume");
            const options = {
                nodeOptions: {
                    // nodeOptions are the options for guild node (aka your queue in simple word)
                    metadata: interaction, // we can access this metadata object using queue.metadata later on
                    channel: channel,
                    bufferingTimeout: 15000,
                    leaveOnStop: true,
                    leaveOnStopCooldown: 5000,
                    leaveOnEnd: true,
                    leaveOnEndCooldown: 15000,
                    leaveOnEmpty: true,
                    leaveOnEmptyCooldown: 300000,
                    skipOnNoStream: true,
                }
            }
            if (setVolume){
                if (volume === null) {
                    options.nodeOptions.volume = musicModule.config.defaultVolume;
                } else {
                    options.nodeOptions.volume = volume;
                }
            }


            if (playlist.advanced.updateTracks) {
                if (playlist.simple.length === 0) {
                    await interaction.followUp({content: loc["playlistEmpty"].replaceAll(/%name%/g,plName)})
                    return;
                }
                // await interaction.followUp({content: loc["updatingTracks"]})
                var loadedMessage = await interaction.followUp({content: loc["playlistLoading"].replaceAll(/%name%/g,plName)})
                for (let i = 0; i < playlist.simple.length; i++) {
                    // console.log("Try to play... ",i)
                    const {track, q} = await player.play(channel, playlist.simple[i].url, options);
                    // if (setVolume) {
                    //     queue = useQueue(interaction.guildId);
                    //     if (queue !== undefined && queue !== null){
                    //         if (volume!==null){
                    //             queue.node.setVolume(volume);
                    //         }else{
                    //             queue.node.setVolume(musicModule.config.defaultVolume);
                    //         }
                    //         setVolume = false;
                    //     }
                    // }
                    if (track === null || track === undefined) {
                        invalidTracks.push(playlist.simple[i]);
                    } else {
                        musicModule.playingGuilds[interaction.guildId]["tracks"][track.title] = playlist.simple[i].title;
                        tracks.push(track);
                    }
                    if (playlist.simple.length!==i)
                        await loadedMessage.edit({content: loc["playlistLoadingState"].replaceAll(/%STEP%/g,i)
                            .replaceAll(/%OUT_OF%/g,playlist.simple.length).replaceAll(/%name%/g,plName)});
                }
                // const encodedTracks = [];
                // for (let i = 0; i < tracks.length; i++) {
                //     encodedTracks.push(encode(serialize(tracks[i])));
                // }
                // musicModule.playlistManager.setEncodedTracks(interaction.guildId, plName, encodedTracks)
                // await interaction.channel.send(loc["playlistUpdated"]+"2");
                musicModule.playingGuilds[interaction.guildId].loadingPL = false;
                await loadedMessage.edit(loc["playlistLoaded"].replaceAll(/%name%/g,plName));
                if (invalidTracks.length > 0) await interaction.channel.send("Some tracks couldn't be loaded! (" + invalidTracks.length + "/" + playlist.simple.length + ")");
            } else {
                //currently this part is not available/functioning
                if (playlist.advanced.playlist.length === 0) {
                    await interaction.followUp({content: loc["playlistEmpty"].replaceAll(/%name%/g,plName)})
                    return;
                }
                for (let i = 0; i < playlist.advanced.playlist.length; i++) {
                    const decodedTrack = decode(playlist.advanced.playlist[i]);
                    if (decodedTrack!==undefined) {
                        const loadedTrack = deserialize(decodedTrack);
                        if (loadedTrack !== undefined && loadedTrack !== null) {
                            const {track, q} = await loadedTrack.play(channel,options)
                            // if (setVolume) {
                            //     queue = useQueue(interaction.guildId);
                            //     if (queue !== undefined && queue !== null){
                            //         if (volume!==null){
                            //             queue.node.setVolume(volume);
                            //         }else{
                            //             queue.node.setVolume(musicModule.config.defaultVolume);
                            //         }
                            //     }
                            //     setVolume = false;
                            // }
                            if (track === null || track === undefined) {
                                invalidTracks.push(playlist.simple[i]);
                            } else {
                                tracks.push(track);
                            }

                        }
                    }else{
                        console.log("Failed to load track!")
                    }
                }

                musicModule.playingGuilds[interaction.guildId].loadingPL = false;
                await interaction.channel.send(loc["playlistLoaded"].replaceAll(/%name%/g,plName));
                if (invalidTracks.length > 0) await interaction.channel.send("Some tracks couldn't be loaded! (" + invalidTracks.length + "/" + playlist.simple.length + ")");
            }


            // queue = useQueue(interaction.guildId);
            // if (queue===undefined||queue===null)return;
            // if (setVolume) {
            //     queue.node.setVolume(musicModule.config.defaultVolume);
            // }
        } catch (e) {
            // let's return error if something failed
            console.error(e);
            await interaction.followUp(loc["error"].replaceAll(/%ERROR%/g, e.toString()));
        }
    },
    async autoComplete(interaction) {
        if (musicModule === undefined) musicModule = moduleLoader.getModule("discordMusic");
        if (permissionManager === undefined) {
            permissionManager = moduleLoader.getModule("permissions");
        }
        const user = permissionManager.getUser(interaction.user.id);
        if (user === undefined || !user.hasPermissions(["discord.command.play", "music.play"])) {
            await interaction.respond([{name: "No Permission", value: "invalid"}]);
            return;
        }
        const focusedValue = interaction.options.getFocused().toLowerCase();

        const playlists = musicModule.playlistManager.getPlaylistsSimple(interaction.guildId);
        var choices = Object.keys(playlists);
        const globalPlaylists = musicModule.playlistManager.getPlaylistsSimple("GLOBAL");
        const globalKeys = Object.keys(globalPlaylists);
        globalKeys.forEach(name=>{
            if (!choices.includes(name))choices.push(name);
        })
        if (choices.length === 0) {
            await interaction.respond([
                    {name: "No Playlists Found!", value: ""}
                ]
            );
            return;
        }
        choices = choices.filter(choice => choice.toLowerCase().includes(focusedValue));
        if (choices.length > 25) {
            var tmp = [];
            for (let i = 0; i < 25 && i < choices.length; i++) {
                tmp.push(choices[i]);
            }
            choices = tmp;
        }
        await interaction.respond(
            choices.map(choice => ({name: choice, value: choice})),
        );
    }
}