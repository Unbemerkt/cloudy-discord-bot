const {SlashCommandBuilder, EmbedBuilder} = require("discord.js");
const {useHistory, useQueue} = require('discord-player');
const moduleLoader = require("../../../moduleLoader");
const discordUserClass = require("../../discordCommandManager/DiscordUser");
var musicModule;
var permissionManager;
module.exports = {
    data: new SlashCommandBuilder().setName("back").setDescription("Go back a track (doesn't work when queue is looped)"),
    async execute(interaction) {
        if (musicModule === undefined) musicModule = moduleLoader.getModule("discordMusic");
        var localisation = musicModule.localisation;
        const loc = localisation[interaction.locale] ?? localisation["en"];
        if (permissionManager === undefined) permissionManager = moduleLoader.getModule("permissions");
        const executeUser = new discordUserClass(permissionManager,interaction.member);
        var embedMessage
        if (executeUser === undefined || !executeUser.hasPermissions(["discord.command.skip", "music.skip"])) {
            embedMessage = new EmbedBuilder()
                .setTitle(loc["errorTitle"])
                .setDescription(loc["noPermissions"])
                .setColor("#c91616")
            interaction.reply({embeds: [embedMessage], ephemeral: true});
            return;
        }

        // let's defer the interaction as things can take time to process
        await interaction.deferReply();
        try {
            if (!interaction.guild.members.me.voice.channel){
                return void interaction.followUp(loc["noPlaying"]);
            }
            if (interaction.member.voice.channelId !== interaction.guild.members.me.voice.channelId){
                return await interaction.followUp({ content: loc["notSameChannel"], ephemeral: true });
            }
            const history = useHistory(interaction.guildId);
            const queue = useQueue(interaction.guildId);
            if (history === null || queue === null || queue.node === null) return void interaction.followUp(loc["unable"]);
            if (history.tracks.size === 0) return void interaction.followUp(loc["noBack"]);
            await history.previous();
            if (musicModule.playingGuilds[interaction.guildId] === undefined) {
                musicModule.playingGuilds[interaction.guildId] = {};
                musicModule.playingGuilds[interaction.guildId].tracks = {};
                musicModule.playingGuilds[interaction.guildId].followUp = false;
            }
            queue.metadata = interaction;
            musicModule.playingGuilds[interaction.guildId].followUp = false;
            musicModule.playingGuilds[interaction.guildId]["interaction"] = interaction;
            await interaction.followUp({content: loc["back"]});
        } catch (e) {
            // let's return error if something failed
            console.error(e);
            await interaction.followUp(loc["error"].replaceAll(/%ERROR%/g, e.toString()));
        }
    },
}