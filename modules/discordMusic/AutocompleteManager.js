const fs = require("fs");

module.exports = class {
    guildID
    constructor(guildID) {
        this.guildID = guildID;
    }

    getGuildID(){
        return this.guildID;
    }

    getList(noGlobal){
        var result = {};
        if (!fs.existsSync(__dirname+"/data/autocomplete/"+this.getGuildID()+".json")){
            fs.writeFileSync(__dirname+"/data/autocomplete/"+this.getGuildID()+".json",JSON.stringify({songs:{}},null,2))
            return this.getGlobal();
        }
        result = JSON.parse(fs.readFileSync(__dirname+"/data/autocomplete/"+this.getGuildID()+".json"));
        if (!noGlobal) {
            const global = this.getGlobal().songs;
            Object.keys(global).forEach(title=>{
                if (result.songs[title]===undefined)result.songs[title] = global[title];
            })
        }
        return result;
    }

    getGlobal(){
        if (!fs.existsSync(__dirname+"/data/autocomplete/GLOBAL.json")){
            fs.writeFileSync(__dirname+"/data/autocomplete/GLOBAL.json",JSON.stringify({songs:{}},null,2))
            return {songs: {}};
        }
        return JSON.parse(fs.readFileSync(__dirname+"/data/autocomplete/GLOBAL.json"));
    }

    getUrl(title,noGlobal){
        const list = this.getList(noGlobal).songs;
        return list[title];
    }

    addToList(title, url){
        const list = this.getList(true).songs;
        list[title] = url;
        this.save(list);
    }

    remove(title){
        const list = this.getList(true).songs;
        if (list[title]===undefined)return undefined;
        const url = list[title];
        delete list[title]
        this.save(list)
        return url;
    }
    /**
     * @param list e.g. [{"TITLE": "URL"}, {...}]
     * */
    save(list){
        fs.writeFileSync(__dirname+"/data/autocomplete/"+this.getGuildID()+".json",JSON.stringify({songs:list},null,2))
    }

}