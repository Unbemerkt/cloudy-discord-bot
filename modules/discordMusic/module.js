const moduleLoader = require("./../../moduleLoader");
const {Player} = require("discord-player");
const fs = require("fs");
const {ModuleUtils} = require("../../ModuleUtils");
const moduleInfo = require("./moduleInfo.json");
const {SettingsRow} = require("../settings/module");
const moduleUtils = new ModuleUtils(moduleInfo);
//https://discord-player.js.org/guide/examples/common-actions
//https://discord-player.js.org/docs/discord-player/class/Player
//@discordjs/opus install trouble: https://github.com/discordjs/opus/issues/43#issuecomment-716168497
var dCore;
var settingsModule;
var expressModule;
var cmdManager;
module.exports = {
    moduleInfo,
    moduleUtils,
    moduleRegistered: function () {

    },
    moduleLoaded: function () {

    },
    runModule: function () {
        if (dCore === undefined) dCore = moduleLoader.getModule("discordCore");
        if (expressModule === undefined) expressModule = moduleLoader.getModule("ExpressCore");
        if (cmdManager === undefined) cmdManager = moduleLoader.getModule("Discord CMD Manager");
        if (settingsModule === undefined) settingsModule = moduleLoader.getModule("settings");
        if (!fs.existsSync(__dirname+"/data/playlists/")){
            fs.mkdirSync(__dirname+"/data/playlists/");
            moduleUtils.log("Created playlists folder!")
        }
        if (!fs.existsSync(__dirname+"/data/autocomplete/")){
            fs.mkdirSync(__dirname+"/data/autocomplete/");
            moduleUtils.log("Created autocomplete folder!")
        }
        this.reloadData();
        const tmp = require("./PlayListManager");
        this.playlistManager = new tmp();
        if (this.config["ffmpeg_check"]) {
            var list = moduleLoader.checkNodeModule(["ffmpeg-static", "@ffmpeg-installer/ffmpeg", "@node-ffmpeg/node-ffmpeg-installer", "ffmpeg-binaries"]);
            if (list.length === 0) {
                moduleUtils.warn("No ffmpeg module found! Please make sure that ffmpeg is installed or install a ffmpeg module (ffmpeg-static (recommended) or @ffmpeg-installer/ffmpeg or @node-ffmpeg/node-ffmpeg-installer or ffmpeg-binaries)")
                moduleUtils.warn("You can disable this warning in the config! (set ffmpeg_check = false)")
            } else {
                moduleUtils.warn("Found an ffmpeg module (" + list.toString() + ")! (These module(s) can be unstable! For best experience download and install ffmpeg: https://ffmpeg.org/download.html)")
                moduleUtils.warn("You can disable this warning in the config! (set ffmpeg_check = false)")
            }
        }
        dCore.setActivityVar("PLAYING_COUNT", () => {
            return Object.keys(this.playingGuilds).length;
        })
        dCore.setActivityVar("PLAYING", () => {
            const i = Object.keys(this.playingGuilds).length;
            if (i === 0) {
                return module.exports.config["activityString"]["zero"];
            } else if (i === 1) {
                return module.exports.config["activityString"]["one"];
            }
            return module.exports.config["activityString"]["more"].replaceAll(/%COUNT%/g, i);
        })


        settingsModule.registerUpdateFunction(moduleInfo.name, this.settings.updateSettings);
        this.settings.updateSettings();


        dCore.registerStartListener(moduleInfo.name, (client) => {
            module.exports.startPlayer(client);
        });
        //register web
        expressModule.registerModulePage(moduleInfo,__dirname+"/routes/discordMusic.js");
        expressModule.registerModulePage(moduleInfo,__dirname+"/routes/playlistEditor.js");
        expressModule.registerModulePage(moduleInfo,__dirname+"/routes/autocompleteEditor.js");
        expressModule.registerModulePage(moduleInfo,__dirname+"/routes/autocomplete.js");
        expressModule.registerModulePage(moduleInfo,__dirname+"/routes/playlist.js");

        dCore.registerStopListener(moduleInfo.name,module.exports.stopPlayer);
    },
    reloadData() {
        delete require.cache[require.resolve("./localisation.json")];
        delete require.cache[require.resolve("./config.json")];
        this.localisation = require("./localisation.json");
        this.config = require("./config.json");

    },
    stopPlayer() {
        moduleUtils.log("Unloading player...")
        module.exports.playingGuilds = {};
        if (module.exports.player===undefined)return;
        module.exports.player.nodes.cache.forEach((queue,str)=>{
            queue.delete();
        })
        module.exports.player.events.removeListener("playerStart",module.exports.events.playerStart);
        module.exports.player.events.removeListener("audioTrackAdd", module.exports.events.audioTrackAdd);
        module.exports.player.events.removeListener("audioTracksAdd", module.exports.events.audioTracksAdd);
        module.exports.player.events.removeListener("disconnect", module.exports.events.disconnect);
        module.exports.player.events.removeListener("emptyChannel", module.exports.events.emptyChannel);
        module.exports.player.events.removeListener("emptyQueue", module.exports.events.emptyQueue);
        module.exports.player.events.removeListener("error", module.exports.events.error);
        moduleUtils.log("Player unloaded!");
    },
    events: {
        playerStart(queue, track){
            // we will later define queue.metadata object while creating the queue
            setTimeout(()=>{
                const loc = module.exports.localisation[queue.metadata.locale] ?? module.exports.localisation["en"];
                var title = track.title;
                const map = module.exports.playingGuilds[queue.metadata.guildId];
                if (map !== undefined) {
                    if (map["tracks"][track.title] !== undefined) {
                        title = map["tracks"][track.title];
                    }
                }
                var interaction = queue.metadata;
                if (map!==undefined&&map["interaction"]!==undefined) interaction = map["interaction"];
                const message = loc["playerStart"].replaceAll(/%TRACK_TITLE%/g, title);
                if (queue.repeatMode===1){
                    //loop current track - prevent spam
                    if (map["loop"]===undefined){
                        map["loop"] = {
                            lastMessage: message
                        }
                    }else{
                        if (map["loop"].lastMessage===message){
                            return;
                        }
                    }
                }else if (map["loop"]!==undefined){
                    delete map["loop"]
                }

                if (interaction.deferred&&map.followUp) {
                    interaction.followUp(message);
                    module.exports.playingGuilds[queue.metadata.guildId].followUp = false;
                }else{
                    interaction.channel.send(message);
                }
                module.exports.playingGuilds[queue.metadata.guildId].current = {
                    title: title,
                    rawTitle: track.title,
                    url: track.url
                }
            },100)
        },
        audioTrackAdd(queue, track){
            // Emitted when the player adds a single song to its queue
            // const loc = module.exports.localisation[queue.metadata.locale]??this.localisation["en"];
            // queue.metadata.channel.send(loc["trackQueued"].replaceAll(/%TRACK_TITLE%/g,track.title));
        },
        audioTracksAdd(queue, track){
            // Emitted when the player adds multiple songs to its queue
            const loc = module.exports.localisation[queue.metadata.locale] ?? module.exports.localisation["en"];
            const message = loc["audioTracksAdd"]
            queue.metadata.channel.send(message);
        },
        disconnect(queue){
            // Emitted when the bot leaves the voice channel
            const loc = module.exports.localisation[queue.metadata.locale] ?? module.exports.localisation["en"];
            if (module.exports.playingGuilds[queue.metadata.guildId] !== undefined) {
                delete module.exports.playingGuilds[queue.metadata.guildId];
            }
            queue.metadata.channel.send(loc["disconnect"]);
        },
        emptyChannel(queue){
            // Emitted when the voice channel has been empty for the set threshold
            // Bot will automatically leave the voice channel with this event
            const loc = module.exports.localisation[queue.metadata.locale] ?? module.exports.localisation["en"];
            if (module.exports.playingGuilds[queue.metadata.guildId] !== undefined) {
                delete module.exports.playingGuilds[queue.metadata.guildId];
            }
            queue.metadata.channel.send(loc["emptyChannel"]);
        },
        emptyQueue(queue){
            // Emitted when the player queue has finished
            const loc = module.exports.localisation[queue.metadata.locale] ?? module.exports.localisation["en"];
            if (module.exports.playingGuilds[queue.metadata.guildId] !== undefined) {
                delete module.exports.playingGuilds[queue.metadata.guildId];
            }
            queue.metadata.channel.send(loc["emptyQueue"]);
        },
        error(queue, error){
            // Emitted when the player queue has finished
            // const loc = module.exports.localisation[queue.metadata.locale]??this.localisation["en"];
            queue.metadata.channel.send("An error occurred!");
            if (module.exports.playingGuilds[queue.metadata.guildId] !== undefined) {
                delete module.exports.playingGuilds[queue.metadata.guildId];
            }
            moduleUtils.error(error);
        }
    },
    startPlayer(client) {
        cmdManager.registerExternCommand(__dirname + "/commands/back.js")
        cmdManager.registerExternCommand(__dirname + "/commands/playlist.js")
        cmdManager.registerExternCommand(__dirname + "/commands/play.js")
        cmdManager.registerExternCommand(__dirname + "/commands/loop.js")
        cmdManager.registerExternCommand(__dirname + "/commands/pause.js")
        cmdManager.registerExternCommand(__dirname + "/commands/stop.js")
        cmdManager.registerExternCommand(__dirname + "/commands/volume.js")
        cmdManager.registerExternCommand(__dirname + "/commands/playmode.js")
        cmdManager.registerExternCommand(__dirname + "/commands/skip.js")
        cmdManager.registerExternCommand(__dirname + "/commands/autocomplete.js")

        moduleUtils.log("Loading audio player...")
        this.player = new Player(client)
        this.player.extractors.loadDefault().then(() => {
            moduleUtils.success("Player loaded!")
        });
        this.player.events.on('playerStart', module.exports.events.playerStart);
        this.player.events.on('audioTrackAdd', module.exports.events.audioTrackAdd);
        this.player.events.on('audioTracksAdd',  module.exports.events.audioTracksAdd);
        this.player.events.on('disconnect', module.exports.events.disconnect);
        this.player.events.on('emptyChannel', module.exports.events.emptyChannel);
        this.player.events.on('emptyQueue', module.exports.events.emptyQueue);
        this.player.events.on('error', module.exports.events.error);
    },
    player: {},
    settings: {
        updateSettings() {
            if (settingsModule === undefined) settingsModule = moduleLoader.getModule("settings");
            const builder = new settingsModule.SettingsBuilder(moduleInfo);
            const row1 = new settingsModule.SettingsRow();
            row1.setTitle("Config");
            row1.addSelect("enableModule", "Enable module", moduleInfo.enable.toString(), [{
                value: "true",
                label: "enable"
            }, {value: "false", label: "disable"}], "*Note: Please restart the app for the settings to take effect!");
            row1.addNumber("default_volume", "Default music volume", module.exports.config["defaultVolume"], "5", "", 0, 100, true)
            builder.add(row1);
            const row_2 = new SettingsRow();
            row_2.setTitle("%PLAYING% Activity Replacement:")
            row_2.addString("activityString_zero", "Not Playing:", module.exports.config.activityString.zero, "Playing currently no music!", false, "", true)
            row_2.addString("activityString_one", "Playing for One:", module.exports.config.activityString.one, "Playing music for one Channel!", false, "", true)
            row_2.addString("activityString_more", "Playing in Multiple VC's:", module.exports.config.activityString.more, "Playing music for %COUNT% Channels!", false, "%COUNT% => Playing in VC's", true);
            builder.add(row_2);
            const moreContainer = new settingsModule.SettingsContainer("more_settings");
            moreContainer.setTitle("More Settings");
            moreContainer.add(new settingsModule.SettingsRow().addSelect("ffmpeg_check", "ffmpeg_check",
                module.exports.config.ffmpeg_check.toString(), [{value: "true", label: "enable"},
                    {value: "false", label: "disable"}]));
            builder.add(moreContainer)
            builder.add(new settingsModule.SettingsRow().addText("Localisation", "You can edit the localisation here:", "*Note: en is the rollback language!"))

            Object.keys(module.exports.localisation).forEach(loc => {
                var i = 0;
                var container = new settingsModule.SettingsContainer(loc)
                var row = new settingsModule.SettingsRow();
                Object.keys(module.exports.localisation[loc]).forEach(loc2 => {
                    if (i === 2) {
                        container.add(row);
                        row = new settingsModule.SettingsRow();
                        i = 0;
                    }
                    row.addString(loc + "_" + loc2, loc2, module.exports.localisation[loc][loc2], "", false, "", true, {
                        localisation: loc,
                        specific: loc2
                    });
                    i++;
                })
                if (row !== undefined) {
                    container.add(row);
                }
                builder.add(container);
            });

            const rowCreate = new settingsModule.SettingsRow();
            rowCreate.setTitle("Create localisation");
            rowCreate.addString("create_loc", "localisation", "", "e.g en, de, pl ...", false)
            rowCreate.addBool("create_loc_bool", "Create localisation?", "Create localisation?", false, "", "Create localisation!", "Do not create localisation")
            builder.add(rowCreate);
            builder.setAutoReload(true);
            builder.setCallback(module.exports.settings.receiveSettings) // set new settings receiver
            settingsModule.registerSettingsBuilder(builder);
        },
        receiveSettings(settingsResult) {
            moduleInfo.enable = settingsResult.getSelect("enableModule") === "true";

            const resultMap = settingsResult.getRaw();
            var updatedLocalisation = {}
            Object.keys(resultMap).every(sID => {
                const setting = resultMap[sID];
                if (typeof setting.extra === "string") return true;
                const extra = setting.extra;
                if (updatedLocalisation[extra.localisation] === undefined) updatedLocalisation[extra.localisation] = {};
                updatedLocalisation[extra.localisation][extra.specific] = setting.value;
                return true;
            })


            if (settingsResult.getBool("create_loc_bool")) {
                var newLoc = settingsResult.getString("create_loc");
                if (newLoc !== undefined && newLoc.length !== 0) {
                    if (updatedLocalisation[newLoc] === undefined) {
                        //create loc
                        newLoc = newLoc.replaceAll(/ /g, "_");
                        updatedLocalisation[newLoc] = {};
                        for (var i in module.exports.localisation["en"])
                            updatedLocalisation[newLoc][i] = updatedLocalisation["en"][i];
                    }
                }
            }
            module.exports.localisation = updatedLocalisation;
            module.exports.config["activityString"]["zero"] = settingsResult.getString("activityString_zero");
            module.exports.config["activityString"]["one"] = settingsResult.getString("activityString_one");
            module.exports.config["activityString"]["more"] = settingsResult.getString("activityString_more");
            module.exports.config["defaultVolume"] = settingsResult.getNumber("default_volume");
            module.exports.config["ffmpeg_check"] = settingsResult.getSelect("ffmpeg_check") === "true";
            fs.writeFileSync(__dirname + "/localisation.json", JSON.stringify(updatedLocalisation, null, 2));
            fs.writeFileSync(__dirname + "/config.json", JSON.stringify(module.exports.config, null, 2));
            fs.writeFileSync(__dirname + "/moduleInfo.json", JSON.stringify(moduleInfo, null, 2));
            module.exports.reloadData();
        }
    },
    localisation: {},
    config: {},
    playingGuilds: {},
    playlistManager: undefined
};