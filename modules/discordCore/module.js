const discord = require("discord.js");
const fs = require("fs");
const Client = discord.Client;
const GatewayIntentBits = discord.GatewayIntentBits;
const config = require("./config.json");
const {ModuleUtils} = require("../../ModuleUtils");
const moduleLoader = require("../../moduleLoader");
const moduleInfo = require("./moduleInfo.json");
const {SettingsContainer, SettingsList, SettingsListInput, SettingsListRow, SettingsRow} = require("../settings/module");
const {Events} = require("discord.js");
const moduleUtils = new ModuleUtils(moduleInfo);
const clientData = {
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.DirectMessages,
        GatewayIntentBits.GuildMembers,
        GatewayIntentBits.GuildVoiceStates
    ]
}
const onlineStates = {
    OFFLINE: "OFFLINE",
    ONLINE: "ONLINE",
    STARTING: "STARTING"
};
var onlineState = onlineStates.OFFLINE;
var settingsBuilder;
var commandsBU;
var startedListeners = {};
var stoppedListeners = {};
module.exports = {
    onlineStates,
    moduleInfo,
    client: {},
    activityVars: {},
    moduleLoaded: function () {
    },
    moduleRegistered: function () {
    },
    setActivityVar(placeholder, callBack){
        this.activityVars[placeholder] = callBack;
        this.updateSettings();
    },
    runModule: function () {
        moduleUtils.log("Starting discord core...")
        if (settingsBuilder === undefined) settingsBuilder = moduleLoader.getModule("settings");
        if (config.autoStart) {
            this.start();
        } else {
            moduleUtils.warn("Autostart disabled! Initializing Client for other modules!")
            this.client = new Client(clientData); //init client for other modules
        }
        this.setActivityVar("GUILDS",()=>{
            const guilds = this.getGuilds();
            return Object.keys(guilds).length;
        })
        settingsBuilder.registerUpdateFunction(moduleInfo.name, this.updateSettings);
    },
    registerStartListener(id, cb) {
        startedListeners[id] = cb;
    },
    registerStopListener(id, cb) {
        stoppedListeners[id] = cb;
    },
    receiveSettings(resultSettings) {
        const stateSetting = resultSettings.getMultiBtn("discordState");
        if (onlineState !== onlineStates.STARTING) {
            if (stateSetting === onlineStates.ONLINE && onlineState === onlineStates.OFFLINE) {
                //shutdown
                moduleUtils.log("Received new should bot state! (" + stateSetting + ")");
                module.exports.start(true);
            } else if (stateSetting === onlineStates.OFFLINE && onlineState === onlineStates.ONLINE) {
                //start
                moduleUtils.log("Received new should bot state! (" + stateSetting + ")");
                module.exports.stop();
            } else {
                moduleUtils.warn("Received new should bot state! But current state is new should state! (Current: " + onlineState + " New: " + stateSetting + ")");
            }
        } else {
            moduleUtils.warn("Please wait before you set a new bot state! Bot is currently starting! (" + stateSetting + ")");
        }
        const autoStart = resultSettings.getSelect("autoStart");
        config.autoStart = autoStart === "true";
        config.BOT_TOKEN = resultSettings.getString("BOT_TOKEN");
        config.clientId = resultSettings.getString("clientId");
        config.guildId = resultSettings.getString("guildId");
        config.activityTimeSeconds = resultSettings.getNumber("activityTimeSeconds");
        config.activities = [];
        const newActivities = resultSettings.getList("activities");
        if (newActivities.length===0){
            config.activities.push({activity: "Custom", name: "» Cloudy"});
        }else{
            newActivities.forEach(val=>{
                config.activities.push({activity: val.values[0], name: val.values[1]});
            })
        }

        module.exports.saveConfig();
    },
    saveConfig() {
        moduleUtils.log("Saving config...")
        try {
            fs.writeFileSync(__dirname + "/config.json", JSON.stringify(config, null, 2));
            moduleUtils.log("Config saved!")
        } catch (e) {
            moduleUtils.error("Failed to save config: ", e.toString());
        }
    },
    getGuilds(reFetch) {
        if (onlineState !== onlineStates.ONLINE) return {};
        if (reFetch) moduleUtils.log("Fetching members... (this may take a while!)")
        const guildList = this.client.guilds.cache.map(guild => {
            if (reFetch) {
                guild.members.fetch()
                    .then((r) => {
                        moduleUtils.log("[" + guild.name + "] Members fetched!");
                    })
                    .catch((r) => {
                        moduleUtils.error("[" + guild.name + "] An error occurred during fetching members! ", r);
                    });
            }
            return {
                guildID: guild.id,
                guildName: guild.name,
                roles: guild.roles.cache.sort((a, b) => b.position - a.position).map(r => {
                    return {
                        id: r.id,
                        name: r.name,
                        members: r.members
                    };
                }),
                members: guild.members.cache.map(u => {
                    return {
                        displayName: u.displayName,
                        id: u.id,
                        rawData: u
                    }
                })
            }
        });
        const guildMap = {};
        guildList.forEach(guild => {
            guildMap[guild.guildID] = guild;
        })
        return guildMap;
    },
    getGuild(guildID) {
        if (onlineState !== onlineStates.ONLINE) return undefined;
        return this.client.guilds.cache.get(guildID);
    },
    stop() {
        commandsBU = this.client.commands;
        moduleUtils.warn("DiscordBot is Shutting down...")
        this.client.destroy().then(() => {
            onlineState = onlineStates.OFFLINE;
            this.updateSettings();
            moduleUtils.warn("DiscordBot stopped!")
            Object.keys(stoppedListeners).forEach(id=>{
                if (stoppedListeners[id]) stoppedListeners[id]();
            })
        });
    },
    updateSettings() {
        if (settingsBuilder === undefined) settingsBuilder = moduleLoader.getModule("settings");
        const builder = new settingsBuilder.SettingsBuilder(moduleInfo);
        const row1 = new settingsBuilder.SettingsRow();
        row1.setTitle("Bot Settings")
        const mBTN = new settingsBuilder.MultiButton("discordState")
        mBTN.setLabel("Bot Status")
        if (onlineState === onlineStates.STARTING) {
            if (onlineState === onlineStates.STARTING) mBTN.disable();
            mBTN.addState("STARTING", "yellow", "Starting...", "STARTING");
        } else {
            mBTN.addState("OFFLINE", "red", "Offline!", "OFFLINE");

            mBTN.addState("ONLINE", "green", "Online!", "ONLINE");
        }
        mBTN.setSelected(onlineState);
        row1.addMultiButton(mBTN);
        row1.addLink("inviteLink","Add your bot to a server","Invite Bot",
            "https://discord.com/api/oauth2/authorize?client_id="+config.clientId+"&permissions=8&scope=bot","","btn btn-info","",true)
        builder.setCallback(module.exports.receiveSettings)
        builder.add(row1)
        const row2 = new settingsBuilder.SettingsRow();
        row2.setTitle("Config")
        row2.addSelect("autoStart", "Enable autostart", config.autoStart.toString(), [{
            value: "true",
            label: "enable"
        }, {value: "false", label: "disable"}], "");
        row2.addString("clientId", "Client ID", config.clientId, "Client ID", false, "*This setting will take effect, when the bot is restarted!", true)
        row2.addString("guildId", "Guild ID", config.guildId, "Guild ID", false, "*Main guild ID (optional)")
        builder.add(row2)
        const row3 = new settingsBuilder.SettingsRow();
        row3.addString("BOT_TOKEN", "Bot token", config.BOT_TOKEN, "Secret Bot token", true, "*This setting will take effect, when the bot is restarted!", true);
        builder.add(row3)

        const container = new SettingsContainer("activities").setTitle("Activities");
        container.add(new SettingsRow().addNumber("activityTimeSeconds","Time between Activity change",config.activityTimeSeconds,"15","","15",undefined,true,""))

        const list = new SettingsList("activities");
        const vars = Object.keys(module.exports.activityVars);
        var placeholders = "";
        var isFirst = true;
        vars.forEach(v=>{
            if (isFirst){
                isFirst=false;
                placeholders+="%"+v+"%";
            }else{
                placeholders+=", %"+v+"%";
            }
        })
        list.setSmall(placeholders)
        list.setColTitles(["Type","Activity"])
        const activitiesInput = new SettingsListInput();
        activitiesInput.addSelectValue("Competing")
        activitiesInput.addSelectValue("Playing")
        activitiesInput.addSelectValue("Custom")
        activitiesInput.addSelectValue("Streaming")
        activitiesInput.addSelectValue("Listening")
        activitiesInput.addSelectValue("Watching")
        list.addInput(activitiesInput)
        list.addInput(new SettingsListInput().setPlaceholder("Activity"));
        config.activities.forEach((act)=>{
            list.addRow(new SettingsListRow([act.activity,act.name]))
        })
        container.add(list);
        builder.add(container);
        settingsBuilder.registerSettingsBuilder(builder);
    },
    start(loadCommands) {
        moduleUtils.log("Starting DiscordBot...")
        const startTime = Date.now();
        onlineState = onlineStates.STARTING;
        this.updateSettings();
        this.client = new Client(clientData);
        if (commandsBU !== undefined) this.client.commands = commandsBU;
        this.client.login(config.BOT_TOKEN).then(r => {
            moduleUtils.success("Successfully logged in!")
            onlineState = onlineStates.ONLINE;
            this.updateSettings();
            this.getGuilds(true);
            const  endTime = Date.now();
            moduleUtils.log("DiscordBot started! (took "+(endTime-startTime)+"ms)")
            Object.keys(startedListeners).forEach(id=>{
                if (typeof startedListeners[id] === "function") startedListeners[id](this.client);
            })
            //https://stackoverflow.com/questions/64044472/discord-bot-presence
            //https://discord-api-types.dev/api/discord-api-types-v10/enum/ActivityType
            this.updateActivity(0);
            if (loadCommands) {
                const cmdManager = moduleLoader.getModule("Discord CMD Manager");
                cmdManager.commands = [];
                cmdManager.loadCommands();
                cmdManager.startListener();
            }
        }).catch(e=>{
            const endTime = Date.now();
            moduleUtils.error("Failed to start bot! (failed after "+(endTime-startTime)+"ms)", e)
            this.onlineState = onlineStates.OFFLINE;
            this.updateSettings();
        });
        this.client.on(Events.GuildCreate,(guild)=>{
            moduleUtils.log(`Joined new guild: ${guild.name}`);
            moduleUtils.log("[" + guild.name + "] Fetching members... (this may take a while!)")
            guild.members.fetch()
                .then((r) => {
                    moduleUtils.log("[" + guild.name + "] Members fetched!");
                })
                .catch((r) => {
                    moduleUtils.error("[" + guild.name + "] An error occurred during fetching members! ", r);
                });
        })
        this.client.on(Events.GuildDelete,(guild)=>{
            moduleUtils.warn(`Removed from guild: ${guild.name}`);
        })
    },
    updateActivity(step){
        if (onlineState!==onlineStates.ONLINE)return;
        if (step>=config.activities.length) step=0;
        const activity = config.activities[step];
        var name =  activity.name;
        Object.keys(this.activityVars).forEach(v=>{
            if (this.activityVars[v]!==undefined){
                var regExp = new RegExp("%"+v+"%");
                name = name.replace(regExp,this.activityVars[v]())
            }
        })
        this.client.user.setPresence({
            activities: [{
                name: name,
                type: discord.ActivityType[activity.activity],
                url: "https://discordapp.com/"
            }]
        });

        setTimeout(()=>{
            this.updateActivity(step+1);
        },config.activityTimeSeconds*1000);
    },
    getCurrentState() {
        return onlineState;
    },
    config,
    discord,
    onlineState
};