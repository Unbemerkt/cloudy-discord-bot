#!/bin/sh
while true
do
    npm start
    echo "Application stopped!"
    echo "Restarting in 5..."
    sleep 1s
    echo "Restarting in 4..."
    sleep 1s
    echo "Restarting in 3..."
    sleep 1s
    echo "Restarting in 2..."
    sleep 1s
    echo "Restarting in 1..."
    sleep 1s
    echo "Restarting NOW!"
done