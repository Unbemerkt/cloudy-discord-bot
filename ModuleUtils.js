const fs = require('fs');

class ModuleUtils {
    moduleInfo
    logFile = "./log.txt";
    htmlLogFile = "./htmlLog.txt";

    constructor(moduleInfo) {
        this.moduleInfo = moduleInfo;
        if (moduleInfo === undefined) {
            this.moduleInfo = {
                name: "LOG",
            };
        }
    }
    success(l1,l2){
        this.log(l1,l2,"color: #00FF00");
    }

    log(l1, l2, style) {
        if (typeof l1 === "object") l1 = l1.toString();
        if (typeof l2 === "object") l2 = l2.toString();
        if (l2 !== undefined) {
            console.log("[" + this.getTimeStamp() + "][" + this.moduleInfo.name.toUpperCase() + "] " + l1, l2)
            this.toLog("[" + this.getTimeStamp() + "][" + this.moduleInfo.name.toUpperCase() + "] " + l1, l2);
            this.toHtmlLog("[" + this.getTimeStamp() + "][" + this.moduleInfo.name.toUpperCase() + "] " + l1, l2, style);
            return;
        }
        console.log("[" + this.getTimeStamp() + "][" + this.moduleInfo.name.toUpperCase() + "] " + l1);
        this.toLog("[" + this.getTimeStamp() + "][" + this.moduleInfo.name.toUpperCase() + "] " + l1);
        this.toHtmlLog("[" + this.getTimeStamp() + "][" + this.moduleInfo.name.toUpperCase() + "] " + l1, undefined, style);
    }

    warn(l1, l2) {
        if (typeof l1 === "object") l1 = l1.toString();
        if (typeof l2 === "object") l2 = l2.toString();
        var style = "color: #ebdb2f";
        if (l2 !== undefined) {
            console.warn("[" + this.getTimeStamp() + "][WARN][" + this.moduleInfo.name.toUpperCase() + "] " + l1, l2)
            this.toLog("[" + this.getTimeStamp() + "][WARN][" + this.moduleInfo.name.toUpperCase() + "] " + l1, l2);
            this.toHtmlLog("[" + this.getTimeStamp() + "][WARN][" + this.moduleInfo.name.toUpperCase() + "] " + l1, l2, style);
            return;
        }
        console.warn("[" + this.getTimeStamp() + "][WARN][" + this.moduleInfo.name.toUpperCase() + "] " + l1);
        this.toLog("[" + this.getTimeStamp() + "][WARN][" + this.moduleInfo.name.toUpperCase() + "] " + l1);
        this.toHtmlLog("[" + this.getTimeStamp() + "][WARN][" + this.moduleInfo.name.toUpperCase() + "] " + l1, undefined, style);
    }


    pureLog(l1, l2) {
        if (typeof l1 === "object") l1 = l1.toString();
        if (typeof l2 === "object") l2 = l2.toString();
        if (l2 !== undefined) {
            console.log(l1, l2)
            this.toLog(l1, l2);
            this.toHtmlLog(l1, l2);
            return;
        }
        console.log(l1);
        this.toLog(l1);
        this.toHtmlLog(l1);
    }

    error(l1, l2) {
        if (typeof l1 === "object") l1 = l1.toString();
        if (typeof l2 === "object") l2 = l2.toString();
        if (l2 !== undefined) {
            console.error("[" + this.getTimeStamp() + "][ERROR][" + this.moduleInfo.name.toUpperCase() + "] " + l1, l2)
            this.toLog("[" + this.getTimeStamp() + "][ERROR][" + this.moduleInfo.name.toUpperCase() + "] " + l1, l2);
            this.toHtmlLog("[" + this.getTimeStamp() + "][ERROR][" + this.moduleInfo.name.toUpperCase() + "] " + l1,l2,"color:#ff0000;");
            return;
        }
        console.error("[" + this.getTimeStamp() + "][ERROR][" + this.moduleInfo.name.toUpperCase() + "] " + l1);
        this.toLog("[" + this.getTimeStamp() + "][ERROR][" + this.moduleInfo.name.toUpperCase() + "] " + l1);
        this.toHtmlLog("[" + this.getTimeStamp() + "][ERROR][" + this.moduleInfo.name.toUpperCase() + "] " + l1,undefined,"color:#ff0000;");
    }

    clear(user) {
        if (user !== undefined) {
            fs.writeFileSync(this.logFile, "[" + this.getTimeStamp() + "][INFO] Log cleared by " + user.getName() + "!");
            fs.writeFileSync(this.htmlLogFile, "[" + this.getTimeStamp() + "][INFO] Log cleared by " + user.getName() + "!");
            return;
        }
        fs.writeFileSync(this.logFile, "[" + this.getTimeStamp() + "][INFO] Log cleared!");
        fs.writeFileSync(this.htmlLogFile, "[" + this.getTimeStamp() + "][INFO] Log cleared!");
    }

    getTimeStamp(msTime) {
        var date;
        if (msTime!==undefined){
            date = new Date(msTime);
        }else{
            date = new Date();
        }
        return this.displayTime(msTime) + " " + date.getDate() + "." + (date.getMonth()+1) + "." + date.getFullYear();
    }

    displayTime(msTime) {
        var str = "";

        var currentTime;
        if (msTime!==undefined){
            currentTime = new Date(msTime);
        }else{
            currentTime = new Date();
        }
        var hours = currentTime.getHours()
        var minutes = currentTime.getMinutes()
        var seconds = currentTime.getSeconds()

        if (minutes < 10) {
            minutes = "0" + minutes
        }
        if (seconds < 10) {
            seconds = "0" + seconds
        }
        str += hours + ":" + minutes + ":" + seconds;
        return str;
    }

    noPermissions() {
        this.error("You don't have the permission to run this command!")
    }

    toLog(l1, l2) {
        if (typeof l1 !== "string") l1 = l1.toString();
        var content = "";
        if (l2 !== undefined) {
            if (typeof l2 !== "string") l2 = l2.toString();
            content = fs.readFileSync(this.logFile);
            content += "\n" + l1 + l2;
            fs.writeFileSync(this.logFile, content);
            return;
        }
        content = fs.readFileSync(this.logFile);
        content += "\n" + l1;
        fs.writeFileSync(this.logFile, content);
    }

    toHtmlLog(l1, l2, style) {
        if (typeof l1 !== "string") l1 = l1.toString();
        var content = "";
        if (l2 !== undefined) {
            if (typeof l2 !== "string") l2 = l2.toString();
            content = fs.readFileSync(this.htmlLogFile);
            content += "\n<br><span style='"+(style||"")+"'>" + l1 + l2 + "</span>";
            fs.writeFileSync(this.htmlLogFile, content);
            return;
        }
        content = fs.readFileSync(this.htmlLogFile);
        content += "\n<br><span style='"+(style||"")+"'>" + l1 + "</span>";
        fs.writeFileSync(this.htmlLogFile, content);
    }

    getLog() {
        return fs.readFileSync(this.logFile).toString();
    }

    getHtmlLog() {
        return fs.readFileSync(this.htmlLogFile).toString();
    }

    copyMap(mapObject){
        var map = {};
        Object.keys(mapObject).forEach(key=>{
            map[key] = mapObject[key];
        })
        return map;
    }
    clamp(value, max, min){
        if (value>max)return max;
        if (value<min)return min;
        return value;
    }
}

module.exports = {
    ModuleUtils
};