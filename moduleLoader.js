var fs = require('fs');
const {ModuleUtils} = require("./ModuleUtils");

var modules = {}
var modulePrio = {}
var unloaded = {}
const moduleInfo = {
    name: "LOG",
}
const moduleUtils = new ModuleUtils(moduleInfo);

async function registerModules() {
    saveCrashLog();
    fs.writeFileSync(moduleUtils.logFile, "");
    fs.writeFileSync(moduleUtils.htmlLogFile, "");
    moduleUtils.log("Log created!")
    moduleUtils.log("Starting Cloudy...")
    crashChecker();
    moduleUtils.pureLog("  / ____/ /___  __  ______/ /_  __");
    moduleUtils.pureLog(" / /   / / __ \\/ / / / __  / / / /");
    moduleUtils.pureLog("/ /___/ / /_/ / /_/ / /_/ / /_/ / ");
    moduleUtils.pureLog("\\____/_/\\____/\\__,_/\\__,_/\\__, /  ");
    moduleUtils.pureLog("                         /____/   ");
    moduleUtils.pureLog("");

    var files = fs.readdirSync("./modules/");
    if (files.length === 0) {
        moduleUtils.log("No modules found!")
        return;
    }
    moduleUtils.log("=========================================")
    moduleUtils.log("             Register Modules")
    moduleUtils.log("=========================================")
    for (const file of files) {
        moduleUtils.log("---")
        moduleUtils.log("Registering '" + file.replace(".js", "") + "'-...");
        var module = await import('./modules/' + file + "/module.js");
        if (module.default !== undefined && module.default.moduleInfo !== undefined) {
            module = module.default;
            moduleUtils.log(module.moduleInfo.name + ": moduleInfo found in module.exports! Please make sure that all your functions are in there, too!")
        }
        if (typeof module.moduleInfo === "object") {
            const moduleInfo = module.moduleInfo;
            var canRegister = true;
            if (typeof module.moduleRegistered !== "function") {
                moduleUtils.error(module.moduleInfo.name + ": main function is missing! (moduleRegistered) Module won't be registered!")
                canRegister = false;
            }
            if (typeof module.moduleLoaded !== "function") {
                moduleUtils.error(module.moduleInfo.name + ": main function is missing! (moduleLoaded) Module won't be registered!")
                canRegister = false;
            }
            if (typeof module.runModule !== "function") {
                moduleUtils.error(module.moduleInfo.name + ": main function is missing! (runModule) Module won't be registered!")
                canRegister = false;
            }
            if (canRegister) {
                var prio;
                if (moduleInfo.prio !== undefined) {
                    prio = moduleInfo.prio + "_";
                } else {
                    prio = "0_";
                }
                if (modulePrio[prio] === undefined) modulePrio[prio] = []
                modulePrio[prio].push(module);
                moduleUtils.log("Module '" + moduleInfo.name + "' registered!")
                module.moduleRegistered();
            }
        } else {
            moduleUtils.error("Failed to register ./modules/" + file + "/module.js! (moduleInfo is missing!)")
        }
    }
    moduleUtils.log("=========================================")
    moduleUtils.log("\n\n")
}

async function loadModules() {
    if (modulePrio.length === 0) return;
    moduleUtils.log("=========================================")
    moduleUtils.log("             Loading Modules")
    moduleUtils.log("=========================================")
    var ascMapKeys = Object.keys(modulePrio).sort();
    ascMapKeys.forEach(key => {
        modulePrio[key].forEach(module => {
                var canLoad = true;
                moduleUtils.log("Loading module '" + module.moduleInfo.name + "'...")
                var missingNodeModules = [];
                if (module.moduleInfo.requiredModules !== undefined && module.moduleInfo.requiredModules.length > 0) {
                    missingNodeModules = checkNodeModules(module);
                    if (missingNodeModules.length !== 0) {
                        canLoad = false;
                    }
                }
                var missingModules = [];
                if (module.moduleInfo.requiredNodeModules !== undefined && module.moduleInfo.requiredNodeModules.length > 0) {
                    missingModules = checkModules(module);
                    if (missingModules.length !== 0) {
                        canLoad = false;
                    }
                }
                if (module.moduleInfo.enable !== undefined && !module.moduleInfo.enable) {
                    canLoad = false;
                }
                if (canLoad) {
                    if (typeof module.moduleLoaded === "function") module.moduleLoaded();
                    modules[module.moduleInfo.name] = module;
                    moduleUtils.log("Module '" + module.moduleInfo.name + "' loaded!")
                } else {
                    moduleUtils.log("=====")
                    moduleUtils.log("Module '" + module.moduleInfo.name + "' Could not be loaded!")
                    module.moduleInfo.reasons = [];
                    if (missingNodeModules.length !== 0) {
                        module.moduleInfo.reasons.push({reason: "Missing node module(s)", data: missingNodeModules})
                        moduleUtils.log("Missing node module(s): ", missingNodeModules);
                        moduleUtils.log("Please run following command to install all missing packages: ");
                        var pg = "";
                        for (let i = 0; i < missingNodeModules.length; i++) {
                            pg += " " + missingNodeModules[i];
                        }
                        moduleUtils.log("'npm i" + pg + "'");
                    }
                    if (missingModules.length !== 0) {
                        moduleUtils.log("Missing module(s): ", missingModules);
                        module.moduleInfo.reasons.push({reason: "Missing module(s)", data: missingModules})
                    }
                    if (module.moduleInfo.enable !== undefined && !module.moduleInfo.enable) {
                        moduleUtils.log("Module '" + module.moduleInfo.name + "' is disabled!")
                        module.moduleInfo.reasons.push({reason: "Module disabled"})
                    }
                    unloaded[module.moduleInfo.name] = module.moduleInfo;
                    moduleUtils.log("=====")
                }
            }
        )
    });
    moduleUtils.log("=========================================")
    moduleUtils.log("\n\n")

}

async function runModules() {
    if (modules.length === 0) return;
    Object.keys(modules).forEach(name => {
        modules[name].runModule();
    })
}



function checkNodeModules(module) {
    if (module.moduleInfo.requiredNodeModules === undefined || module.moduleInfo.requiredNodeModules.length === 0) return [];
    var missingModules = [];
    module.moduleInfo.requiredNodeModules.forEach(rm => {
        if (!checkNodeModule(rm)) {
            missingModules.push(rm);
        }
    });
    return missingModules;
}

function checkNodeModule(moduleID) {
    if (typeof moduleID === "string"){
        try {
            require.resolve(moduleID)
            return true;
        } catch (e) {
            return false;
        }
    }else{
        var resultMap = []
        moduleID.forEach(id=>{
            try {
                require.resolve(id)
                resultMap.push(id);
            } catch (e) {
            }
        });
        return resultMap;
    }

}


function checkModules(module) {
    if (module.moduleInfo.requiredModules === undefined || module.moduleInfo.requiredModules.length === 0) return [];
    var missingModules = [];
    module.moduleInfo.requiredModules.forEach(rm => {
        if (!getModule(rm)) {
            missingModules.push(rm);
        }
    });
    return missingModules;
}


function getRegisteredModule(moduleName) {
    const keys = Object.keys(modulePrio);
    for (let i = 0; i < keys.length; i++) {
        for (let ii = 0; i < modulePrio[keys[i]].length; ii++) {
            if (modulePrio[keys[i]][ii] !== undefined && modulePrio[keys[i]][ii].moduleInfo.name === moduleName) return modulePrio[keys[i]];
        }
    }
    return undefined;
}

function getSimpleModules() {
    try {
        const simpleModules = [];
        Object.keys(modules).forEach(name => {
            simpleModules.push(modules[name].moduleInfo);
        });
        return simpleModules;
    } catch (e) {
        moduleUtils.error(e);
        return [];
    }
}

function getRegisteredSimpleModules() {
    const simpleModules = {};
    Object.keys(modulePrio).forEach((prio) => {
        modulePrio[prio].forEach(module => {
            simpleModules[module.moduleInfo.name] = module.moduleInfo;
        })
    })
    return simpleModules;
}


function getModule(moduleName) {
    return modules[moduleName];
}

module.exports = {
    hasCrashed: undefined,
    crashData: undefined,
    getRegisteredModule,
    getModule,
    modules,
    modulePrio,
    getSimpleModules,
    getRegisteredSimpleModules,
    unloaded,
    moduleUtils,
    checkModules,
    checkNodeModules,
    checkNodeModule,
    createTmp(){
        fs.writeFileSync(__dirname+"/tmp.json",JSON.stringify({running: true, crashes: 0, time: Date.now()},null,2))
    },
    getTmpFile(){
        if (!fs.existsSync(__dirname+"/tmp.json"))return undefined;
        return JSON.parse(fs.readFileSync(__dirname+"/tmp.json"));
    },
    saveTmpFile(data){
        fs.writeFileSync(__dirname+"/tmp.json",JSON.stringify(data,null,2))
    },
    shutdown(){
        fs.unlinkSync(__dirname+"/tmp.json")
        process.exit(0);
    },
    checkCrash(){
        this.hasCrashed = fs.existsSync(__dirname+"/tmp.json");
        return this.hasCrashed;
    }
};
//start app
var logSaved;

function saveCrashLog(){
    if (module.exports.checkCrash()){
        const tmpFile = module.exports.getTmpFile();
        if (!fs.existsSync("./crash-logs/")){
            fs.mkdirSync("./crash-logs/");
        }
        if (tmpFile.time===undefined){
            logSaved = "./crash-logs/"+(moduleUtils.getTimeStamp(Date.now()).replaceAll(/ /g,"_"))+"_log";
        }else{
            logSaved = "./crash-logs/"+(moduleUtils.getTimeStamp(tmpFile.time).replaceAll(/ /g,"_"))+"_log";
        }
        if (fs.existsSync("./htmlLog.txt")){
            fs.copyFileSync("./htmlLog.txt",logSaved+".html")
        }
        if (fs.existsSync("./log.txt")){
            fs.copyFileSync("./log.txt",logSaved+".txt")
        }
    }
}
function crashChecker(){
    if (module.exports.hasCrashed){
        const tmpFile = module.exports.getTmpFile();
        tmpFile.crashes += 1;
        if (tmpFile.crashes===1){
            moduleUtils.warn("Detected crash! Seems like its the first!")
        }else{
            moduleUtils.warn("Detected crash! Its the "+tmpFile.crashes+". crash in a row!")
        }
        if (logSaved!==undefined){
            moduleUtils.warn("Crash-log saved at '"+logSaved+"'");
        }
        tmpFile.time = Date.now();
        module.exports.crashData = tmpFile;
        module.exports.saveTmpFile(tmpFile);
    }else{
        module.exports.createTmp();
    }
}
registerModules().then(r => {
    loadModules().then(() => {
        runModules();
    });
});