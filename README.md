# Cloudy Discord Bot
Cloudy is a Discord bot that can play music in your voice channels. This Bot is made for one Guild but can be used on multiple guilds as well. Cloudy is built mainly using the [Discord.js](https://discord.js.org/) and [Discord-Player.js](https://discord-player.js.org) libraries.

*Please note: each module needs its own libs. When the application is run for the first time, it is displayed which node_modules are required

*The webinterface uses:
- mainly self coded stuff
- [Bootstrap v5.0.2](https://getbootstrap.com/docs/5.0/getting-started/introduction/)
- [jQuery v3.7.1](https://jquery.com/)

[[_TOC_]]

## Features
- Webinterface
- Modular
- Edit settings of the modules in the Webinterface
- Console to control Cloudy
- Own Complex Permission System (Can be managed in the Webinterface or in the Console)
- MusicModule:
  - Localisation
  - Playlist editor in the webinterface
- More coming soon!

## Installation
1. Clone the repository
2. Run the application `npm start`
3. Run the shown commands to install all needed node modules
   - If you want to run this bot on Linux and have trouble to install the @discordjs/opus module, please make sure you have `make` on your 
   system (install `make` with `sudo apt-get install build-essential`)
   - *Note: the `discordMusic` module needs `ffmpeg` (on linux `sudo apt install ffmpeg` on windows please download it [here](https://ffmpeg.org/download.html) or you can install it by running this (not recommended, this can be unstable): `npm i ffmpeg-static`) 
4. Follow the `Configuration` instructions
5. start the bot again with `npm start`
   - On linux you can optional install `screen` and run the bot with the `start.sh` file
6. open now the webinterface (`http://127.0.0.1:8088` depends on your configuration)
   - Login with the default user (username: `admin` password: `admin`)
7. Go to `Settings` then press on `Register commands` (if you want the bot only on one server, please make sure that you have properly edited the config of the `discordCore` module) or `Register commands Global` (for multiple servers)

## Configuration
- discordCore Module:
  1. go in the `modules/discordCore` directory
  2. rename `config-exapmle.json` to `config.json`
     - edit `BOT_TOKEN`, `clientId` and `guildId` (your main guild, this is just for the first time important) `activities` can be edited later in the webinterface
- expressCore Module
  1. go in the `modules/express` directory
  2. edit the config.json
      - set `devMode` to `true`, you can set `bindHost` blank or bind it to a domain or ip (e.g. `my.domain.com` or `127.0.0.1`)

### License ###
---
```
MIT License

Copyright (c) 2023 Keno Schulz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE
